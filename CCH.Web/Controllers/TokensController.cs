﻿using CCH.Common.Entities.Form;
using CCH.Common.Entities.View;
using CCH.Common.Exceptions;
using CCH.Common.Interfaces;
using CCH.Common.Settings;
using CCH.Infrastructure.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/Tokens")]
    public class TokensController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public TokensController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpPost]
        // POST api/Tokens 
        public IActionResult Post([FromBody] FormAuthorization user)
        {
            if (!ModelState.IsValid)
            {
                Dictionary<string, string> errors = new Dictionary<string, string>();
                ModelState.Keys.ToList().ForEach(keyErr => errors.Add(keyErr, ModelState[keyErr].Errors[0].ErrorMessage));

                return BadRequest(errors);
            }
            user.Login = user.Login.ToLower();

            ApplicationUser currentUser = _userManager.Users.FirstOrDefault(x => (x.Email == user.Login || x.Login == user.Login) && x.PasswordHash == user.PasswordHash);

            if (currentUser == null)
            {
                return BadRequest(new Error("Пользователь не найден или неверный пароль"));
            }

            var identity = GetIdentity(currentUser);

            var now = DateTime.UtcNow;

            //  JWT-токен
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetIssuerSigningKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);


            return Ok(new ViewUser()
            {
                Token = encodedJwt,
                Id = currentUser.Id,
                Name = currentUser.UserName,
                Surname = currentUser.Surname,
                Patronymic = currentUser.Patronymic,
                PhoneNumber = currentUser.PhoneNumber,
                Email = currentUser.Email,
                Reminder = currentUser.Reminder
            });
        }

        /// <summary>
        /// Метод проверки jwt токена 
        /// Необходим при открытии страниц без запросов, с сохранённым пользователем в localeStorage
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost("check")]
        // POST api/Tokens/check
        public IActionResult CheckTokenJWT() {
            return Ok(true);
        }

        private ClaimsIdentity GetIdentity(ApplicationUser user)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id),
                    new Claim("UserName", user.UserName?? user.Login),
                    new Claim("Login", user.Login),
                };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token",
                ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }
    }
}
