﻿using CCH.Common.Entities.Form;
using CCH.Common.Exceptions;
using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //[Produces("application/json")]
    [Route("api/appointment")]
    public class AppointmentController : BaseController
    {
        private readonly IAppointmentRepository _appointmentRepository;

        public AppointmentController(IAppointmentRepository appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
        }

        // GET api/appointment
        [HttpGet]
        public IActionResult Get([FromQuery] DateTime date, [FromQuery] int idDoctor = -1, [FromQuery] int idClinic = -1, [FromQuery] bool isOnlyActiveStatus = false)
        {
            return Ok(_appointmentRepository.Get(date, idDoctor, idClinic, isOnlyActiveStatus));
        }

        // GET api/appointment/patient/my
        [HttpGet("patient/my")]
        public IActionResult Get([FromQuery] bool isOnlyActiveStatus = false, [FromQuery] bool isOnlyNotPassed = false)
        {
            string idPatient = User?.Identity?.Name;

            if(idPatient == null)
            {
                throw new UnauthorizedAccessException();
            }

            return Ok(_appointmentRepository.GetByIdUser(idPatient, isOnlyActiveStatus, isOnlyNotPassed));
        }

        // POST api/appointment/create
        [HttpPost("create")]
        public IActionResult Create([FromBody] FormAppointment form)
        {
            if (form == null)
            {
                throw new AppException("Выбранное время на приём не указано!");
            }

            string idPatient = User?.Identity?.Name;
            var appointment = _appointmentRepository.Add(idPatient, form);

            return Ok(appointment);
        }

        // POST api/appointment/cancel
        [HttpPost("cancel")]
        public IActionResult Cancel([FromQuery] long idAppointment)
        {
            string idPatient = User?.Identity?.Name;
            return Ok(_appointmentRepository.Cancel(idAppointment, idPatient));
        }
    }
}