﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/Reminder")]
    public class ReminderController : BaseController
    {
        private readonly IReminderRepository _reminderRepository;

        public ReminderController(IReminderRepository reminderRepository)
        {
            _reminderRepository = reminderRepository;
        }


        /// <summary>
        /// Получение своих настроек напоминаний
        /// </summary>
        /// <returns></returns>
        [HttpGet("my")]
        public IActionResult GetMy()
        {
            string idPatient = User?.Identity?.Name;
            if (idPatient == null)
                throw new UnauthorizedAccessException();

            //получаем Reminder по id пользователя 
            return Ok(_reminderRepository.GetByUserId(idPatient));
        }

        /// <summary>
        /// Изменение своих настроек напоминаний
        /// </summary>
        /// <returns></returns>
        [HttpPost("my/change")]
        public IActionResult Change([FromBody] Reminder form)
        {
            string idPatient = User?.Identity?.Name;
            if (idPatient == null)
                throw new UnauthorizedAccessException();

            //получаем Reminder по id пользователя 
            Reminder reminder = _reminderRepository.GetByUserId(idPatient);

            //сохраняем изменения с сервера
            var id = reminder.ID;
            reminder = form;
            reminder.ID = id;
            reminder.IdUser = idPatient;
            _reminderRepository.Update(reminder);

            return Ok(true);
        }
    }
}
