﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CCH.Common.Exceptions;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/clinics")]
    public class ClinicsController : BaseController
    {
        private readonly IClinicRepository _clinicRepository;

        public ClinicsController(IClinicRepository clinicRepository)
        {
            _clinicRepository = clinicRepository;
        }

        // GET api/clinics
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_clinicRepository.ListAll());
        }

        // GET api/clinics/{id}
        [HttpGet("{id}")]
        public IActionResult Get([FromRoute]long id)
        {
            return Ok(_clinicRepository.GetById(id));
        }

        [HttpPost("{update}")]
        public IActionResult UpdateClinics([FromBody] Clinic data)
        {
            if (data == null)
                throw new AppException("Данные клиники не заполнены");
            if (data.ID > 0)
                _clinicRepository.Update(data);
            else
                _clinicRepository.Add(data);
            return Ok(data);
        }
    }
}