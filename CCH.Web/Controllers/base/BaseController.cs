﻿using CCH.Common.Entities.View;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CCH.Web.Controllers
{
    public class BaseController : Controller
    {

        //
        // Сводка:
        //     Creates a Microsoft.AspNetCore.Mvc.OkResult object that produces an empty Microsoft.AspNetCore.Http.StatusCodes.Status200OK
        //     response.
        //
        // Возврат:
        //     The created Microsoft.AspNetCore.Mvc.OkResult for the response.
        [NonAction]
        public new OkObjectResult Ok()
        {
            return base.Ok(new OkResult());
        }

        //
        // Сводка:
        //     Creates an Microsoft.AspNetCore.Mvc.OkObjectResult object that produces an Microsoft.AspNetCore.Http.StatusCodes.Status200OK
        //     response.
        //
        // Параметры:
        //   value:
        //     The content value to format in the entity body.
        //
        // Возврат:
        //     The created Microsoft.AspNetCore.Mvc.OkObjectResult for the response.
        [NonAction]
        public override OkObjectResult Ok(object value)
        {
            return base.Ok(new OkResult(value));
        }

        //
        // Сводка:
        //     Creates an Microsoft.AspNetCore.Mvc.BadRequestResult that produces a Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest
        //     response.
        //
        // Возврат:
        //     The created Microsoft.AspNetCore.Mvc.BadRequestResult for the response.
        [NonAction]
        public virtual BadRequestObjectResult BadRequest(Error error)
        {
            return base.BadRequest(new ErrorResult { ErrorMessage = error.ErrorMessage, ErrorData = error });
        }

        //
        // Сводка:
        //     Creates an Microsoft.AspNetCore.Mvc.BadRequestResult that produces a Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest
        //     response.
        //
        // Возврат:
        //     The created Microsoft.AspNetCore.Mvc.BadRequestResult for the response.
        [NonAction]
        public virtual BadRequestObjectResult BadRequest(Dictionary<string, string> error)
        {
            return base.BadRequest(new ErrorResult { KeyErrors = error });
        }
    }

    class OkResult : Result
    {
        public object Data = null;

        public OkResult(): base(true) { }
        public OkResult(object data) : this()
        {
            Data = data;
        }
    }

    class ErrorResult : Result
    {
        public string ErrorMessage;
        public object ErrorData;
        public Dictionary<string, string> KeyErrors = new Dictionary<string, string>();

        public ErrorResult() : base(false) { }
    }

    class Result
    {
        public readonly bool IsSuccess;

        public Result(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }
    }
}
