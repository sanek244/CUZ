﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/ClinicSpecializations")]
    public class ClinicSpecializationsController : BaseController
    {
        private readonly IClinicSpecializationRepository _clinicSpecializationRepository;

        public ClinicSpecializationsController(IClinicSpecializationRepository clinicSpecializationRepository)
        {
            _clinicSpecializationRepository = clinicSpecializationRepository;
        }


        // GET api/ClinicSpecializations
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ClinicSpecialization> clinicSpecializations = _clinicSpecializationRepository.ListAll();
            List<ClinicSpecialization> test = clinicSpecializations.ToList();
            return Ok(clinicSpecializations);
        }
    }
}