﻿using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/DoctorSpecialities")]
    public class DoctorSpecialitiesController : BaseController
    {
        private readonly IDoctorSpecialitiesRepository _doctorSpecialitiesRepository;

        public DoctorSpecialitiesController(IDoctorSpecialitiesRepository doctorSpecialitiesRepository)
        {
            _doctorSpecialitiesRepository = doctorSpecialitiesRepository;
        }

        // GET api/DoctorSpeciality
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_doctorSpecialitiesRepository.ListAll());
        }
    }
}