﻿using AutoMapper;
using CCH.Common.Entities;
using CCH.Common.Entities.Form;
using CCH.Common.Entities.View;
using CCH.Common.Exceptions;
using CCH.Common.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CCH.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/Doctors")]
    public class DoctorsController : BaseController
    {
        private readonly IDoctorRepository _doctorRepository;

        public DoctorsController(IDoctorRepository doctorRepository)
        {
            _doctorRepository = doctorRepository;
        }

        // GET api/doctors/{id}
        [HttpGet("{id}")]
        public IActionResult Get([FromRoute]long id)
        {
            return Ok(_doctorRepository.GetById(id));
        }

        // POST api/doctors/search/
        [HttpPost("search")]
        public IActionResult GetSearchResults([FromBody] FormSearchDoctors form)
        {
            return Ok(_doctorRepository.Search(form));
        }


        // POST api/doctors/schedule/
        [HttpGet("schedule")]
        public IActionResult GetSchedule([FromQuery] long idDoctor, [FromQuery] long idClinic)
        {
            return Ok(_doctorRepository.GetSchedule(idDoctor, idClinic));
        }


        // POST api/doctors/clinics/
        [HttpGet("clinics")]
        public IActionResult GetClinics([FromQuery] long idDoctor, [FromQuery] bool isOnlyWithSheduleItems = false)
        {
            return Ok(Mapper.Map<IEnumerable<ClinicMin>>(_doctorRepository.GetClinics(idDoctor, isOnlyWithSheduleItems)));
        }

        // POST api/doctors/busy_days/
        /// <summary>
        /// Дни, в которых все записи на приём уже заняты
        /// </summary>
        /// <param name="idDoctor"></param>
        /// <param name="idClinic"></param>
        /// <returns></returns>
        [HttpGet("busy_days")]
        public IActionResult GetBusyDays([FromQuery] long idDoctor, [FromQuery] long idClinic)
        {
            return Ok(_doctorRepository.GetBusyDays(idDoctor, idClinic));
        }

        [HttpPost("{update}")]
        public IActionResult Update([FromBody] FormDoctorEdit data)
        {
            if (data == null)
                throw new AppException("Данные врача не заполнены");

            Doctor doctor = Mapper.Map<Doctor>(data);

            if (data.ID > 0)
                _doctorRepository.Update(doctor);
            else
                _doctorRepository.Add(doctor);

            return Ok(data);
        }
    }
}