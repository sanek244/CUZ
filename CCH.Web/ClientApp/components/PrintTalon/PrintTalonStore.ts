﻿import { Action, Reducer } from 'redux';

import Clinic from "../../models/Clinic";
import Doctor from "../../models/Doctor";
import Appointment from "../../models/Appointment";
import { AppThunkAction } from "../../store";

// STATE

export interface PrintTalonState {
    appointment?: Appointment,
    isClose?: boolean
}

// ACTIONS
interface CloseAction { type: 'PRINT_TALON__CLOSE' }
interface OpenAction { type: 'PRINT_TALON__OPEN', appointment: Appointment}

type KnownAction = CloseAction | OpenAction;

// ACTION CREATORS
//for TypeScript
export interface PrintTalonAction {
    close?: () => CloseAction;
    print?: () => OpenAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'PRINT_TALON__CLOSE' },
    print: (appointment: Appointment) => <OpenAction>{ type: 'PRINT_TALON__OPEN', appointment: appointment }
};

// REDUCER 
export const reducer: Reducer<PrintTalonState> = (state: PrintTalonState, action: KnownAction) => {
    const stateNew: PrintTalonState = {
        isClose: state ? state.isClose : true,
        appointment: state ? state.appointment : new Appointment(),
    }

    switch (action.type) {
        case 'PRINT_TALON__CLOSE':
            stateNew.isClose = true;
            break;

        case 'PRINT_TALON__OPEN':
            stateNew.isClose = false;
            stateNew.appointment = action.appointment;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
