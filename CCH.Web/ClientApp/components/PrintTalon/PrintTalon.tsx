﻿import * as React from 'react';
import { connect } from "react-redux";
import * as moment from 'moment';

import BaseComponent from "../../models/BaseComponent";
import App from "../../App";
import { ApplicationState } from '../../store';
import * as PrintTalonStore from './PrintTalonStore';
import Alert from "../Alert/Alert";

import './PrintTalon.less';

type Props =
    PrintTalonStore.PrintTalonState
    & PrintTalonStore.PrintTalonAction
    & {};

/**
 * Распечатка талона
 */
class PrintTalon extends BaseComponent<Props> {

    public Name: string = 'PrintTalon';


    componentDidUpdate() {
        if (this.props.isClose || !this.props.appointment) {
            return null;
        }

        const el = document.getElementById(this.Name);
        var mywindow = window.open('', 'PRINT', 'height=500,width=750');

        if (el && mywindow) {
            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(el.innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

            App.Store.dispatch(PrintTalonStore.actionCreators.close());
        }
        else {
            Alert.Open('Не удалось распечатать документ!');
        }
    }

    public render() {
        if (this.props.isClose || !this.props.appointment) {
            return null;
        }

        return <div className={this.getClassName()} id={this.getClassName()}>
            <div style={{ width: '350px', border: '1px solid gray', padding: '2px', marginBottom: '5px' }}>
                <div style={{ textAlign: 'center', fontSize: '18px', marginBottom: '10px' }}>
                    <strong>Талон на прием к врачу</strong>
                </div>

                <style dangerouslySetInnerHTML={{
                    __html: `
                      .` + this.getClassName('info-value') + ` { font-size: 13px }
                    `}} />

                <strong>Поликлиника: </strong>
                <span className={this.getClassName('info-value')}>{this.props.appointment.CLINIC.FULL_NAME}</span>
                <br />

                <strong>Адрес: </strong>
                <span className={this.getClassName('info-value')}>{this.props.appointment.CLINIC.ADDRESS_DESCRIPTION}</span>
                <br />

                <strong>Пациент: </strong>
                <span className={this.getClassName('info-value')}>{App.user ? (App.user.Surname + ' ' + App.user.Name[0].toUpperCase() + '.' + App.user.Patronymic[0].toUpperCase() + '.') : ''}</span>
                <br />

                <strong>Врач: </strong>
                <span className={this.getClassName('info-value')}>{this.props.appointment.DOCTOR.DOCTOR_F + ' ' + this.props.appointment.DOCTOR.DOCTOR_I + ' ' + this.props.appointment.DOCTOR.DOCTOR_O}</span>
                <br />

                <strong>Дата: </strong>
                <span className={this.getClassName('info-value')}>{moment(this.props.appointment.FROM).format('DD MMMM YYYYг., dddd')}</span>
                <br />

                <strong>Время: </strong>
                <span className={this.getClassName('info-value')}>{moment(this.props.appointment.FROM).format('HH:mm')}</span>
                <br />

                <strong>Кабинет: </strong>
                <span className={this.getClassName('info-value')}>{this.props.appointment.CABINET}</span>
                <br />

                <div style={{ textAlign: 'center', fontSize: '18px', marginTop: '10px' }}>
                    <strong>Пожалуйста, не опаздывайте.</strong>
                </div>
            </div>
            Талон сформирован: {moment().format('DD MMMM YYYYг., HH:mm.')}
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.printTalon,
    PrintTalonStore.actionCreators
)(PrintTalon) as typeof PrintTalon;
