﻿import { Reducer } from "redux";
import { addTask } from "domain-task";
import App from "../../App";

import Clinic from "../../models/Clinic";
import ClinicSpecialization from "../../models/ClinicSpecialization";
import { AppThunkAction } from "../../store";
import ServiceController from "../../controllers/ServiceController";
import * as AlertStore from "../Alert/AlertStore";
import Alert from "../Alert/Alert";
import ClinicEditFormError from "../../models/forms/errors/ClinicEditFormError";
import * as ClinicsCrudPageStore from "../../pages/ClinicsCrud/ClinicsCrudPageStore";

// STATE
export interface ClinicEditState {
    isClose: boolean;
    clinicSpecializations: ClinicSpecialization[],
    clinic: Clinic,
    errors: ClinicEditFormError,
}

// ACTIONS
interface CloseAction {
    type: "ClinicEdit__CLOSE"
}

interface OpenAction {
    type: "ClinicEdit__OPEN",
    clinicSpecializations: ClinicSpecialization[],
    clinic: Clinic
}

interface GetErrorsAction {
    type: "ClinicEdit__GET_ERRORS",
    errors: ClinicEditFormError,
}

type KnownAction = CloseAction | OpenAction | GetErrorsAction

// ACTION CREATORS
export const actionCreators = {

    //закрыть окно
    close: () => <CloseAction>{ type: "ClinicEdit__CLOSE" },

    //открыть окно
    open: (clinic: Clinic, clinicSpecializations: ClinicSpecialization[]) =>
        <OpenAction>{ type: "ClinicEdit__OPEN", clinicSpecializations: clinicSpecializations, clinic: clinic },

    //отправка запроса изменения/создания клиники
    requestCreateClinic: (clinica: Clinic): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const errors = new ClinicEditFormError();
        let isHasError = false;

        if (!clinica.FULL_NAME) {
            errors.FullName = "Поле не заполнено!";
            isHasError = true;
        }

        if (!clinica.NAME) {
            errors.Name = "Поле не заполнено!";
            isHasError = true;
        }

        if (!clinica.PHONE_REGISTRY) {
            errors.PhoneRegistry = "Поле не заполнено!";
            isHasError = true;
        }

        if (!clinica.INN) {
            errors.INN = "Поле не заполнено!";
            isHasError = true;
        }

        if (!clinica.ADDRESS_SHORT) {
            errors.AddressShort = "Поле не заполнено!";
            isHasError = true;
        }

        if (clinica.ID_CLINIC_SPECIALIZATION <= 0) {
            errors.Specialization = "Специализация не выбрана!";
            isHasError = true;
        }

        if (isHasError) {
            dispatch({ type: "ClinicEdit__GET_ERRORS", errors: errors });
            return;
        }

        const promise = ServiceController.sendUpdateClinic(clinica)
            .then((data: any) => {
                Alert.Open("Данные успешно обновлены!", AlertStore.HeaderState.success);
                //обновляем клиники
                App.Store.dispatch(ClinicsCrudPageStore.actionCreators.requestClinics(true));
            });

        addTask(promise);
    }
};

// REDUCER 
export const reducer: Reducer<ClinicEditState> = (state: ClinicEditState, action: KnownAction) => {
    const stateNew: ClinicEditState = {
        isClose: state ? state.isClose : true,
        clinicSpecializations: state ? state.clinicSpecializations : [],
        clinic: state ? state.clinic : new Clinic(),
        errors: state ? state.errors : new ClinicEditFormError(),
    };

    switch (action.type) {
        case "ClinicEdit__CLOSE":
            stateNew.isClose = true;
            stateNew.clinic = new Clinic();
            break;

        case "ClinicEdit__OPEN":
            stateNew.isClose = false;
            stateNew.clinicSpecializations = action.clinicSpecializations;
            stateNew.clinic = action.clinic;
            break;

        case "ClinicEdit__GET_ERRORS":
            stateNew.errors = action.errors;
            break;

        default:
            const exhaustiveCheck = action;
    }

    return stateNew;
};