﻿import * as React from "react";
import { connect } from "react-redux";

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from "../../store";
import * as ClinicEditStore from "./ClinicEditStore";
import App from "../../App";
import Clinic from "../../models/Clinic";

import "./ClinicEdit.less";

type Props =
    ClinicEditStore.ClinicEditState
    & typeof ClinicEditStore.actionCreators
    & {};

/**
 * Модальное окно редактирования данных клиники
 */
class ClinicEdit extends BaseComponent<Props> {

    Name = "ClinicEdit";

    //Инпуты (для редактирования клиники)
    inputIdClinicSpecialization: HTMLSelectElement | null = null;

    clinic: Clinic;

    render() {
        let isAdmin = App.user ? App.user.IsAdmin : false;

        if (this.props.isClose || !isAdmin) {
            return null;
        }

        this.clinic = new Clinic(this.props.clinic);
        let title = this.clinic.ID > 0
            ? `Редактирование клиники ${this.clinic.NAME}.`
            : "Создание новой клиники.";

        return <div className={`modal show ${this.getClassName()}`} {...{ tabindex: "-1", role: "dialog" }}>
            <div className="modal-dialog" role="document">
                <div className="content">
                    <div className={this.getClassName("header")}>
                        <h5 className={this.getClassName("header-title")}>{title}</h5>
                    </div>
                    <div className={this.getClassName("modal")}>
                        {this.renderModalClinicSub("Полное наименование*:",
                            this.clinic.FULL_NAME,
                            this.props.errors.FullName,
                            false,
                            (e: any) => { this.clinic.FULL_NAME = e.target.value; })}
                        {this.renderModalClinicSub("Краткое наименование*:",
                            this.clinic.NAME,
                            this.props.errors.Name,
                            false,
                            (e: any) => { this.clinic.NAME = e.target.value; })}
                        <div className={this.getClassName("modal-section")}>
                            <label className={this.getClassName("modal-section-label")}>Специализация</label>
                            <select
                                className={this.getClassName("modal-section-select")}
                                ref={(input: any) => { this.inputIdClinicSpecialization = input; }}>
                                <option value="">Все</option>
                                {this.props.clinicSpecializations
                                    .map(clinicSpeciality =>
                                        <option value={clinicSpeciality.ID}
                                            {...{
                                                selected: clinicSpeciality.ID === this.clinic.ID_CLINIC_SPECIALIZATION
                                            }}>
                                            {clinicSpeciality.NAME}
                                        </option>
                                    )}
                            </select>
                            <small className={this.getClassName("modal-section-error")}>{this.props.errors
                                .Specialization}</small>
                        </div>
                        {this.renderModalClinicSub("Адрес:",
                            this.clinic.ADDRESS_DESCRIPTION,
                            null,
                            false,
                            (e: any) => { this.clinic.ADDRESS_DESCRIPTION = e.target.value; })}
                        {this.renderModalClinicSub("Короткий адрес*:",
                            this.clinic.ADDRESS_SHORT,
                            this.props.errors.AddressShort,
                            false,
                            (e: any) => { this.clinic.ADDRESS_SHORT = e.target.value; })}
                        {this.renderModalClinicSub("Фамилия руководителя:",
                            this.clinic.LEADER_F,
                            null,
                            false,
                            (e: any) => { this.clinic.LEADER_F = e.target.value; })}
                        {this.renderModalClinicSub("Имя руководителя:",
                            this.clinic.LEADER_I,
                            null,
                            false,
                            (e: any) => { this.clinic.LEADER_I = e.target.value; })}
                        {this.renderModalClinicSub("Отчество руководителя:",
                            this.clinic.LEADER_O,
                            null,
                            false,
                            (e: any) => { this.clinic.LEADER_O = e.target.value; })}
                        {this.renderModalClinicSub("Телефон руководителя:",
                            this.clinic.PHONE_LEADER,
                            null,
                            false,
                            (e: any) => { this.clinic.PHONE_LEADER = e.target.value; })}
                        {this.renderModalClinicSub("Описание:",
                            this.clinic.DESCRIPTION,
                            null,
                            true,
                            (e: any) => { this.clinic.DESCRIPTION = e.target.value; })}
                        {this.renderModalClinicSub("Телефон регистратуры*:",
                            this.clinic.PHONE_REGISTRY,
                            this.props.errors.PhoneRegistry,
                            false,
                            (e: any) => { this.clinic.PHONE_REGISTRY = e.target.value; })}
                        {this.renderModalClinicSub("Факс:",
                            this.clinic.FAX,
                            null,
                            true,
                            (e: any) => { this.clinic.FAX = e.target.value; })}
                        {this.renderModalClinicSub("Электронная почта:",
                            this.clinic.EMAIL,
                            null,
                            true,
                            (e: any) => { this.clinic.EMAIL = e.target.value; })}
                        {this.renderModalClinicSub("Сайт:",
                            this.clinic.SITE,
                            null,
                            true,
                            (e: any) => { this.clinic.SITE = e.target.value; })}
                        {this.renderModalClinicSub("Бухгалтер:",
                            this.clinic.BOOKER,
                            null,
                            true,
                            (e: any) => { this.clinic.BOOKER = e.target.value; })}
                        {this.renderModalClinicSub("ИНН*:",
                            this.clinic.INN,
                            this.props.errors.INN,
                            false,
                            (e: any) => { this.clinic.INN = e.target.value; })}
                        {this.renderModalClinicSub("КПП:",
                            this.clinic.KPP,
                            null,
                            false,
                            (e: any) => { this.clinic.KPP = e.target.value; })}
                        {this.renderModalClinicSub("ОГРН:",
                            this.clinic.OGRN,
                            null,
                            false,
                            (e: any) => { this.clinic.OGRN = e.target.value; })}
                        {this.renderModalClinicSub("ОКТМО:",
                            this.clinic.OKTMO,
                            null,
                            false,
                            (e: any) => { this.clinic.OKTMO = e.target.value; })}
                        <br />
                        <label>* - поля, обязательные для заполенения</label>
                        <br />
                        <input
                            className={`btn text-white mt-auto ${this.getClassName("modal-section-button")}`}
                            type="submit"
                            value="Отправить"
                            onClick={() => this.requestUpdateClinic()} />

                        <button
                            className={`btn text-white mt-auto ${this.getClassName("modal-section-button")}`}
                            onClick={() => this.props.close()}>Закрыть</button>
                    </div>
                </div>
            </div>
        </div>;
    }

    renderModalClinicSub(caption: string, value: string, error: string | null, notSupported: boolean, a: (e: any) => void)
    {
        if (notSupported)
            return null;

        return <div className={this.getClassName("modal-section")}>
            <label className={this.getClassName("modal-section-label")}>{caption}</label>
            <input className={this.getClassName("modal-section-input")} type="text" onChange={a} defaultValue={value} />
            <small className={this.getClassName("modal-section-error")}>{error}</small>
        </div>;
    }

    requestUpdateClinic() {
        this.clinic.ID_CLINIC_SPECIALIZATION = this.inputIdClinicSpecialization ? +this.inputIdClinicSpecialization.value : -1;
        this.props.requestCreateClinic(this.clinic);
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.clinicEdit,
    ClinicEditStore.actionCreators
)(ClinicEdit) as typeof ClinicEdit;