﻿import { Action, Reducer } from 'redux';

import Helper from "../../Helper";


// STATE
export interface SelectState {
    values?: any
    valuesChanged?: any
}

// ACTIONS
interface ChangeAction { type: 'Select__CHANGE', value: string, id: string }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = ChangeAction | BagAction;

// ACTION CREATORS
//for TypeScript
export interface SelectAction {
    changeValue?: (id: string, value: string) => ChangeAction;
}
export const actionCreators = {
    changeValue: (id: string, value: string) => <ChangeAction>{ type: 'Select__CHANGE', value: value, id: id }
};

// REDUCER 
export const reducer: Reducer<SelectState> = (state: SelectState, action: KnownAction) => {
    const newState: SelectState = {
        values: state ? Helper.clone(state.values) : {},
        valuesChanged: state ? Helper.clone(state.valuesChanged) : {}
    }

    switch (action.type) {
        case 'Select__CHANGE':
            newState.values[action.id] = action.value;
            newState.valuesChanged[action.id] = true;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return newState;
};
