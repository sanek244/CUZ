﻿import * as React from 'react';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as SelectStore from './SelectStore';
import App from "../../App";


import './Select.less';

export interface Option {
    value: string,
    title: string
}

interface Prop {
    id: string
    value?: string,
    ref?: (input: HTMLSelectElement | null) => void,
    data?: Option[],
    defaultOptionTitle?: string
}

type Props =
    SelectStore.SelectState
    & SelectStore.SelectAction
    & Prop;

/**
 * Стилизованный checkbox
 */
class Select extends BaseComponent<Props> {

    public Name: string = 'Select-default';

    public static getValue(id: string, defaultValue: any) {
        const values = App.Store.getState().select.values;
        const valuesChanged = App.Store.getState().select.valuesChanged;

        return valuesChanged[id] ? values[id] : defaultValue
    }

    public get value() {
        return this.props.valuesChanged[this.props.id] ? this.props.values[this.props.id] : this.props.value;
    }

    public render() {
        const changeValue = this.props.changeValue;

        if (!changeValue) {
            return null;
        }

        const data = this.props.data ? this.props.data : [];
        
        return <div className={this.getClassName('')}>
            <select
                className={this.getClassName('input')}
                ref={this.props.ref ? this.props.ref : () => { }}
                onChange={(e) => this.props.changeValue ? this.props.changeValue(this.props.id, e.target.value) : () => { }}
            >
                {this.props.defaultOptionTitle ? <option>{this.props.defaultOptionTitle}</option> : null}

                {this.props.children
                    ? this.props.children
                    : data.map(dat => {
                        return <option
                            value={dat.value}
                            {...{ selected: this.value === dat.value }}
                        >
                            {dat.title}
                        </option>
                    })}
            </select>
        </div>;
    }
}


// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState, ownProps: Prop) => {
        return { ...ownProps, ...state.select  };
    },
    SelectStore.actionCreators
)(Select) as typeof Select;