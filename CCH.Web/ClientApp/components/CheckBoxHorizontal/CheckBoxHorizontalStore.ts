﻿import { Action, Reducer } from 'redux';

import Helper from "../../Helper";


// STATE
export interface CheckBoxHorizontalState {
    values?: any,
    valuesChanged?: any 
}

// ACTIONS
interface ChangeAction { type: 'CheckBoxHorizontal__CHANGE', value: boolean, id: string }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = ChangeAction | BagAction;

// ACTION CREATORS
//for TypeScript
export interface CheckBoxHorizontalAction {
    changeValue?: (id: string, value: boolean) => ChangeAction;
}
export const actionCreators = {
    changeValue: (id: string, value: boolean) => <ChangeAction>{ type: 'CheckBoxHorizontal__CHANGE', value: value, id: id }
};

// REDUCER 
export const reducer: Reducer<CheckBoxHorizontalState> = (state: CheckBoxHorizontalState, action: KnownAction) => {
    const newState: CheckBoxHorizontalState = {
        values: state ? Helper.clone(state.values) : {},
        valuesChanged: state ? Helper.clone(state.valuesChanged) : {}
    }

    switch (action.type) {
        case 'CheckBoxHorizontal__CHANGE':
            newState.values[action.id] = action.value;
            newState.valuesChanged[action.id] = true;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return newState;
};
