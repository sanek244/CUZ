﻿import * as React from 'react';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as CheckBoxHorizontalStore from './CheckBoxHorizontalStore';
import App from "../../App";



import './CheckBoxHorizontal.less';
interface Prop {
    id: string
    value?: boolean,
    ref?: (input: HTMLInputElement | null) => void
}

type Props =
    CheckBoxHorizontalStore.CheckBoxHorizontalState
    & CheckBoxHorizontalStore.CheckBoxHorizontalAction
    & Prop;

/**
 * Стилизованный checkbox
 */
class CheckBoxHorizontal extends BaseComponent<Props> {

    public Name: string = 'CheckBoxHorizontal';

    public static getValue(id: string, defaultValue: boolean) {
        const values = App.Store.getState().checkBoxHorizontal.values;
        const valuesChanged = App.Store.getState().checkBoxHorizontal.valuesChanged;

        return valuesChanged[id] ? values[id] : defaultValue
    }
    
    public get value() {
        return this.props.valuesChanged[this.props.id] ? this.props.values[this.props.id] : this.props.value;
    }

    public render() {
        const changeValue = this.props.changeValue;

        if (!changeValue) {
            return null;
        }

        return <span
            className={this.getClassName('')}
            onClick={() => changeValue(this.props.id, !this.value)}
        >
            <span className={this.getClassName('background', this.value ? 'active' : '')}>
                <span className={this.getClassName('point', this.value ? 'active' : '')}></span>
            </span>
            <input
                id={this.props.id || ''}
                type='checkbox'
                className={this.getClassName('input')}
                ref={this.props.ref ? this.props.ref : () => { }}
                checked={this.value}
            />
        </span>;
    }
}


// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState, ownProps: Prop) => {
        return { ...ownProps, ...state.checkBoxHorizontal  };
    },
    CheckBoxHorizontalStore.actionCreators
)(CheckBoxHorizontal) as typeof CheckBoxHorizontal;