﻿import * as React from "react";
import { connect } from "react-redux";

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from "../../store";
import * as DoctorEditStore from "./DoctorEditStore";
import App from "../../App";
import Doctor from "../../models/Doctor";
import DoctorEditModel from "../../models/forms/DoctorEdit";

import "./DoctorEdit.less";

type Props =
    DoctorEditStore.DoctorEditState
    & typeof DoctorEditStore.actionCreators
    & {};

/**
 * Модальное окно редактирования данных врача
 */
class DoctorEdit extends BaseComponent<Props> {

    Name = "DoctorEdit";

    //Инпуты (для редактирования клиники)
    inputIdSpeciality: HTMLSelectElement | null = null;

    doctor: DoctorEditModel;

    render() {
        let isAdmin = App.user ? App.user.IsAdmin : false;

        if (this.props.isClose || !isAdmin) {
            return null;
        }

        this.doctor = new DoctorEditModel(this.props.doctor);
        let title = this.doctor.ID > 0
            ? `Редактирование данных врача ${(new Doctor(this.props.doctor)).FIOFull}.`
            : "Добавление нового врача.";

        return <div className={`modal show ${this.getClassName()}`} {...{ tabindex: "-1", role: "dialog" }}>
            <div className="modal-dialog" role="document">
                <div className="content">
                    <div className={this.getClassName("header")}>
                        <h5 className={this.getClassName("header-title")}>{title}</h5>
                    </div>
                    <div className={this.getClassName("modal")}>
                        {this.renderModalClinicSub("Фамилия*:",
                            this.doctor.DOCTOR_F,
                            this.props.errors.DOCTOR_F,
                            (e: any) => { this.doctor.DOCTOR_F = e.target.value; })}
                        {this.renderModalClinicSub("Имя*:",
                            this.doctor.DOCTOR_I,
                            this.props.errors.DOCTOR_I,
                            (e: any) => { this.doctor.DOCTOR_I = e.target.value; })}
                        {this.renderModalClinicSub("Отчество:",
                            this.doctor.DOCTOR_O,
                            null,
                            (e: any) => { this.doctor.DOCTOR_O = e.target.value; })}

                        <div className={this.getClassName("modal-section")}>
                            <label className={this.getClassName("modal-section-label")}>Специальность</label>
                            <select
                                className={this.getClassName("modal-section-select")}
                                ref={(input: any) => { this.inputIdSpeciality = input; }}>
                                <option value="">Все</option>
                                {this.props.doctorSpecialty.map(doctorSpeciality =>
                                    <option
                                        value={doctorSpeciality.ID}
                                        {...{ selected: doctorSpeciality.ID === this.doctor.ID_DOCTOR_SPECIALITY }}
                                    >
                                        {doctorSpeciality.NAME}
                                    </option>
                                )}
                            </select>
                            <small className={this.getClassName("modal-section-error")}>
                                {this.props.errors.ID_DOCTOR_SPECIALITY}
                            </small>
                        </div>

                        {this.renderModalClinicSub("Первичный приём:",
                            this.doctor.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION ? '1' : '0',
                            null,
                            (e: any) => { this.doctor.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION = e.target.value; },
                            true
                        )}
                        <br />
                        <label>* - поля, обязательные для заполенения</label>
                        <br />
                        <input
                            className={`btn text-white mt-auto ${this.getClassName("modal-section-button")}`}
                            type="submit"
                            value="Отправить"
                            onClick={() => this.requestUpdateClinic()} />

                        <button
                            className={`btn text-white mt-auto ${this.getClassName("modal-section-button")}`}
                            onClick={() => this.props.close()}>Закрыть</button>
                    </div>
                </div>
            </div>
        </div>;
    }

    renderModalClinicSub(caption: string, value: string, error: string | null, a: (e: any) => void, isCheckbox: boolean = false)
    {
        return <div className={this.getClassName("modal-section")}>
            <label className={this.getClassName("modal-section-label")}>{caption}</label>
            <input
                className={this.getClassName("modal-section-" + (isCheckbox ? 'checkbox' : 'input'))}
                type={isCheckbox ? 'checkbox' : "text"}
                onChange={a}
                defaultValue={value} />
            <small className={this.getClassName("modal-section-error")}>{error}</small>
        </div>;
    }

    requestUpdateClinic() {
        this.doctor.ID_DOCTOR_SPECIALITY = this.inputIdSpeciality ? parseInt(this.inputIdSpeciality.value) : -1;
        this.props.requestUpdateDoctor(this.doctor);
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.doctorEdit,
    DoctorEditStore.actionCreators
)(DoctorEdit) as typeof DoctorEdit;