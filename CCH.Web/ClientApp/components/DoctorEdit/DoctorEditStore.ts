﻿import { Reducer } from "redux";
import { addTask } from "domain-task";
import App from "../../App";

import Doctor from "../../models/Doctor";
import DoctorEdit from "../../models/forms/DoctorEdit";
import DoctorSpecialty from "../../models/DoctorSpecialty";
import { AppThunkAction } from "../../store";
import ServiceController from "../../controllers/ServiceController";
import * as AlertStore from "../Alert/AlertStore";
import Alert from "../Alert/Alert";
import DoctorEditFormError from "../../models/forms/errors/DoctorEditFormError";
import * as SearchDoctorsFormStore from "../SearchDoctorsForm/SearchDoctorsFormStore";

// STATE
export interface DoctorEditState {
    isClose: boolean;
    doctorSpecialty: DoctorSpecialty[],
    doctor: Doctor,
    errors: DoctorEditFormError,
}

// ACTIONS
interface CloseAction {
    type: "DoctorEdit__CLOSE"
}

interface OpenAction {
    type: "DoctorEdit__OPEN",
    doctorSpecialty: DoctorSpecialty[],
    doctor: Doctor
}

interface GetErrorsAction {
    type: "DoctorEdit__GET_ERRORS",
    errors: DoctorEditFormError,
}

type KnownAction = CloseAction | OpenAction | GetErrorsAction

// ACTION CREATORS
export const actionCreators = {

    //закрыть окно
    close: () => <CloseAction>{ type: "DoctorEdit__CLOSE" },

    //открыть окно
    open: (doctor: Doctor, doctorSpecialty: DoctorSpecialty[]) =>
        <OpenAction>{ type: "DoctorEdit__OPEN", doctorSpecialty: doctorSpecialty, doctor: doctor },

    //отправка запроса изменения/создания врача
    requestUpdateDoctor: (doctor: DoctorEdit): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const errors = new DoctorEditFormError();
        let isHasError = false;

        if (!doctor.DOCTOR_F) {
            errors.DOCTOR_F = "Поле не заполнено!";
            isHasError = true;
        }

        if (!doctor.DOCTOR_I) {
            errors.DOCTOR_I = "Поле не заполнено!";
            isHasError = true;
        }

        if (doctor.ID_DOCTOR_SPECIALITY < 0) {
            errors.ID_DOCTOR_SPECIALITY = "Поле не заполнено!";
            isHasError = true;
        }

        /*if (doctor.ID_POSITION < 0) {
            errors.ID_POSITION = "Поле не заполнено!";
            isHasError = true;
        }

        if (doctor.ID_QUALIFICATION_CATEGORY < 0) {
            errors.ID_QUALIFICATION_CATEGORY = "Поле не заполнено!";
            isHasError = true;
        }*/

        if (isHasError) {
            dispatch({ type: "DoctorEdit__GET_ERRORS", errors: errors });
            return;
        }

        const promise = ServiceController.sendUpdateDoctor(doctor)
            .then((data: any) => {
                Alert.Open("Данные успешно обновлены!", AlertStore.HeaderState.success);
                //обновляем результаты поиска
                App.Store.dispatch(SearchDoctorsFormStore.actionCreators.updateSearchResults());
            });

        addTask(promise);
    }
};

// REDUCER 
export const reducer: Reducer<DoctorEditState> = (state: DoctorEditState, action: KnownAction) => {
    const stateNew: DoctorEditState = {
        isClose: state ? state.isClose : true,
        doctorSpecialty: state ? state.doctorSpecialty : [],
        doctor: state ? state.doctor : new Doctor(),
        errors: state ? state.errors : new DoctorEditFormError(),
    };

    switch (action.type) {
        case "DoctorEdit__CLOSE":
            stateNew.isClose = true;
            stateNew.doctor = new Doctor();
            break;

        case "DoctorEdit__OPEN":
            stateNew.isClose = false;
            stateNew.doctorSpecialty = action.doctorSpecialty;
            stateNew.doctor = action.doctor;
            break;

        case "DoctorEdit__GET_ERRORS":
            stateNew.errors = action.errors;
            break;

        default:
            const exhaustiveCheck = action;
    }

    return stateNew;
};