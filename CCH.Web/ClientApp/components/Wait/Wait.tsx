import * as React from 'react';

import BaseComponent from "../../models/BaseComponent";
import Helper from "../../Helper";
import App from "../../App";

import './Wait.less';

interface Prop {
    isVisible: boolean
}

/**
 * ������� ��������
 */
export default class Wait extends BaseComponent<Prop> {

    public Name: string = 'Wait';

    wall: HTMLElement | null;
    img: HTMLElement | null;

    public componentDidUpdate() {
        //������� ��������� ��������
        setTimeout(() => { //��� setTimeout �������� �� ��������� - ����� ��������� �� �������� ������������

            if (this.wall) {
                this.wall.style.opacity = '1';
            }

            if (this.img) {
                this.img.style.opacity = '1';
            }

        }, 25);
    }

    public render() {
        //�� ���������� ? 
        if (!this.props.isVisible) {

            //������� �� ����� ���������� ��������
            return <div> {this.props.children} </div>;
        }

        return <div className={this.getClassName()}>
            <div
                className={this.getClassName('wall')}
                ref={el => this.wall = el}
            />

            {this.props.children}

            <i
                className={"fa fa-spinner fa-pulse " + this.getClassName('img')}
                ref={el => this.img = el}
            />
        </div>;
    }
}