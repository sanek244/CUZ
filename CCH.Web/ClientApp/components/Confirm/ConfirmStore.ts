import { Action, Reducer } from 'redux';

export interface ButtonItem {
    label: string,
    callback?: Function,
    title?: string
}

// STATE
export interface ConfirmState {
    isClose?: boolean;
    text?: string,
    title?: string,
    buttons?: ButtonItem[]
}

// ACTIONS
interface CloseAction { type: 'Confirm__CLOSE' }
interface OpenAction { type: 'Confirm__OPEN', text: string, title: string, buttons: ButtonItem[] }

type KnownAction = CloseAction | OpenAction;

// ACTION CREATORS
//for TypeScript
export interface ConfirmAction {
    close?: () => CloseAction;
    open?: () => OpenAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'Confirm__CLOSE' },
    open: (text: string = '', buttons?: ButtonItem[], title?: string) => <OpenAction>{ type: 'Confirm__OPEN', text: text, buttons: buttons, title: title }
};

// REDUCER 
export const reducer: Reducer<ConfirmState> = (state: ConfirmState, action: KnownAction) => {
    const stateNew: ConfirmState = {
        isClose: state ? state.isClose : true,
        text: state ? state.text : '',
        title: state ? state.title : '',
        buttons: state ? state.buttons : []
    }

    switch (action.type) {
        case 'Confirm__CLOSE':
            stateNew.isClose = true;
            stateNew.title = '';
            break;

        case 'Confirm__OPEN':
            stateNew.isClose = false;
            stateNew.text = action.text;
            stateNew.title = action.title;
            stateNew.buttons = action.buttons;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
