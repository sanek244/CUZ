﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as ConfirmStore from './ConfirmStore';
import App from "../../App";

import './Confirm.less';

type Props =
    ConfirmStore.ConfirmState
    & ConfirmStore.ConfirmAction
    & {};

/**
 * Модальное окно с выбором действия
 */
class Confirm extends BaseComponent<Props> {

    public Name: string = 'Confirm';

    public static Open(text: string = '', buttons?: ConfirmStore.ButtonItem[], title?: string) {
        App.Store.dispatch(ConfirmStore.actionCreators.open(text, buttons, title));
    }

    public static Close() {
        App.Store.dispatch(ConfirmStore.actionCreators.close());
    }

    public render() {
        if (this.props.isClose || !this.props.buttons) {
            return null;
        }

        return <div>
            <div className={"modal " + this.getClassName()} {...{ tabIndex: -1 }} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className={"modal-header " + this.getClassName('header')}>
                            <span className="modal-title">{this.props.title || 'Вопрос'}</span>
                            <button type="button" className="close" aria-label="Close" onClick={this.props.close}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{this.props.text}</p>
                        </div>
                        <div className="modal-footer">
                            {this.props.buttons.map((button: ConfirmStore.ButtonItem) => {
                                const onClick = () => {
                                    if (this.props.close) {
                                        this.props.close();
                                    }

                                    if (button.callback) {
                                        button.callback();
                                    }
                                };

                                return <button
                                    type="button"
                                    className={"btn btn-primary " + this.getClassName('button')}
                                    onClick={onClick}
                                    dangerouslySetInnerHTML={{ __html: button.label }}
                                    title={button.title}
                                >
                                </button>;
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop fade in"></div>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.confirm,
    ConfirmStore.actionCreators
)(Confirm) as typeof Confirm;
