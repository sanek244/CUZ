﻿import * as React from 'react';

import BaseComponent from "../../models/BaseComponent";

import './Feedback.less';


/**
 * Форма обратной связи
 */
export default class Feedback extends BaseComponent<{}> {

    public Name: string = 'Feedback';


    public render() {
        return <div className={this.getClassName('')}>
            <div className={'row ' + this.getClassName('line-data')}>
                <input type="text" className={"form-control col-sm-3 " + this.getClassName('input-data')} placeholder="Введите ваше имя"/>
                <input type="text" className={"form-control col-sm-3 " + this.getClassName('input-data', 'offset')} placeholder="Введите ваш e-mail"/>
                <input type="text" className={"form-control col-sm-3 " + this.getClassName('input-data', 'offset')} placeholder="Ваш номер телефона" />
            </div>
            <textarea className={'form-control ' + this.getClassName('input-data')} placeholder="Текст сообщения" rows={3} />
            <button type="button" className={'btn btn-primary ' + this.getClassName('button-send')}>Отправить</button>
        </div>;
    }
}
