import * as React from 'react';
import * as Redux from 'redux';

import BaseComponent from "../../models/BaseComponent";
import EvaluationService from '../EvaluationService/EvaluationService';
import Feedback from '../Feedback/Feedback';
import * as AppStore from "../../store/AppStore";
import App from "../../App";

import './Footer.less';

/**
 * Нижняя область экрана
 */
export default class Footer extends BaseComponent<{}> {

    public Name: string = 'Footer';

    componentDidMount() {
        //подписываемся на изменения пользователя
        AppStore.listChangeUserCallbacks.push(() => this.forceUpdate());
    }

    public render() {
        if (!App.user) {
            return <div className={this.getClassName()}></div>;
        }

        return <div className={this.getClassName()}>
            <div className='container'>
                <div className='row'>
                    <div className='col-sm-8'>
                        <Feedback />
                    </div>
                    <div className='col-sm-4'>
                        <EvaluationService />
                    </div>
                </div>
            </div>
        </div>;
    }
}
