﻿import * as React from 'react';
import { Link } from "react-router-dom";

import BaseComponent from "../../models/BaseComponent";
import App from "../../App";
import User from "../../models/User";
import * as AuthorizationPageStore from "../../pages/Authorization/AuthorizationPageStore";
import * as AppStore from "../../store/AppStore";

import './Header.less';

/**
 * Верхняя область экрана
 */
export default class Header extends BaseComponent<{}> {

    public Name: string = 'Header';


    componentDidMount() {
        //подписываемся на изменения пользователя
        AppStore.listChangeUserCallbacks.push(() => this.forceUpdate());
    }

    public render() {
        return <div className={this.getClassName()}>
            {App.user
                ? this.renderAuthorizationUserContent()
                : this.renderGuestContent()}
        </div>;
    }

    public renderGuestContent() {
        return <div className={this.getClassName('guest-header')}>ЦУЗ</div>
    }

    public renderAuthorizationUserContent() {
        const user: User = App.user ? App.user : new User();

        return <div className='container'>
            <div className={this.getClassName('block-logo')}>
                <Link to='/' className={this.getClassName('link')}>
                    <span className="">Лого</span>
                </Link>
            </div>
            <div className="d-flex">
                <div className={this.getClassName('block-user')}>
                    <Link to='/cabinet'>
                        <img src="media/no_avatar.png" />
                        <span className={"my-auto " + this.getClassName('block-text')}>{user.Surname + ' ' + user.Name + ' ' + user.Patronymic}</span>
                    </Link>
                </div>

                <div>
                    <Link to='/calendar' className={this.getClassName('link')}>
                        <img src="media/ico_calendar.png" alt="Календарь клиента" />
                        <span className={'my-auto ' + this.getClassName('block-text') + ' ' + this.getClassName('block-calendar-text')}>Календарь клиента</span>
                    </Link>
                </div>

                <div className={this.getClassName('block-exit')}>
                    <Link to='/login' onClick={() => App.Store.dispatch(AuthorizationPageStore.actionCreators.requestLogout())}>
                        <span className={this.getClassName('block-exit-text')}>Выход</span>
                        <img src="media/ico_exit.png" alt="Выход" />
                    </Link>
                </div>
            </div>
        </div>;
    }
}
