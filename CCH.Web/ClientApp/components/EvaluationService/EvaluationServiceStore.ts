import { Action, Reducer } from 'redux';

// STATE
export interface EvaluationServiceState {
    isSelectQualityServiceScore?: boolean; //��� ���� ������� ������ �������� �������? (� ������� ��������� ��������)
}

// ACTIONS
interface SelectAction { type: 'EvaluationService__SELECT' }
interface OffSelectAction { type: 'EvaluationService__OFF_SELECT' }

type KnownAction = SelectAction | OffSelectAction;

// ACTION CREATORS
//for TypeScript
export interface EvaluationServiceAction {
    select?: () => SelectAction;
    offSelect?: () => OffSelectAction;
}
export const actionCreators = {
    select: () => <SelectAction>{ type: 'EvaluationService__SELECT' },
    offSelect: () => <OffSelectAction>{ type: 'EvaluationService__OFF_SELECT' }
};

// REDUCER 
export const reducer: Reducer<EvaluationServiceState> = (state: EvaluationServiceState, action: KnownAction) => {
    switch (action.type) {
        case 'EvaluationService__SELECT':
            return { isSelectQualityServiceScore: true };
        case 'EvaluationService__OFF_SELECT':
            return { isSelectQualityServiceScore: false };
        default:
            const exhaustiveCheck: never = action;
    }

    return state || { isSelectQualityServiceScore: false };
};
