﻿import * as React from 'react';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import User from '../../models/User';
import { ApplicationState } from '../../store';
import * as EvaluationServiceStore from './EvaluationServiceStore';

import './EvaluationService.less';


type Props =
    EvaluationServiceStore.EvaluationServiceState
    & EvaluationServiceStore.EvaluationServiceAction;

/**
 * Оценка качества сервиса 
 */
class EvaluationService extends BaseComponent<Props> {

    public Name: string = 'EvaluationService';

    public render() {
        if (this.props.isSelectQualityServiceScore) {
            return this.renderThanksEnd();
        }

        return <div className={this.getClassName()}>
            <div className={this.getClassName('label-main')}>ОЦЕНИТЕ КАЧЕСТВО РАБОТЫ СЕРВИСА</div>
            <ul className='nav justify-content-center'>
                {this.renderNumber(5)}
                {this.renderNumber(4)}
                {this.renderNumber(3)}
            </ul>
            <ul className='nav justify-content-center'>
                {this.renderNumber(2)}
                {this.renderNumber(1)}
            </ul>
        </div>;
    }

    /**
     * Отрисовка цифры
     * @param {number} number
     * @return {any}
     */
    public renderNumber(number: number) {
        let className = this.getClassName('div-number', User.instance.SelectQualityServiceScore === number ? 'active' : '');

        return <li
            className={'nav-item ' + className}
            onClick={() => this.onClickNumber(number)}
        >
            {number}
        </li>;
    }

    /**
     * Отрисовка благодарности
     * @return {any}
     */
    public renderThanksEnd() {
        return <div className={this.getClassName('block-end')}>Благодарим, за вашу оценку.</div>
    }

    public onClickNumber(selectedNumber: number) {
        //TODO: отправляем запрос на сервер - изменить поле у User
        //fetch('/article/fetch/user.json', {method: 'POST', body: {selectedNumber: selectedNumber}})

        //меняем глобальную переменную User
        User.instance.SelectQualityServiceScore = selectedNumber;

        if (this.props.select) {
            this.props.select();
        }

        //можно анимаию запустить плавного исчезновения текста
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.evaluationService, 
    EvaluationServiceStore.actionCreators                
)(EvaluationService) as typeof EvaluationService;
