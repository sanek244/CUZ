﻿import * as React from 'react';
import { connect } from "react-redux";
import * as moment from 'moment';

import BaseComponent from "../../models/BaseComponent";
import App from "../../App";
import { ApplicationState } from '../../store';
import * as PrintRoutingListStore from './PrintRoutingListStore';
import Alert from "../Alert/Alert";

import './PrintRoutingList.less';

type Props =
    PrintRoutingListStore.PrintRoutingListState
    & PrintRoutingListStore.PrintRoutingListAction
    & {};

/**
 * Распечатка талона
 */
class PrintRoutingList extends BaseComponent<Props> {

    public Name: string = 'PrintRoutingList';


    componentDidUpdate() {
        if (this.props.isClose || !this.props.appointments || !this.props.appointments.length) {
            return null;
        }

        const el = document.getElementById(this.Name);
        var mywindow = window.open('', 'PRINT', 'height=500,width=750');

        if (el && mywindow) {
            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(el.innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

            App.Store.dispatch(PrintRoutingListStore.actionCreators.close());
        }
        else {
            Alert.Open('Не удалось распечатать документ!');
        }
    }

    public render() {
        if (this.props.isClose || !this.props.appointments || !this.props.appointments.length || !App.user) {
            return null;
        }

        return <div className={this.getClassName()} id={this.getClassName()}>
            <div><strong>Маршрутный лист.</strong></div>
            <div><strong>Клиент.</strong>{' ' + App.user.Surname + ' ' + App.user.Name + ' ' + App.user.Patronymic}</div>
            <br />
            <style dangerouslySetInnerHTML={{
            __html: `
                    .` + this.getClassName('table') + ` td{ 
                        border: 1px solid black; 
                        padding: 2px 10px;
                    }
                `}} />

            <table className={this.getClassName('table')} style={{ 'border-collapse': 'collapse', 'border-spacing': 0 }}>
                <thead>
                    <tr>
                        <td>Дата/Время приема</td>
                        <td>ФИО врача</td>
                        <td>Специальность врача</td>
                        <td>Адрес клиники</td>
                        <td>Кабинет</td>
                        <td>Этаж</td>
                    </tr>
                </thead>
                <tbody>
                    {this.props.appointments.map(appointment => {
                        return <tr>
                            <td>{moment(appointment.FROM).format('DD.MM.YYYY HH:mm')}</td>
                            <td>{appointment.DOCTOR.DOCTOR_F + ' ' + appointment.DOCTOR.DOCTOR_I + ' ' + appointment.DOCTOR.DOCTOR_O}</td>
                            <td>{appointment.DOCTOR.DOCTOR_SPECIALITY.NAME}</td>
                            <td>{appointment.CLINIC.ADDRESS_DESCRIPTION}</td>
                            <td>{appointment.CABINET}</td>
                            <td>{appointment.STOREY}</td>
                        </tr>;
                    })}
                </tbody>
            </table>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.printRoutingList,
    PrintRoutingListStore.actionCreators
)(PrintRoutingList) as typeof PrintRoutingList;
