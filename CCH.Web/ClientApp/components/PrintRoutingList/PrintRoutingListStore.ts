﻿import { Action, Reducer } from 'redux';

import Clinic from "../../models/Clinic";
import Doctor from "../../models/Doctor";
import Appointment from "../../models/Appointment";
import { AppThunkAction } from "../../store";

// STATE

export interface PrintRoutingListState {
    appointments?: Appointment[],
    isClose?: boolean
}

// ACTIONS
interface CloseAction { type: 'PRINT_ROUTING_LIST__CLOSE' }
interface OpenAction { type: 'PRINT_ROUTING_LIST__OPEN', appointments: Appointment[]}

type KnownAction = CloseAction | OpenAction;

// ACTION CREATORS
//for TypeScript
export interface PrintRoutingListAction {
    close?: () => CloseAction;
    print?: () => OpenAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'PRINT_ROUTING_LIST__CLOSE' },
    print: (appointments: Appointment[]) => <OpenAction>{ type: 'PRINT_ROUTING_LIST__OPEN', appointments: appointments }
};

// REDUCER 
export const reducer: Reducer<PrintRoutingListState> = (state: PrintRoutingListState, action: KnownAction) => {
    const stateNew: PrintRoutingListState = {
        isClose: state ? state.isClose : true,
        appointments: state ? state.appointments : [],
    }

    switch (action.type) {
        case 'PRINT_ROUTING_LIST__CLOSE':
            stateNew.isClose = true;
            break;

        case 'PRINT_ROUTING_LIST__OPEN':
            stateNew.isClose = false;
            stateNew.appointments = action.appointments;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
