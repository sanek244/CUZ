﻿import { Action, Reducer } from 'redux';

export enum HeaderState {
    danger,
    success,
    warning,
}
export const HeaderStateType: any = {
    [HeaderState.danger]: 'danger',
    [HeaderState.warning]: 'warning',
    [HeaderState.success]: 'success',
}
export const HeaderStateTitle: any = {
    [HeaderState.danger]: 'Ошибка',
    [HeaderState.warning]: 'Предупреждение',
    [HeaderState.success]: 'Сообщение',
}

// STATE
export interface AlertState {
    isClose?: boolean;
    text?: string,
    headerState?: HeaderState | null
}

// ACTIONS
interface CloseAction { type: 'ALERT__CLOSE' }
interface OpenAction { type: 'ALERT__OPEN', text: string, title: string, headerState: HeaderState }

type KnownAction = CloseAction | OpenAction;

// ACTION CREATORS
//for TypeScript
export interface AlertAction {
    close?: () => CloseAction;
    open?: () => OpenAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'ALERT__CLOSE' },
    open: (text: string = '', headerState?: HeaderState) => <OpenAction>{ type: 'ALERT__OPEN', text: text, headerState: headerState }
};

// REDUCER 
export const reducer: Reducer<AlertState> = (state: AlertState, action: KnownAction) => {
    switch (action.type) {
        case 'ALERT__CLOSE':
            return { isClose: true };

        case 'ALERT__OPEN':
            return {
                isClose: false,
                text: action.text || '',
                headerState: action.headerState || null
            };
        default:
            const exhaustiveCheck: never = action;
    }

    return state || {
        isClose: true,
        text: '',
        headerState: null
    };
};
