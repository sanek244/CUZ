﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as AlertStore from './AlertStore';
import App from "../../App";

import './Alert.less';


type Props =
    AlertStore.AlertState
    & AlertStore.AlertAction
    & {};

/**
 * Сообщение в модальном окне 
 */
class Alert extends BaseComponent<Props> {

    public Name: string = 'Alert';

    public static Open(text: string = '', headerState?: AlertStore.HeaderState) {
        App.Store.dispatch(AlertStore.actionCreators.open(text, headerState));
    }

    public static Close() {
        App.Store.dispatch(AlertStore.actionCreators.close());
    }

    public render() {
        if (this.props.isClose) {
            return null;
        }
        const close = this.props.close ? this.props.close : () => { };
        const headerState = typeof this.props.headerState === 'number' ? this.props.headerState : AlertStore.HeaderState.danger;
        const headerClassAdd = ' ' + this.getClassName('header', AlertStore.HeaderStateType[headerState]);

        return <div>
            <div className={"modal " + this.getClassName()} {...{ tabIndex: -1 }} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className={"modal-header " + this.getClassName('header') + headerClassAdd}>
                            <span className="modal-title">{AlertStore.HeaderStateTitle[headerState]}</span>
                            <button type="button" className="close" aria-label="Close" onClick={() => close()}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{this.props.text}</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={() => close()}>OK</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop fade in"></div>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.alert,
    AlertStore.actionCreators
)(Alert) as typeof Alert;
