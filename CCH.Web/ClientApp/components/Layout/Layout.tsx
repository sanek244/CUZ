import * as React from 'react';
import { NavLink } from "react-router-dom";
import { Breadcrumbs } from 'react-breadcrumbs-dynamic'

import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import BaseComponent from "../../models/BaseComponent";
import BreadcrumbsContainer from "../BreadcrumbsContainer/BreadcrumbsContainer";
import * as AuthorizationPageStore from '../../pages/Authorization/AuthorizationPageStore';
import Alert from "../Alert/Alert";
import WaitModal from "../WaitModal/WaitModal";
import PrintTalon from "../PrintTalon/PrintTalon";
import PrintRoutingList from "../PrintRoutingList/PrintRoutingList";
import * as CalendarPageStore from "../../pages/Calendar/CalendarPageStore";
import Confirm from "../Confirm/Confirm";
import ConfirmSelectDoctorClinic from "../ConfirmSelectDoctorClinic/ConfirmSelectDoctorClinic";
import * as ConfirmSelectDoctorClinicStore from "../ConfirmSelectDoctorClinic/ConfirmSelectDoctorClinicStore";
import App from "../../App";
import * as routes from "../../routes";

import './Layout.less';

export interface LayoutProps {
    children?: React.ReactNode;
}

/**
 * Входная точка и структура сайта
 */
export default class Layout extends BaseComponent<LayoutProps> {

    public Name: string = 'Layout';


    componentDidMount() {
        //Подписываемся на изменение url адреса
        App.History.listen((location: any) => {
            const url = location.pathname.substr(1);

            //В календаре запрашиваем новые данные
            if (url.indexOf('calendar') === 0) {
                App.Store.dispatch(CalendarPageStore.actionCreators.onOpen());
            }
        });
    }

    public render() {
        let content = this.props.children;

        //Если нет пользователя и адрес не является открытым, то ничего не отображаем
        if (!App.user && routes.publicUrls.filter(x => location.pathname.indexOf(x) === 0).length === 0) {
            content = null;
            App.Store.dispatch(AuthorizationPageStore.actionCreators.changeUrlReturn(location.pathname));
        }

        return <div className={this.getClassName()}>
            <div className={this.getClassName('content')}>
                <div>
                    <Header />
                </div>
                <div className='content'>
                    <div className="container">
                        <Breadcrumbs
                            separator={''}
                            container={BreadcrumbsContainer}
                            item={NavLink}
                            finalItem={'b'}
                        />
                        { content }
                    </div>
                </div>
            </div>
            <div className={this.getClassName('footer')}>
                <Footer />
            </div>
            <WaitModal />
            <Alert />
            <Confirm />
            <PrintTalon />
            <PrintRoutingList />
            <ConfirmSelectDoctorClinic {...App.Store.getState().confirmSelectDoctorClinic} {...ConfirmSelectDoctorClinicStore.actionCreators} />
        </div>;
    }
}

