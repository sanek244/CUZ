﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";
import * as moment from 'moment';

import Clinic from "../../models/Clinic";
import Doctor from "../../models/Doctor";
import DoctorSpecialty from "../../models/DoctorSpecialty";
import ClinicSpecialization from "../../models/ClinicSpecialization";
import { AppThunkAction } from "../../store";
import WaitModal from "../../components/WaitModal/WaitModal";
import ServiceController from "../../controllers/ServiceController";
import SearchDoctrosForm from "../../models/forms/SearchDoctrosForm";

// STATE
export interface SearchDoctorsFormState {
    clinic: Clinic | null,
    clinics: Clinic[],
    clinicSpecialization: ClinicSpecialization[],
    doctorSpecialities: DoctorSpecialty[],
    resultsSearch: Doctor[],

    dateFrom: Date | null,
    dateTo: Date | null,
    timeFrom: string,
    timeTo: string,

    searchForm: SearchDoctrosForm
}

// ACTIONS
interface SuccessLoadClinicAction { type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC', clinic: Clinic | null }
interface SuccessLoadClinicsAction { type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINICS', clinics: Clinic[] }
interface SuccessGetSearchResultsAction { type: 'SearchDoctorsForm__SEARCH_RESULTS', resultsSearch: Doctor[] }
interface SuccessLoadDoctorSpecialitiesAction { type: 'SearchDoctorsForm__SUCCESS_LOAD_DOCTOR_SPECIALITY', doctorSpecialities: DoctorSpecialty[] }
interface SuccessLoadClinicSpecializationAction { type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC_SPECIALIZATION', clinicSpecialization: ClinicSpecialization[] }
interface ChangeDatesAction { type: 'SearchDoctorsForm__CHANGE_DATES', dateFrom: Date | null, dateTo: Date | null, timeFrom: string, timeTo: string }
interface ChangeSearchForm { type: 'SearchDoctorsForm__CHANGE_SEARCH_FROM', searchForm: SearchDoctrosForm }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = SuccessLoadClinicsAction |
    SuccessLoadClinicAction |
    SuccessGetSearchResultsAction |
    SuccessLoadDoctorSpecialitiesAction |
    SuccessLoadClinicSpecializationAction |
    ChangeDatesAction | 
    ChangeSearchForm |
    BagAction;

// ACTION CREATORS
export const actionCreators = {

    requestSearch: (searchForm: SearchDoctrosForm): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const state = getState().searchDoctorsForm;

        //Начало загрузок
        WaitModal.Open('Получение данных');

        dispatch({
            type: 'SearchDoctorsForm__CHANGE_DATES',
            dateFrom: searchForm.DateFrom,
            dateTo: searchForm.DateTo,
            timeFrom: searchForm.TimeFrom ? moment(searchForm.TimeFrom).format('HH:mm') : '',
            timeTo: searchForm.TimeTo ? moment(searchForm.TimeTo).format('HH:mm') : ''
        });

        const promises = [];

        //Загружаем список клиник, если нету
        if (!state.clinics.length) {
            promises.push(ServiceController
                .getClinics(false)
                .then((data: Clinic[]) => dispatch({ type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINICS', clinics: data.filter(clinic => !clinic.IS_DELETED) })));
        }

        //Загружаем клинику по id
        if (searchForm.ClinicId !== null) {
            if (searchForm.ClinicId > -1) {
                promises.push(ServiceController
                    .getClinic(searchForm.ClinicId + '')
                    .then((data: Clinic) => dispatch({ type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC', clinic: data })));
            }
            else {
                dispatch({ type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC', clinic: null });
            }
        }

        //Загружаем специальности врачей, если нету
        if (!state.doctorSpecialities.length) {
            promises.push(ServiceController
                .getDoctorSpecialities()
                .then((data: DoctorSpecialty[]) => dispatch({ type: 'SearchDoctorsForm__SUCCESS_LOAD_DOCTOR_SPECIALITY', doctorSpecialities: data })));
        }

        //Загружаем специальности клиник, если нету
        if (!state.clinicSpecialization.length) {
            promises.push(ServiceController
                .getClinicSpecializations()
                .then((data: ClinicSpecialization[]) => dispatch({ type: 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC_SPECIALIZATION', clinicSpecialization: data })));
        }

        //Ищем врачей по параметрам
        promises.push(ServiceController
            .getSearchResultDoctors(searchForm)
            .then((data: Doctor[]) => dispatch({ type: 'SearchDoctorsForm__SEARCH_RESULTS', resultsSearch: data || [] })));

        //проверка на ошибки запросов
        const promise = Promise.all(promises)
            .then(() => {
                dispatch({ type: 'SearchDoctorsForm__CHANGE_SEARCH_FROM', searchForm: searchForm });
                WaitModal.Close();
            })
            .catch((err) => WaitModal.Close());

        addTask(promise); // Ensure server-side prerendering waits for this to complete
    },

    //обновление результатов поиска
    updateSearchResults: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        ServiceController
            .getSearchResultDoctors(getState().searchDoctorsForm.searchForm)
            .then((data: Doctor[]) => dispatch({ type: 'SearchDoctorsForm__SEARCH_RESULTS', resultsSearch: data || [] }));
    }
    
};

// REDUCER 
export const reducer: Reducer<SearchDoctorsFormState> = (state: SearchDoctorsFormState, action: KnownAction) => {
    const stateNew: SearchDoctorsFormState = {
        clinic: state ? state.clinic : null,
        clinics: state ? state.clinics : [],
        resultsSearch: state ? state.resultsSearch : [],
        doctorSpecialities: state ? state.doctorSpecialities : [],
        clinicSpecialization: state ? state.clinicSpecialization : [],

        dateFrom: state ? state.dateFrom : null,
        dateTo: state ? state.dateTo : null,
        timeFrom: state ? state.timeFrom : '',
        timeTo: state ? state.timeTo : '',

        searchForm: state ? state.searchForm : new SearchDoctrosForm(),
    }

    switch (action.type) {
        case 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC':
            stateNew.clinic = action.clinic;
            break;

        case 'SearchDoctorsForm__SUCCESS_LOAD_CLINICS':
            stateNew.clinics = action.clinics;
            break;

        case 'SearchDoctorsForm__SEARCH_RESULTS':
            stateNew.resultsSearch = action.resultsSearch;
            break;

        case 'SearchDoctorsForm__SUCCESS_LOAD_DOCTOR_SPECIALITY':
            stateNew.doctorSpecialities = action.doctorSpecialities;
            break;

        case 'SearchDoctorsForm__SUCCESS_LOAD_CLINIC_SPECIALIZATION':
            stateNew.clinicSpecialization = action.clinicSpecialization;
            break;

        case 'SearchDoctorsForm__CHANGE_DATES':
            stateNew.dateFrom = action.dateFrom;
            stateNew.dateTo = action.dateTo;
            stateNew.timeFrom = action.timeFrom;
            stateNew.timeTo = action.timeTo;
            break;

        case 'SearchDoctorsForm__CHANGE_SEARCH_FROM':
            stateNew.searchForm = action.searchForm;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
