﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import Select from 'react-select';
import * as moment from 'moment';

import BaseComponent from "../../models/BaseComponent";
import Doctor from "../../models/Doctor";
import { ApplicationState } from '../../store';
import * as SearchDoctorsFormStore from './SearchDoctorsFormStore';
import * as RecordingAdmissionStore from '../../components/RecordingAdmission/RecordingAdmissionStore';
import RecordingAdmission from '../../components/RecordingAdmission/RecordingAdmission';
import DoctorSpecialty from "../../models/DoctorSpecialty";
import SearchDoctrosForm from "../../models/forms/SearchDoctrosForm";
import SearchDoctrosUrlParams from "../../models/SearchDoctrosUrlParams";
import Helper from "../../Helper";
import App from "../../App";

import './SearchDoctorsForm.less';
import 'react-select/less/default.less';

interface Prop {
    defaultUrl?: string, //куда возвращать без параметров поиска
    isShowDateFilter?: boolean, //показывать формы с датой для фильтрации поиска?
    isIdClinicInUrl?: boolean //учитывать idClinic из url, false - брать значение клиники из параметров url
    urlWithClinicId?: string, //если isIdClinicInUrl = true, указывать url с {0} для вставки clinicId
}
type Props =
    SearchDoctorsFormStore.SearchDoctorsFormState
    & typeof SearchDoctorsFormStore.actionCreators
    & Prop;

/**
 * Поиск врачей
 */
class SearchDoctorsForm extends BaseComponent<Props> {

    public Name: string = 'SearchDoctorsForm';

    //Инпуты (для получения значений)
    isOnlyAvailableRecording = false;
    inputSearchFio: HTMLInputElement | null = null;
    inputClinicAdress: HTMLInputElement | null = null;

    inputDateFrom: HTMLInputElement | null = null;
    inputDateTo: HTMLInputElement | null = null;
    inputTimeFrom: HTMLInputElement | null = null;
    inputTimeTo: HTMLInputElement | null = null;

    selectDoctorSpecializations: string = '';
    selectClinicSpecializations: string = '';
    selectClinicId: string = 'all';

    //предыдущий url поиска (для контроля изменений)
    oldUrl: string = '-';

    public componentWillMount() {
        App.Store.subscribe(() => this.forceUpdate());
    }

    public componentDidMount() {
        this.loadData();
    }

    public componentDidUpdate() {
        this.loadData();
    }

    //загружаем новые данные, если текущий url отличается от предыдущего
    protected loadData() {
        const urlParams = this.getUrlParams();
        this.isOnlyAvailableRecording = urlParams.IsOnlyAvailableRecording === 'true';
        this.selectDoctorSpecializations = urlParams.DoctorSpecialityIds;
        this.selectClinicSpecializations = urlParams.ClinicSpecializationIds;
        this.selectClinicId = urlParams.ClinicId;

        const url = App.History.location.pathname + App.History.location.search;

        //url изменился
        if (this.oldUrl !== url) {
            this.oldUrl = url;
            const form = new SearchDoctrosForm(urlParams);
            this.props.requestSearch(form);
        }
    }

    /**
     * Параметры из url строки 
     * @return {SearchDoctrosUrlParams}
     */
    getUrlParams() {
        const searchParams = new SearchDoctrosUrlParams();

        if (this.props.isIdClinicInUrl) {
            searchParams.ClinicId = App.History.location.pathname.split('/').filter(x => x)[1];
        }

        App.History.location.search
            .replace('?', '')
            .split('&')
            .map(el => {
                const pair = el.split('=');
                switch (pair[0]) {
                    case 'doctorFio': searchParams.DoctorFio = decodeURIComponent(pair[1]); break;
                    case 'isOnlyAvailableRecording': searchParams.IsOnlyAvailableRecording = pair[1]; break;
                    case 'doctorSpecialityIds': searchParams.DoctorSpecialityIds = pair[1]; break;
                    case 'clinicSpecializationIds': searchParams.ClinicSpecializationIds = pair[1]; break;
                    case 'clinicAdress': searchParams.ClinicAdress = decodeURIComponent(pair[1]); break;
                    case 'dateFrom': searchParams.DateFrom = decodeURIComponent(pair[1]); break;
                    case 'dateTo': searchParams.DateTo = decodeURIComponent(pair[1]); break;
                    case 'timeFrom': searchParams.TimeFrom = decodeURIComponent(pair[1]); break;
                    case 'timeTo': searchParams.TimeTo = decodeURIComponent(pair[1]); break;
                    case 'clinicId': searchParams.ClinicId = pair[1]; break;
                }
            });

        return searchParams;
    }
    

    /**
     * Отрисовка формы поиска
     */
    public render() {
        const searchParams = this.getUrlParams();

        let clinics = this.props.clinics;
        let labelDefaultClinics = 'Все';

        //Фильтруем клиники под выбранные параметры
        if (this.selectClinicSpecializations.length) {
            clinics = clinics.filter(x => this.selectClinicSpecializations.indexOf(x.ID_CLINIC_SPECIALIZATION + '') > -1);
            labelDefaultClinics += ', со специализацией';
        }
        if (this.inputClinicAdress && this.inputClinicAdress.value) {
            const address = this.inputClinicAdress.value.toLowerCase();
            clinics = clinics.filter(x => x.ADDRESS_DESCRIPTION.toLowerCase().indexOf(address) > -1);
            labelDefaultClinics += ', с адресом';
        }

        //если указанной клиники нет в отфильтрованных
        if (this.props.isIdClinicInUrl && searchParams.ClinicId !== 'all' && !clinics.find(x => x.ID + '' === searchParams.ClinicId)) {
            App.History.push((this.props.defaultUrl || App.History.location.pathname) + App.History.location.search);
            return null;
        }

        //add default value + transform in format select
        const clinicsView = [{ label: labelDefaultClinics, value: 'all' }]
            .concat(clinics.map(el => ({ label: el.NAME, value: el.ID + '' })));

        return <div className={this.getClassName('form-search')}>

            <div className="form-group">
                <label>Специальность врача</label>
                <Select
                    multi={true}
                    className={this.getClassName('form-search-select2')}
                    value={searchParams.DoctorSpecialityIds.split(',')}
                    options={this.props.doctorSpecialities.map(el => { return { label: el.NAME, value: el.ID } })}
                    onChange={(data: { value: string; label: string }[]) => {
                        this.selectDoctorSpecializations = data.map(el => el.value).join(',');
                        this.search()
                    }}
                    placeholder='Все'
                />
            </div>

            <div className="form-group">
                <label>Специализация клиники</label>
                <Select
                    multi={true}
                    className={this.getClassName('form-search-select2')}
                    value={searchParams.ClinicSpecializationIds.split(',')}
                    options={this.props.clinicSpecialization.map(el => { return { label: el.NAME, value: el.ID } })}
                    onChange={(data: { value: string; label: string }[]) => {
                        this.selectClinicSpecializations = data.map(el => el.value).join(',');
                        this.search()
                    }}
                    placeholder='Все'
                />
            </div>

            <div className="form-group">
                <label>ФИО врача</label>
                <div className={this.getClassName('form-search-input-container')}>
                    <input
                        className={this.getClassName('form-search-input')}
                        defaultValue={searchParams.DoctorFio}
                        ref={(input) => { this.inputSearchFio = input; }}
                        onKeyDown={e => { if (e.keyCode === 13) this.search(); }}
                    />
                </div>
            </div>

            <div className="form-group">
                <label>Клиника</label>
                <Select
                    className={this.getClassName('form-search-select2')}
                    value={searchParams.ClinicId}
                    options={clinicsView}
                    onChange={(data: { value: string; label: string }) => {
                        this.selectClinicId = data.value;
                        this.search()
                    }}
                    placeholder={labelDefaultClinics}
                    clearable={false}
                />
            </div>

            {this.renderDateFilterForm()}

            <div className="form-group">
                <label>Адрес клиники</label>
                <div className={this.getClassName('form-search-input-container')}>
                    <input
                        defaultValue={searchParams.ClinicAdress}
                        className={this.getClassName('form-search-input')}
                        ref={(input) => { this.inputClinicAdress = input; }}
                        onKeyDown={e => { if (e.keyCode === 13) this.search(); }}
                    />
                </div>
            </div>

            <div className="form-group">
                <span
                    className={this.getClassName('checkbox-default-container')}
                    onClick={() => {
                        this.isOnlyAvailableRecording = !this.isOnlyAvailableRecording;
                        this.search();
                    }}
                >
                    <div className={this.getClassName('checkbox-default', this.isOnlyAvailableRecording ? 'active' : '')}>
                        <i className="fa fa-check"></i>
                    </div>
                    <span className={this.getClassName('checkbox-default-text')}>Только доступные для записи</span>
                </span>
            </div>

            <div className="form-group d-flex">
                <button
                    className={"btn text-white mt-auto " + this.getClassName('form-search-button')}
                    onClick={() => this.search()}
                >
                    <span className={this.getClassName('form-search-button-text')}>НАЙТИ</span>
                </button>
            </div>
            <div className="form-group d-flex">
                <button
                    className={"btn text-white mt-auto " + this.getClassName('form-search-button')}
                    onClick={() => this.clearFilters()}
                    style={{ width: 190 }}
                >
                    <span className={this.getClassName('form-search-button-text')}>ОЧИСТИТЬ ФИЛЬТРЫ</span>
                </button>
            </div>
        </div>
    }

    renderDateFilterForm() {
        if (!this.props.isShowDateFilter) {
            return null;
        }

        const minDate = moment().format('YYYY-MM-DD');
        const maxDate = moment().add(30, 'd').format('YYYY-MM-DD');

        return <div className="form-group">
            <label>Дата и время для записи</label>
            <br />
            <input type="date" min={minDate} max={maxDate} ref={(input) => { this.inputDateFrom = input; }} onChange={() => this.search(true)} />
            -
            <input type="date" min={minDate} max={maxDate} ref={(input) => { this.inputDateTo = input; }} onChange={() => this.search(true)} />
            <br />
            <input type="time" ref={(input) => { this.inputTimeFrom = input; }} onChange={() => this.search(true)} />
            -
            <input type="time" ref={(input) => { this.inputTimeTo = input; }} onChange={() => this.search(true)} />
        </div>;
    }

    /**
     * Поиск (редирект на данную страницу с другими параметрами)
     */
    public search(hideSearchDateTimeError = false) {
        
        if (!this.checkSearchDate()) {
            if (this.props.isShowDateFilter && !hideSearchDateTimeError) {
                alert("Некорректно настроены дата и время поиска");
            }

            return;
        }

        const searchParams = [
            this.inputSearchFio ? (this.inputSearchFio.value ? 'doctorFio=' + this.inputSearchFio.value : '') : '',
            this.inputClinicAdress ? (this.inputClinicAdress.value ? 'clinicAdress=' + this.inputClinicAdress.value : '') : '',
            this.selectDoctorSpecializations ? 'doctorSpecialityIds=' + this.selectDoctorSpecializations : '',
            this.selectClinicSpecializations ? 'clinicSpecializationIds=' + this.selectClinicSpecializations : '',
            this.isOnlyAvailableRecording ? 'isOnlyAvailableRecording=' + this.isOnlyAvailableRecording : '',
            this.inputDateFrom ? (this.inputDateFrom.value ? 'dateFrom=' + this.inputDateFrom.value : '') : '',
            this.inputDateTo ? (this.inputDateTo.value ? 'dateTo=' + this.inputDateTo.value : '') : '',
            this.inputTimeFrom ? (this.inputTimeFrom.value ? 'timeFrom=' + this.inputTimeFrom.value : '') : '',
            this.inputTimeTo ? (this.inputTimeTo.value ? 'timeTo=' + this.inputTimeTo.value : '') : ''
        ].filter(x => x).join('&');

        if (this.props.isIdClinicInUrl) {
            App.History.push(Helper.format(this.props.urlWithClinicId + '{1}', [this.selectClinicId, searchParams ? '?' + searchParams : '']));
        }
        else {
            App.History.push(Helper.format(App.History.location.pathname + '{0}', [searchParams ? '?' + searchParams : '']));
        }
    }

    checkSearchDate(): boolean {
        if ((!this.inputDateFrom &&
                !this.inputDateTo &&
                !this.inputTimeFrom &&
                !this.inputTimeTo) ||
            (this.inputDateFrom && !this.inputDateFrom.value &&
                this.inputDateTo && !this.inputDateTo.value &&
                this.inputTimeFrom && !this.inputTimeFrom.value &&
                this.inputTimeTo && !this.inputTimeTo.value) ||
            (this.inputDateFrom && this.inputDateFrom.value &&
                this.inputDateTo && this.inputDateTo.value &&
                this.inputTimeFrom && this.inputTimeFrom.value &&
                this.inputTimeTo && this.inputTimeTo.value &&
                this.inputDateFrom.value <= this.inputDateTo.value &&
                this.inputTimeFrom.value < this.inputTimeTo.value))
            return true;

        return false;
    }

    public clearFilters() {
        if (this.inputClinicAdress) {
            this.inputClinicAdress.value = '';
        }

        if (this.inputSearchFio) {
            this.inputSearchFio.value = '';
        }

        if (this.inputDateFrom) {
            this.inputDateFrom.value = "";
        }

        if (this.inputDateTo) {
            this.inputDateTo.value = "";
        }

        if (this.inputTimeFrom) {
            this.inputTimeFrom.value = "";
        }

        if (this.inputTimeTo) {
            this.inputTimeTo.value = "";
        }

        App.History.push(this.props.defaultUrl || App.History.location.pathname);
    }
}

export default connect(
    (state: ApplicationState) => state.searchDoctorsForm,
    SearchDoctorsFormStore.actionCreators
)(SearchDoctorsForm) as typeof SearchDoctorsForm;