import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from "react-router-dom";

import BaseComponent from "../../models/BaseComponent";

import './BreadcrumbsContainer.less';

/**
 * ������������� "������� ������"
 */
export default class BreadcrumbsContainer extends BaseComponent<{}> {

    public Name: string = 'BreadcrumbsContainer';

    public render() {
        return <div className={this.getClassName()}>
            <ul className="nav">
                {this.props.children}
            </ul>
        </div>;
    }
}
