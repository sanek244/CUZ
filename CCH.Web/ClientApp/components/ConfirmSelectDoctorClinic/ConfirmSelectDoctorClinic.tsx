﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as ConfirmSelectDoctorClinicStore from './ConfirmSelectDoctorClinicStore';

import './ConfirmSelectDoctorClinic.less';

type Props =
    ConfirmSelectDoctorClinicStore.ConfirmSelectDoctorClinicState
    & typeof ConfirmSelectDoctorClinicStore.actionCreators
    & {};

/**
 * Модальное окно с выбором клиники к которой привязан врач
 */
class ConfirmSelectDoctorClinic extends BaseComponent<Props> {

    public Name: string = 'ConfirmSelectDoctorClinic';

    public render() {
        if (this.props.isClose) {
            return null;
        }

        const clinicId = this.props.selectedCLinic ? this.props.selectedCLinic.ID : '';

        return <div>
            <div className={"modal " + this.getClassName()} {...{ tabIndex: -1 }} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className={"modal-header " + this.getClassName('header')}>
                            <span className="modal-title">{this.props.title || 'Выберите больницу для записи'}</span>
                            <button type="button" className="close" aria-label="Close" onClick={this.props.close}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            {this.props.text}
                            <div className={this.getClassName('select')}>
                                <select
                                    className={this.getClassName('select-input')}
                                    onChange={(value) => this.props.changeSelectedClinic(this.props.clinics.find(x => x.ID + '' === value.target.value))}
                                >
                                    {this.props.clinics && this.props.clinics.map(clinic => <option value={clinic.ID} {...{ selected: clinic.ID + '' === clinicId }}>{clinic.NAME}</option>)}
                                </select>
                            </div>
                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={() => this.props.close()}>Отмена</button>
                            <button type="button" className={"btn " + this.getClassName('button')} onClick={() => this.next()}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop fade in"></div>
        </div>;
    }

    protected next() {
        if (this.props.callbackNext) {
            this.props.callbackNext();
        }

        this.props.close();
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.confirmSelectDoctorClinic,
    ConfirmSelectDoctorClinicStore.actionCreators
)(ConfirmSelectDoctorClinic) as typeof ConfirmSelectDoctorClinic;
