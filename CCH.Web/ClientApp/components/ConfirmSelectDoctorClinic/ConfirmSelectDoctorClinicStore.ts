import { Action, Reducer } from 'redux';
import ClinicMin from "../../models/ClinicMin";
import ServiceController from "../../controllers/ServiceController";
import { AppThunkAction } from "../../store";

// STATE
export interface ConfirmSelectDoctorClinicState {
    isClose?: boolean;
    text?: string,
    title?: string,

    clinics: ClinicMin[],
    selectedCLinic?: ClinicMin | null,
    callbackNext?: Function | null
}

// ACTIONS
interface CloseAction { type: 'ConfirmSelectDoctorClinic__CLOSE' }
interface OpenAction { type: 'ConfirmSelectDoctorClinic__OPEN', clinics: ClinicMin[], text: string, title: string, callbackNext: Function }
interface ChangeSelectedClinicAction { type: 'ConfirmSelectDoctorClinic__CHANGE_SEECTED_CLINIC', selectedCLinic: ClinicMin }

type KnownAction = CloseAction | OpenAction | ChangeSelectedClinicAction;

// ACTION CREATORS
export const actionCreators = {
    close: () => <CloseAction>{ type: 'ConfirmSelectDoctorClinic__CLOSE' },
    changeSelectedClinic: (selectedCLinic?: ClinicMin | null) => <ChangeSelectedClinicAction>{ type: 'ConfirmSelectDoctorClinic__CHANGE_SEECTED_CLINIC', selectedCLinic: selectedCLinic },
    open: (clinics: ClinicMin[], callbackNext: Function, text: string = '', title: string = '') =>
        <OpenAction>{ type: 'ConfirmSelectDoctorClinic__OPEN', clinics: clinics, text: text, title: title, callbackNext: callbackNext },
};

// REDUCER 
export const reducer: Reducer<ConfirmSelectDoctorClinicState> = (state: ConfirmSelectDoctorClinicState, action: KnownAction) => {
    const stateNew: ConfirmSelectDoctorClinicState = {
        isClose: state ? state.isClose : true,
        text: state ? state.text : '',
        title: state ? state.title : '',
        selectedCLinic: state ? state.selectedCLinic : null,
        clinics: state ? state.clinics : [],
        callbackNext: state ? state.callbackNext : null
    }

    switch (action.type) {
        case 'ConfirmSelectDoctorClinic__CLOSE':
            stateNew.isClose = true;
            stateNew.title = '';
            break;

        case 'ConfirmSelectDoctorClinic__OPEN':
            stateNew.isClose = false;
            stateNew.text = action.text;
            stateNew.title = action.title;
            stateNew.callbackNext = action.callbackNext;
            stateNew.clinics = action.clinics;
            if (action.clinics.length > 0) {
                stateNew.selectedCLinic = action.clinics[0];
            }
            break;

        case 'ConfirmSelectDoctorClinic__CHANGE_SEECTED_CLINIC':
            stateNew.selectedCLinic = action.selectedCLinic;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
