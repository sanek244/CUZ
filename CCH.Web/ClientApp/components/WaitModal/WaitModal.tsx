import * as React from 'react';
import { connect } from 'react-redux';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as WaitModalStore from './WaitModalStore';
import App from "../../App";

import './WaitModal.less';

interface Prop {
    text?: string
}

type Props =
    WaitModalStore.WaitModalState
    & WaitModalStore.WaitModalAction
    & Prop;

/**
 * ������� �������� � ��������� ���� 
 */
class WaitModal extends BaseComponent<Props> {

    public Name: string = 'WaitModal';

    public static Open(text: string = '') {
        App.Store.dispatch(WaitModalStore.actionCreators.open(text));
    }

    public static Close() {
        App.Store.dispatch(WaitModalStore.actionCreators.close());
    }

    component: HTMLElement | null;

    public componentDidUpdate() {
        //������� ��������� ����
        setTimeout(() => { //��� setTimeout �������� �� ��������� - ����� ��������� �� �������� ������������
            if (this.component) {
                this.component.style.opacity = '1';
            }
        }, 100);
    }

    public render() {
        if (this.props.isClose) {
            return null;
        }

        return <div
            className={"modal " + this.getClassName()} {...{ tabIndex: -1 }}
            role="dialog"
            ref={el => this.component = el}
        >
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                        <div className={this.getClassName('img-container')}>
                            <div className='fa-3x'>
                                <i className="fa fa-spinner fa-pulse"></i>
                            </div>
                            <div>{this.props.text || '�������� ������'}...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState, ownProps: Prop) => {
        return { ...state.WaitModal, ...ownProps };
    },
    WaitModalStore.actionCreators
)(WaitModal) as typeof WaitModal;
