import { Action, Reducer } from 'redux';

// STATE
export interface WaitModalState {
    isClose?: boolean;
    text?: string,
}

// ACTIONS
interface CloseAction { type: 'WaitModal__CLOSE' }
interface OpenAction { type: 'WaitModal__OPEN', text: string }

type KnownAction = CloseAction | OpenAction;

// ACTION CREATORS
//for TypeScript
export interface WaitModalAction {
    close?: () => CloseAction;
    open?: () => OpenAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'WaitModal__CLOSE' },
    open: (text: string = '') => <OpenAction>{ type: 'WaitModal__OPEN', text: text}
};

// REDUCER 
export const reducer: Reducer<WaitModalState> = (state: WaitModalState, action: KnownAction) => {
    switch (action.type) {
        case 'WaitModal__CLOSE':
            return { isClose: true };
        case 'WaitModal__OPEN':
            return {
                isClose: false,
                text: action.text,
            };
        default:
            const exhaustiveCheck: never = action;
    }

    return state || {
        isClose: true,
        text: '',
    };
};
