﻿import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Calendar from 'react-calendar';
import * as moment from 'moment';
import MaskedInput from 'react-text-mask';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as RecordingAdmissionStore from './RecordingAdmissionStore';
import Helper from "../../Helper";
import ScheduleItem from "../../models/ScheduleItem";
import * as AlertStore from "../Alert/AlertStore";
import Alert from "../Alert/Alert";
import Wait from "../Wait/Wait";
import App from "../../App";

import './RecordingAdmission.less';

type Props =
    RecordingAdmissionStore.RecordingAdmissionState
    & typeof RecordingAdmissionStore.actionCreators
    & {};

/**
 * Модальное окно записи на приём
 */
class RecordingAdmission extends BaseComponent<Props> {

    public Name: string = 'RecordingAdmission';


    public render() {
        if (this.props.isClose) {
            return null;
        }

        //Если пользователь это пациент, и специальность доктора не имеет возможности записи на первичный приём
        if (!this.props.doctorSpecialty.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION) {
            Alert.Open('Запись на прием к врачу узкой специальности возможна только после назначения лечащим врачом', AlertStore.HeaderState.warning);
            return null;
        }

        return <div className={"modal show " + this.getClassName()} {...{ tabindex: "-1", role: "dialog" }}>
            <div className="modal-dialog" role="document">
                <div className="content">
                    <button type="button" className={'close ' + this.getClassName('button-close')} aria-label="Close" onClick={this.props.close}></button>
                    <div className={'caption text-white ' + this.getClassName('header')}>
                        <p><strong>{this.props.doctor.DOCTOR_F + ' ' + this.props.doctor.DOCTOR_I + ' ' + this.props.doctor.DOCTOR_O}</strong></p>
                        <p>{this.props.doctorSpecialty.NAME}</p>
                        <p>{this.props.clinic.FULL_NAME}</p>
                    </div>
                    <div className={"bg-white " + this.getClassName('body')}>

                        {this.renderCalendar()}

                        {this.renderForm()}
                        
                        <p><Link to="#" className={this.getClassName('body-link')}>Политика в отношении обработки персональных данных</Link></p>
                        <p><Link to="#" className={this.getClassName('body-link')}>Согласие на обработку персональных данных</Link></p>
                    </div>
                </div>
            </div>
        </div>;
    }

    private renderCalendar() {
        return <div className={"row " + this.getClassName('body-calendar-container')}>
            <div className={this.getClassName('body-calendar-header-text')}>Выберите дату и удобное время приёма специалиста</div>

            <Wait isVisible={this.props.isLoadingSchedule || this.props.isLoadingBusyDays}>
                <Calendar
                    className={this.getClassName('body-calendar')}
                    onChange={this.props.changeDate}
                    minDate={new Date()}
                    formatMonthYear={value => Helper.upFirstChar(moment(value).format('MMMM YYYYг'))}
                    value={this.props.date}

                    //добавляем класс для дней, на которых доступна запись
                    tileClassName={({ date, view }) => { 
                        if (view === 'month' && date > new Date() && this.props.doctorSchedule.DayWeek) {
                            //проверка занятых дней
                            if (this.props.busyDays.indexOf(moment(date).format('YYYY-MM-DDT00:00:00')) > -1) {
                                return this.getClassName('body-calendar-day-busy')
                            }

                            const classNameActive = this.getClassName('body-calendar-day-active');

                            //указан график на особый день и в нём период не пустой
                            let specialDays = this.getSpecialDays(date);
                            if (specialDays.length > 0) {
                                return specialDays.filter(x => x.FROM !== x.TO).length > 0
                                    ? classNameActive
                                    : null;
                            }

                            //рабочая неделя
                            if (this.getWeekDays(date).length > 0) {
                                return classNameActive;
                            }
                        }

                        return null;
                    }}
                />
            </Wait>

            <div className={this.getClassName('body-time-select')}>
                {this.renderTimesAppointment()}
            </div>

        </div>;
    }

    private renderTimesAppointment() {

        //пустой шаблон
        const emptyContent = <div className={this.getClassName('body-time-select-empty')}>
            На <strong>{moment(this.props.date).format('DD MMMM')} </strong> приёма нет.
            </div>;

        //либо не загружен график работы врача, либо данных нету
        if (!this.props.doctorSchedule.DayWeek || this.props.doctorSchedule.DayWeek.length === 0 && this.props.doctorSchedule.SpecialDays.length === 0) {
            return emptyContent;
        }

        //день с расписанием (может быть разбито на части - утром и вечером)
        let schedulesDay: object[] = [];

        //сперва смотрим в привилегированный график по особым дням
        schedulesDay = this.getSpecialDays(this.props.date);

        //особые дни не найдены -> ищем по будням
        if (schedulesDay.length === 0) {
            schedulesDay = this.getWeekDays(this.props.date);
        }

        //по будням тоже не найдено
        if (schedulesDay.length === 0) {
            return emptyContent;
        }

        interface SpanTime {
            date: Date,
            idSchedule: number,
            timeString: string,
            free: boolean,
            from: number,
        };

        //записи на приём
        const selectDay = moment(this.props.date).startOf('day').format('DD.MM.YYYY'); //только день, месяц и год
        let appointments: SpanTime[] = [];
        let minFrom : number = -1;
        let maxTo: number = -1;
        schedulesDay
            .map(x => new ScheduleItem(x))
            .sort(scheduleDay => scheduleDay.fromInt)
            .map(scheduleDay => {
                minFrom = minFrom !== -1 ? (scheduleDay.fromInt < minFrom ? scheduleDay.fromInt : minFrom) : scheduleDay.fromInt;
                maxTo = maxTo !== -1 ? (scheduleDay.toInt > maxTo ? scheduleDay.toInt : maxTo) : scheduleDay.toInt;

                for (let i = scheduleDay.fromInt; i < scheduleDay.toInt; i += scheduleDay.DURATION) {
                    const hours = parseInt(i / 60 + '');
                    const minutes = i % 60;

                    let date = moment(this.props.date).hours(hours).minutes(minutes).seconds(0).milliseconds(0).toDate();

                    appointments.push({
                        date: date,
                        idSchedule: scheduleDay.ID,
                        timeString: Helper.addZeroData(hours) + ':' + Helper.addZeroData(minutes),
                        free: selectDay === moment().startOf('day').format('DD.MM.YYYY') //если сегодняшний день, то активна пока до приёма больше 1 часа
                            ? moment().hours(hours).minutes(minutes) > moment().add(2, 'h')
                            : true,
                        from: i,
                    });
                }
            });

        if (this.props.dateTimeFrom && this.props.dateTimeTo) {
            const timeFrom = this.props.dateTimeFrom.getHours() * 60 + this.props.dateTimeFrom.getMinutes();
            const timeTo = this.props.dateTimeTo.getHours() * 60 + this.props.dateTimeTo.getMinutes();
            let dateFrom = new Date(this.props.dateTimeFrom.toString());
            dateFrom.setHours(0);
            dateFrom.setMinutes(0);
            let dateTo = new Date(this.props.dateTimeTo.toString());
            dateTo.setHours(23);
            dateTo.setMinutes(59);
            console.info(timeFrom);
            console.info(timeTo);
            console.info(appointments);
            appointments = appointments.filter(x => x.from >= timeFrom && x.from <= timeTo
                && this.props.date >= dateFrom && this.props.date <= dateTo);
        }

        //записй нету
        if (appointments.length === 0) {
            return emptyContent;
        }

        //смотрим на уже занятые записи
        this.props.appointments.map(appointment => {
            for (let i = 0; i < appointments.length; i++) {
                if (moment(appointments[i].date).format('HH:mm') === moment(appointment.FROM).format('HH:mm')) {
                    appointments[i].free = false;
                    break;
                }
            }
        });

        return <div>
            <div className={this.getClassName('body-time-select-title')}>
                <span>{moment(this.props.date).format('DD MMMM')} </span>
                прием
                <span> с {Helper.addZeroData(parseInt(minFrom / 60 + ''))}-{Helper.addZeroData(minFrom % 60)} до {Helper.addZeroData(parseInt(maxTo / 60 + ''))}-{Helper.addZeroData(maxTo % 60)}</span>
            </div>

            <div className={this.getClassName('body-time-select-list')}>
                <Wait isVisible={this.props.isLoadingAppointments || this.props.isLoadingSchedule}>
                    {appointments.map(appointment => {
                        const isActive = moment(this.props.date).format('DD.MM.YYYY HH:mm') === moment(appointment.date).format('DD.MM.YYYY HH:mm');

                        return <span
                            className={this.getClassName('body-time-select-list-item') + ' ' +
                                this.getClassName(
                                    'body-time-select-list-' + (appointment.free ? 'free' : 'busy'),
                                    isActive ? 'active' : ''
                                )}
                            onClick={() => {
                                if (appointment.free && !isActive) {
                                    this.props.changeDate(appointment.date, appointment.idSchedule)
                                }
                            }}
                        >
                            {appointment.timeString}
                        </span>
                    })}
                </Wait>
            </div>
        </div>;
    }

    private renderForm() {
        return <div className={"form " + (this.props.errorEnty ? 'has-error' : '')}>

            <p>Оставьте ваш номер телефона, и наши операторы свяжутся с вами в ближайшее время для подтверждения записи на прием.</p>

            <div className='text-danger'>{this.props.errorEnty}</div>

            <div className={"form-group " + this.getClassName('body-form-phone')}>

                <MaskedInput
                    mask={['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}
                    placeholder="+7 (000) 000-00-00"
                    className={this.getClassName('body-form-phone-input')}
                    onChange={(val: any) => this.props.changePhone(val.target.value)}
                    defaultValue={App.user ? App.user.PhoneNumber : ''}
                />

                <button
                    className={"text-white bg_green mt-auto " + this.getClassName('body-form-phone-button')}
                    onClick={() => this.props.requestSendCreateAppointment()}>
                    ОТПРАВИТЬ ЗАЯВКУ
                </button>

            </div>
        </div>;
    }

    /**
     * Возвращает особыми рабочий график на указанную дату
     * @param date
     */
    private getSpecialDays(date: Date) {
        const selectDay = moment(date).startOf('day').format('DD.MM.YYYY'); //только день, месяц и год
        return this.props.doctorSchedule.SpecialDays.filter(x => moment(x.DATE).startOf('day').format('DD.MM.YYYY') === selectDay);
    }

    /**
     * Возвращает недельный рабочий график на указанную дату
     * @param date
     */
    private getWeekDays(date: Date) {
        return this.props.doctorSchedule.DayWeek.filter(x => x.DAY === date.getDay());
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.recordingAdmission,
    RecordingAdmissionStore.actionCreators
)(RecordingAdmission) as typeof RecordingAdmission;
