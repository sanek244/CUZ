﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";
import * as moment from 'moment';

import Doctor from "../../models/Doctor";
import Clinic from "../../models/Clinic";
import ClinicMin from "../../models/ClinicMin";
import DoctorSpecialty from "../../models/DoctorSpecialty";
import Alert from "../Alert/Alert";
import * as ConfirmStore from "../Confirm/ConfirmStore";
import Confirm from "../Confirm/Confirm";
import * as PrintTalonStore from "../PrintTalon/PrintTalonStore";
import { AppThunkAction } from "../../store";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";
import Helper from "../../Helper";
import Schedule from "../../models/Schedule";
import ScheduleItem from "../../models/ScheduleItem";
import AppointmentForm from "../../models/forms/AppointmentForm";
import * as ConfirmSelectDoctorClinicStore from "../../components/ConfirmSelectDoctorClinic/ConfirmSelectDoctorClinicStore";
import Appointment from "../../models/Appointment";
import { StatusAppointmentEnum } from "../../models/Appointment";

// STATE
export interface RecordingAdmissionState {
    isClose: boolean;
    doctor: Doctor;
    doctorSchedule: Schedule;
    clinic: Clinic | ClinicMin;
    doctorSpecialty: DoctorSpecialty,
    //Дата обозначает интервал дней, когда отрисовывать график, а время - интервал по времени, в какие часы отображать
    dateTimeFrom: Date | null,
    dateTimeTo: Date | null,

    appointments: Appointment[],
    busyDays: string[],
    idScheduleItemSelect: number,
    date: Date,
    errorEnty: string,
    phone: string,
    isLoadingAppointments: boolean,
    isLoadingSchedule: boolean,
    isLoadingBusyDays: boolean,
}

// ACTIONS
interface CloseAction { type: 'RecordingAdmission__CLOSE' }
interface OpenAction {
    type: 'RecordingAdmission__OPEN', doctor: Doctor, clinic: Clinic | ClinicMin, doctorSpecialty: DoctorSpecialty,
    dateTimeFrom: Date | null, dateTimeTo: Date | null;
}
interface ChangeDateAction { type: 'RecordingAdmission__CHANGE_DATE', date: Date }
interface ChangeTimeAction { type: 'RecordingAdmission__CHANGE_TIME', idScheduleItemSelect: number }
interface ChangePhoneAction { type: 'RecordingAdmission__CHANGE_PHONE', phone: string }
interface ChangeAppointmentsAction { type: 'RecordingAdmission__CHANGE_APPOINTMENTS', appointments: Appointment[] }
interface ChangeBusyDaysAction { type: 'RecordingAdmission__CHANGE_BUSY_DAYS', busyDays: string[] }

interface EntryErrorAction { type: 'RecordingAdmission__ENTRY_ERROR', errorEnty: string }
interface ChangeScheduleAction { type: 'RecordingAdmission__CHANGE_SCHEDULE', doctorSchedule: Schedule }

interface ChangeIsLoadingAppointmentsAction { type: 'RecordingAdmission__CHANGE_IS_LOADING_APPONTMENTS', isLoading: boolean }
interface ChangeIsLoadingScheduleAction { type: 'RecordingAdmission__CHANGE_IS_LOADING_SCHEDULE', isLoading: boolean }
interface ChangeIsLoadingBusyDaysAction { type: 'RecordingAdmission__CHANGE_IS_LOADING_BUSY_DAYS', isLoading: boolean }

type KnownAction = CloseAction | OpenAction | ChangeDateAction | EntryErrorAction | ChangePhoneAction | ChangeScheduleAction |
    ChangeTimeAction | ChangeIsLoadingAppointmentsAction | ChangeIsLoadingScheduleAction | ChangeAppointmentsAction |
    ChangeIsLoadingBusyDaysAction | ChangeBusyDaysAction;

// ACTION CREATORS
export const actionCreators = {

    //закрыть окно
    close: () => <CloseAction>{ type: 'RecordingAdmission__CLOSE' },

    //открыть окно
    open: (doctor: Doctor, clinic: Clinic | null, doctorSpecialty: DoctorSpecialty, dateTimeFrom: Date | null, dateTimeTo: Date | null)
        : AppThunkAction<KnownAction> => (dispatch, getState) => {

            if (!doctor.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION) {
                Alert.Open('Отсутствует запись на первичный приём');
                return;
            }

            //если нету клиники
            if (!clinic) {
                ServiceController
                    .getDoctorClinics(doctor.ID, true)
                    .then((clinics: ClinicMin[]) => {
                        console.log(clinics);

                        //если клиник несколько
                        if (clinics.length > 1) {
                            //показываем окно с выбором клиники
                            return App.Store.dispatch(ConfirmSelectDoctorClinicStore.actionCreators.open(clinics, () => {
                                const selectedClinic = App.Store.getState().confirmSelectDoctorClinic.selectedCLinic;
                                if (selectedClinic) {
                                    Open(dispatch, getState, doctor, selectedClinic, doctorSpecialty, dateTimeFrom, dateTimeTo);
                                }
                            }));
                        }

                        //1 клиника
                        if (clinics.length === 1) {
                            return Open(dispatch, getState, doctor, clinics[0], doctorSpecialty, dateTimeFrom, dateTimeTo);
                        }

                        Alert.Open('Не найдены клиники, к которым прикреплён выбранный врач!');
                    });

                return;
            }

            Open(dispatch, getState, doctor, clinic, doctorSpecialty, dateTimeFrom, dateTimeTo);
    },

    //изменить телефон
    changePhone: (phone: string) => <ChangePhoneAction>{ type: 'RecordingAdmission__CHANGE_PHONE', phone: phone },

    //изменить дату
    changeDate: (date: Date, idScheduleItemSelect?: number): AppThunkAction<KnownAction> => (dispatch, getState) => {

        //сохраняем новое состояние
        dispatch({ type: 'RecordingAdmission__CHANGE_DATE', date: date });

        if (idScheduleItemSelect) {
            dispatch({ type: 'RecordingAdmission__CHANGE_TIME', idScheduleItemSelect: idScheduleItemSelect });
            return;
        }

        App.Store.dispatch(actionCreators.requestGetAppointment(date));
    },

    //запрос на получение записей на указанную дату
    requestGetAppointment: (date: Date, doctorId?: number, clinicId?: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_APPONTMENTS', isLoading: true });
        dispatch({ type: 'RecordingAdmission__CHANGE_APPOINTMENTS', appointments: [] });

        //Отправляем запрос на получение записей на приём по текущей дате
        const promise = ServiceController
            .getRecordingTimeAdmissionByDate(
                Helper.timeToISOString(date),
                doctorId ? doctorId : getState().recordingAdmission.doctor.ID,
                clinicId ? clinicId : getState().recordingAdmission.clinic.ID,
                true
            )
            .then((data: Appointment[]) => {
                dispatch({ type: 'RecordingAdmission__CHANGE_APPOINTMENTS', appointments: data });
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_APPONTMENTS', isLoading: false });
            })
            .catch(() => {
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_APPONTMENTS', isLoading: false })
            });

        addTask(promise); // Ensure server-side prerendering WaitModals for this to complete
    },

    //запрос на получение занятых дней
    requestGetBusyDays: (doctorId?: number, clinicId?: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_BUSY_DAYS', isLoading: true });

        ServiceController.getBusyDay(
                doctorId ? doctorId : getState().recordingAdmission.doctor.ID,
                clinicId ? clinicId : getState().recordingAdmission.clinic.ID
            )
            .then((data: string[]) => {
                dispatch({ type: 'RecordingAdmission__CHANGE_BUSY_DAYS', busyDays: data });
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_BUSY_DAYS', isLoading: false })
            })
            .catch(() => {
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_BUSY_DAYS', isLoading: false })
            });
    },

    //запрос на получение расписание врача
    requestGetSchedule: (doctorId?: number, clinicId?: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_SCHEDULE', isLoading: true });
        dispatch({ type: 'RecordingAdmission__CHANGE_SCHEDULE', doctorSchedule: new Schedule() });

        //Отправляем запрос
        const promise = ServiceController
            .getSchedule(
                doctorId ? doctorId : getState().recordingAdmission.doctor.ID,
                clinicId ? clinicId : getState().recordingAdmission.clinic.ID
            )
            .then((data: Schedule) => {
                dispatch({ type: 'RecordingAdmission__CHANGE_SCHEDULE', doctorSchedule: data });
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_SCHEDULE', isLoading: false })
            })
            .catch(() => {
                dispatch({ type: 'RecordingAdmission__CHANGE_IS_LOADING_SCHEDULE', isLoading: false })
            });

        addTask(promise); // Ensure server-side prerendering WaitModals for this to complete
    },

    //запрос на запись
    requestSendCreateAppointment: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const state = getState().recordingAdmission;

        //Выбранная дата
        const date = state.date;
        const momentDateFrom = moment(date);
        let dateString = momentDateFrom.format('DD.MM.YYYY в HH:mm');

        //вычисляем до скольки приём на записываемую запись
        const dateTo: ScheduleItem =
            state.doctorSchedule.DayWeek.find(x => x.ID === state.idScheduleItemSelect) ||
            state.doctorSchedule.SpecialDays.find(x => x.ID === state.idScheduleItemSelect) ||
            new ScheduleItem();
        const momentDateTo = moment(date).add(dateTo.DURATION, 'm');

        let isAppointmentThisDoctor = false;
        
        //получаем свои записи на приём и фильтруем по времени
        ServiceController
            .getMyRecordingTimeAdmission(true, true)
            .then((appointmentsMy: Appointment[]) => {
                //те записи, что пересекаются с указанным диапазоном (от state.date до state.date + duration) 
                    //- что бы не было записей на одно время к разным врачам (приоритет 1)
                var res = appointmentsMy
                    .filter(x => {
                        const momentFrom = moment(x.FROM);
                        const momentTo = moment(x.FROM).add(x.DURATION, 'm');

                        return momentTo > momentDateFrom && momentFrom <= momentDateFrom ||
                            momentTo >= momentDateTo && momentFrom < momentDateTo;
                    });

                //если нет пересечений по времени - проверяем нет ли записей к врачам той же специальности (приоритет 2)
                if (!res.length) {
                    res = appointmentsMy
                        .filter(x => x.DOCTOR.DOCTOR_SPECIALITY.ID === state.doctorSpecialty.ID);
                }

                return res.map(x => new Appointment(x));
            })
            .then((currentAppointments: Appointment[]) => {
                //если нет пересечений со своими записями в тот же период
                if (!currentAppointments.length) {
                    isAppointmentThisDoctor = true;

                    //проверяем, есть ли собственные записи на текущее число к указанному врачу в указанной клиники (приоритет 3)
                    //это проверка последня т.к. этой перезаписью можно нечайянно стереть время приёма к другому врачу и не узнать об этом.
                        //данная перезапись всегда видна визуально - на одном поле времени (можно окрашивать свои записи в другой цвет, что бы нагляднее было)
                    return state.appointments.filter(x => x.ID_PATIENT === (App.user ? App.user.Id : ''));
                }

                return currentAppointments;
            })
            .then((currentAppointments: Appointment[]) => {
                //нашли пересечение записей
                if (currentAppointments.length) {
                    //спрашиваем у пользователя, хочет ли он перенести дату приёма
                    dateString = moment(currentAppointments[0].FROM).format('DD.MM.YYYY в HH:mm');
                    let string = dateString;

                    //имеется запись к ддругому врачу
                    if (!isAppointmentThisDoctor) {
                        string += ' к "' + currentAppointments[0].DOCTOR.DOCTOR_SPECIALITY.NAME + '" ' + currentAppointments[0].DOCTOR.FIO;
                    }

                    Confirm.Open('Вы уже записаны на ' + string + ', хотите перезаписаться?', [
                        { label: 'Да', callback: () => SendCreateAppointment(dispatch, getState, dateString) } as ConfirmStore.ButtonItem,
                        { label: 'Нет' } as ConfirmStore.ButtonItem,
                    ]);
                    return;
                }

                //Отправляем запрос
                SendCreateAppointment(dispatch, getState, dateString);
            });
    }
};

/**
 * Открытие формы
 */
function Open(dispatch: any, getState: any, doctor: Doctor, clinic: Clinic | ClinicMin, doctorSpecialty: DoctorSpecialty,
    dateTimeFrom: Date | null, dateTimeTo: Date | null) {

    if (doctor.ID !== getState().recordingAdmission.doctor.ID || clinic.ID !== getState().recordingAdmission.clinic.ID) {
        App.Store.dispatch(actionCreators.requestGetSchedule(doctor.ID, clinic.ID));
    }

    App.Store.dispatch(actionCreators.requestGetAppointment(new Date(), doctor.ID, clinic.ID));
    App.Store.dispatch(actionCreators.requestGetBusyDays(doctor.ID, clinic.ID));

    dispatch({
        type: 'RecordingAdmission__OPEN', doctor: doctor, clinic: clinic, doctorSpecialty: doctorSpecialty,
        dateTimeFrom: dateTimeFrom, dateTimeTo: dateTimeTo
    });
}

/**
 * Отправка запроса на создание записи на приём
 */
function SendCreateAppointment(dispatch: any, getState: any, dateString: string) {

    const appointment = new AppointmentForm();
    appointment.datetime = Helper.timeToISOString(getState().recordingAdmission.date);
    appointment.idSchedule = getState().recordingAdmission.idScheduleItemSelect;
    appointment.phone = getState().recordingAdmission.phone;

    ServiceController
        .sendAddAppointment(appointment)
        .then((appointmentNew: Appointment) => {
            //успешно создана запись
            dispatch({ type: 'RecordingAdmission__CLOSE' });

            //составляем информацию о записи
            const doctor = getState().recordingAdmission.doctor;
            let info = dateString;
            info += Helper.format(' к врачу: {0} {1}.{2}.', [doctor.DOCTOR_F, doctor.DOCTOR_I[0], doctor.DOCTOR_O[0]]);
            if (appointmentNew.CABINET) {
                info += ' Кабинет: ' + appointmentNew.CABINET;
            }
            if (appointmentNew.STOREY) {
                info += ' Этаж: ' + appointmentNew.STOREY;
            }

            //показываем информацию о записи
            Confirm.Open('Вы успешно записаны на приём ' + info + '.', [
                { label: 'ОК' } as ConfirmStore.ButtonItem,
                {
                    label: '<i class="fa fa-print"></i>',
                    callback: () => App.Store.dispatch(PrintTalonStore.actionCreators.print(appointmentNew)),
                    title: 'Распечатать талон'
                } as ConfirmStore.ButtonItem,
            ], 'Успешная запись');
        });
}


// REDUCER 
export const reducer: Reducer<RecordingAdmissionState> = (state: RecordingAdmissionState, action: KnownAction) => {
    const stateNew: RecordingAdmissionState = {
        isClose: state ? state.isClose : true,
        clinic: state ? state.clinic : new ClinicMin(),
        doctor: state ? state.doctor : new Doctor(),
        doctorSchedule: state ? state.doctorSchedule : new Schedule(),
        doctorSpecialty: state ? state.doctorSpecialty : new DoctorSpecialty(),
        date: state ? state.date : new Date(),
        errorEnty: state ? state.errorEnty : '',
        phone: state ? state.phone : '',
        idScheduleItemSelect: state ? state.idScheduleItemSelect : -1,
        appointments: state ? state.appointments : [],
        busyDays: state ? state.busyDays : [],
        dateTimeFrom: state ? state.dateTimeFrom : null,
        dateTimeTo: state ? state.dateTimeTo : null,

        isLoadingAppointments: state ? state.isLoadingAppointments : true,
        isLoadingSchedule: state ? state.isLoadingSchedule : true,
        isLoadingBusyDays: state ? state.isLoadingBusyDays : true,
    }

    switch (action.type) {
        case 'RecordingAdmission__CLOSE':
            stateNew.isClose = true;
            break;

        case 'RecordingAdmission__OPEN':
            stateNew.isClose = false;
            stateNew.errorEnty = '';
            stateNew.date = new Date();
            stateNew.clinic = action.clinic;
            stateNew.doctor = action.doctor;
            stateNew.doctorSpecialty = action.doctorSpecialty;
            stateNew.dateTimeFrom = action.dateTimeFrom;
            stateNew.dateTimeTo = action.dateTimeTo;
            break;

        case 'RecordingAdmission__CHANGE_DATE':
            stateNew.date = action.date;
            break;

        case 'RecordingAdmission__CHANGE_TIME':
            stateNew.idScheduleItemSelect = action.idScheduleItemSelect;
            break;

        case 'RecordingAdmission__CHANGE_PHONE':
            stateNew.phone = action.phone;
            break;
            
        case 'RecordingAdmission__ENTRY_ERROR':
            stateNew.errorEnty = action.errorEnty;
            break; 

        case 'RecordingAdmission__CHANGE_APPOINTMENTS':
            stateNew.appointments = action.appointments;
            break;

        case 'RecordingAdmission__CHANGE_BUSY_DAYS':
            stateNew.busyDays = action.busyDays;
            break;

        case 'RecordingAdmission__CHANGE_SCHEDULE':
            stateNew.doctorSchedule = action.doctorSchedule;
            break;

        case 'RecordingAdmission__CHANGE_IS_LOADING_SCHEDULE':
            stateNew.isLoadingSchedule = action.isLoading;
            break;

        case 'RecordingAdmission__CHANGE_IS_LOADING_APPONTMENTS':
            stateNew.isLoadingAppointments = action.isLoading;
            break;

        case 'RecordingAdmission__CHANGE_IS_LOADING_BUSY_DAYS':
            stateNew.isLoadingBusyDays = action.isLoading;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
