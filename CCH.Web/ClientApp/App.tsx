import { Store } from "react-redux";
import { History } from 'history';
import { ApplicationState } from "./store";
import * as AppStore from "./store/AppStore";
import User from "./models/User";

/**
 * Класс содержащий основные экземпляры приложения
 */
export default class App {
    /**
     * Залогиненный пользователь системы
     * @return {User}
     */
    public static get user(): User | null {
        const user = localStorage.getItem('user'); //было бы хорошо использовать cookie, но они заняты asp net

        if (!user) {
            return null;
        }

        let parsedUser = JSON.parse(user);
        const a: boolean | null = parsedUser && parsedUser.Name && parsedUser.Name === 'Администратор';
        if (a !== null)
            parsedUser.IsAdmin = a;
        return parsedUser;
    }

    public static set user(value: User | null) {
        App.Store.dispatch(AppStore.actionCreators.changeUser(value));
    }

    /**
     * Хранилище состояний
     */
    public static Store: Store<ApplicationState>

    /**
     * История перемещений между страниц - для редиректа
     */
    public static History: History;
}