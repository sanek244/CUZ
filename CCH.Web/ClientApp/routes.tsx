import * as React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import MainPage from './pages/Main/MainPage';
import ClinicsCrudPage from './pages/ClinicsCrud/ClinicsCrudPage';
import ClinicViewPage from './pages/ClinicView/ClinicViewPage';
import DoctorViewPage from './pages/DoctorView/DoctorViewPage';
import ClinicDoctorsSearchPage from './pages/ClinicDoctorsSearch/ClinicDoctorsSearchPage';
import CalendarPage from './pages/Calendar/CalendarPage';
import AuthorizationPage from './pages/Authorization/AuthorizationPage';
import RegistrationPage from './pages/Registration/RegistrationPage';
import NotFoundPage from './pages/NotFound/NotFoundPage';
import CabinetPage from './pages/Cabinet/CabinetPage';
import DoctorsCrudPage from './pages/DoctorsCrud/DoctorsCrudPage';
import ServiceController from './controllers/ServiceController';
import App from "./App";

export const publicUrls = [
    '/login',
    '/registration'
];

//�������� jwt ������
ServiceController.sendCheckTokenJWT();

export const routes = <div>
    {!App.user && publicUrls.filter(x => location.pathname.indexOf(x) === 0).length === 0
        ? <Redirect to='/login' />
        : null
    }
    <Layout>
        <Switch>
            <Route exact path='/' component={MainPage} />
            <Route exact path='/clinics' component={ClinicsCrudPage} />
            <Route exact path='/clinics/:id' component={ClinicViewPage} />
            <Route exact path='/clinics/:idClinic/doctors' component={ClinicDoctorsSearchPage} />
            <Route exact path='/clinics/all/doctors/:id' component={DoctorViewPage} />
            <Route exact path='/doctors' component={DoctorsCrudPage} />
            <Route path='/calendar' component={CalendarPage} />
            <Route path='/cabinet' component={CabinetPage} />
            <Route exact path='/login' component={AuthorizationPage} />
            <Route exact path='/registration' component={RegistrationPage} />
            <Route path="*" component={NotFoundPage} />
        </Switch>
    </Layout>
</div>;
