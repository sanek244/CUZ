﻿import { Action, Reducer } from 'redux';
import User from "../models/User";

export const listChangeUserCallbacks: Function[] = [];

// STATE
export interface AppState {
    user: User | null
}

// ACTIONS
interface ChangeUserAction { type: 'App__CHANGE_USER', user: User | null }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = ChangeUserAction | BagAction;

// ACTION CREATORS
export const actionCreators = {
    changeUser: (user: User | null) => <ChangeUserAction>{ type: 'App__CHANGE_USER', user: user },
};

// REDUCER 
export const reducer: Reducer<AppState> = (state: AppState, action: KnownAction) => {
    const newState: AppState = {
        user: state ? state.user : null,
    }

    switch (action.type) {
        case 'App__CHANGE_USER':
            newState.user = action.user;
            //сохраняем в локальное хранилище (время хранения не ограничего)
            localStorage.setItem('user', JSON.stringify(action.user));
            listChangeUserCallbacks.map(x => x());
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return newState;
};
