import * as AlertStore from "../components/Alert/AlertStore";
import * as WaitModalStore from "../components/WaitModal/WaitModalStore";
import * as ConfirmStore from "../components/Confirm/ConfirmStore";
import * as EvaluationServiceStore from '../components/EvaluationService/EvaluationServiceStore';
import * as ClinicViewPageStore from '../pages/ClinicView/ClinicViewPageStore';
import * as DoctorViewPageStore from '../pages/DoctorView/DoctorViewPageStore';
import * as ClinicsCrudPageStore from '../pages/ClinicsCrud/ClinicsCrudPageStore';
import * as SearchDoctorsFormStore from '../components/SearchDoctorsForm/SearchDoctorsFormStore';
import * as CalendarPageStore from '../pages/Calendar/CalendarPageStore';
import * as CalendarEventPopoverStore from '../pages/Calendar/EventPopover/CalendarEventPopoverStore';
import * as RecordingAdmissionStore from '../components/RecordingAdmission/RecordingAdmissionStore';
import * as PrintTalonStore from '../components/PrintTalon/PrintTalonStore';
import * as PrintRoutingListStore from '../components/PrintRoutingList/PrintRoutingListStore';
import * as RegistrationPageStore from '../pages/Registration/RegistrationPageStore';
import * as AuthorizationPageStore from '../pages/Authorization/AuthorizationPageStore';
import * as CabinetPageStore from '../pages/Cabinet/CabinetPageStore';
import * as CheckBoxHorizontalStore from '../components/CheckBoxHorizontal/CheckBoxHorizontalStore';
import * as SelectStore from '../components/Select/SelectStore';
import * as ConfirmSelectDoctorClinicStore from '../components/ConfirmSelectDoctorClinic/ConfirmSelectDoctorClinicStore';
import * as AppStore from './AppStore';
import * as ClinicEditStore from '../components/ClinicEdit/ClinicEditStore';
import * as DoctorsCrudPageStore from '../pages/DoctorsCrud/DoctorsCrudPageStore';
import * as DoctorEditStore from '../components/DoctorEdit/DoctorEditStore';

// The top-level state object
export interface ApplicationState {
    alert: AlertStore.AlertState;
    WaitModal: WaitModalStore.WaitModalState;
    confirm: ConfirmStore.ConfirmState;
    evaluationService: EvaluationServiceStore.EvaluationServiceState;
    clinicViewPage: ClinicViewPageStore.ClinicViewPageState;
    doctorViewPage: DoctorViewPageStore.DoctorViewPageState;
    searchDoctorsForm: SearchDoctorsFormStore.SearchDoctorsFormState;
    calendarPage: CalendarPageStore.CalendarPageState;
    recordingAdmission: RecordingAdmissionStore.RecordingAdmissionState;
    registrationPage: RegistrationPageStore.RegistrationPageState;
    authorizationPage: AuthorizationPageStore.AuthorizationPageState;
    printTalon: PrintTalonStore.PrintTalonState;
    printRoutingList: PrintRoutingListStore.PrintRoutingListState;
    calendarEventPopover: CalendarEventPopoverStore.CalendarEventPopoverState;
    cabinetPage: CabinetPageStore.CabinetPageState;
    checkBoxHorizontal: CheckBoxHorizontalStore.CheckBoxHorizontalState;
    select: SelectStore.SelectState;
    confirmSelectDoctorClinic: ConfirmSelectDoctorClinicStore.ConfirmSelectDoctorClinicState;
    app: AppStore.AppState;
    clinicEdit: ClinicEditStore.ClinicEditState;
    clinicsCrudPage: ClinicsCrudPageStore.ClinicsCrudPageState;
    doctorEdit: DoctorEditStore.DoctorEditState;
    doctorCrudPage: DoctorsCrudPageStore.DoctorsCrudPageState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    alert: AlertStore.reducer,
    WaitModal: WaitModalStore.reducer,
    confirm: ConfirmStore.reducer,
    evaluationService: EvaluationServiceStore.reducer,
    clinicViewPage: ClinicViewPageStore.reducer,
    doctorViewPage: DoctorViewPageStore.reducer,
    clinicsCrudPage: ClinicsCrudPageStore.reducer,
    searchDoctorsForm: SearchDoctorsFormStore.reducer,
    calendarPage: CalendarPageStore.reducer,
    recordingAdmission: RecordingAdmissionStore.reducer,
    registrationPage: RegistrationPageStore.reducer,
    authorizationPage: AuthorizationPageStore.reducer,
    printTalon: PrintTalonStore.reducer,
    printRoutingList: PrintRoutingListStore.reducer,
    calendarEventPopover: CalendarEventPopoverStore.reducer,
    cabinetPage: CabinetPageStore.reducer,
    checkBoxHorizontal: CheckBoxHorizontalStore.reducer,
    select: SelectStore.reducer,
    confirmSelectDoctorClinic: ConfirmSelectDoctorClinicStore.reducer,
    app: AppStore.reducer,
    clinicEdit: ClinicEditStore.reducer,
    doctorEdit: DoctorEditStore.reducer,
    doctorCrudPage: DoctorsCrudPageStore.reducer,
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
