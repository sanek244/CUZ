var CryptoJS: any = require("crypto-js");

/**
 * Вспомогательный класс
 */
export default class Helper {
    /**
     * Форматирование строки
     * Helper.format('Покупай машину {0} за {1} рублей', ['mustang', 3100200]);   = 'Покупай машину mustang за 3100200 рублей'
     * @param str {string}
     * @param params {Array}
     */
    public static format(str: string, params: any[]) {
        const args = params;
        return str.replace(/{(\d+)}/g, (match, number) => {
            if (args[number] === null) {
                return 'null';
            }
            if (typeof args[number] === 'undefined') {
                return match;
            }
            if (typeof args[number] === 'object') {
                return JSON.stringify(args[number]);
            }
            return args[number];
        });
    }

    /**
     * Формат числа xx (для даты)
     * @param i
     * @return {string}
     * @testOk
     */
    static addZeroData(i: string | number) {
        i = parseInt(i + '');
        return i < 10 ? '0' + i : i;
    }

    static upFirstChar(str: string) {
        if (!str) {
            return '';
        }

        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    /**
     * For DateTime c#
     * @param time
     */
    static timeToISOString(time?: Date) {
        time = time || new Date();

        return time.getFullYear() +
            '-' + Helper.addZeroData(time.getMonth() + 1) +
            '-' + Helper.addZeroData(time.getDate()) +
            'T' + Helper.addZeroData(time.getHours()) +
            ':' + Helper.addZeroData(time.getMinutes()) +
            ':' + Helper.addZeroData(time.getSeconds()) +
            '.' + (time.getMilliseconds() / 1000).toFixed(3).slice(2, 5) +
            'Z';
    }

    /**
     * 128-битный алгоритм хеширования
     * @param {string} str - хешируемая строка
     * @return {string}
     */
    static md5(str: string) {
        let buffer = CryptoJS.enc.Utf16LE.parse(str);
        buffer = CryptoJS.MD5(buffer);
        return CryptoJS.enc.Base64.stringify(buffer);
    }

    /**
     * sort array by field
     * @param array
     * @param fun
     */
    static sortByField(array: any, fun: Function) {
        return array.sort((a: any, b: any) => {
            if (fun(a) > fun(b)) {
                return 1;
            }

            if (fun(a) < fun(b)) {
                return -1;
            }

            return 0;
        });
    };

    /**
     * Clone
     * @param obj
     */
    static clone(obj: any) {
        return JSON.parse(JSON.stringify(obj));
    }


    /**
     * Uid
     * @return {string}
     */
    static generateUid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
