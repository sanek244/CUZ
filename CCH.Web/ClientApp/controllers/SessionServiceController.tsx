import BaseServiceController from './BaseServiceController';

/**
 * Использование сессии страницы(html5) для уже имеющихся данных
 *
 * сессия активна только в течении жизни вкладки и в её пределах (дублирование вкладки в хроме происходит с копирование сессии)
 * при клике на ссылку и "Открыть в новой вкладе" - не копирует сессию в новую вкладку
 * F5 и Ctrl + F5 не очищает от сессии (в хроме)
 */
export default class SessionServiceController{

    /**
     * Запрос к сервису
     * @param {string} method - POST или GET
     * @param {string} url - url сервиса
     * @param {object} data - передаваемые данные
     * @param {string} serviceName - имя сервиса, куда отсылается запрос
     * @param {boolean} isCloseWait - закрывать окно с ползунком отправки запроса после получения ответа?
     *        -> бывает полезно выключить, когда запрос фоновый или их несколько одновременно идёт
     * @param {boolean} isOpenWait - открывать окно с ползунком отправки запроса? Надписью будет имя сервиса
     * @param {boolean} isOnShowError - показывать ошибки?
     * @param {boolean} forceSend - отправить запрос принудительно?
     * @return {Promise} параметр: object - данные от сервиса
     */
    static sendCoorsQuery(method: string, url: string, data: object | null = null, serviceName: string = '', isCloseWait = false, isOpenWait = false, isOnShowError = true, forceSend = false) {

        //ключ под которым сохраняется запись в кеш
        const key = url + ' ' + JSON.stringify(data);

        //просматриваем сессию и возвращаем данные, если есть
        const res = window.sessionStorage.getItem(key);
        if (res && !forceSend) {
            return new Promise(ok => ok(JSON.parse(res)));
        }

        //запрос на сервер
        return BaseServiceController.sendCoorsQuery(method, url, data, serviceName, isCloseWait, isOpenWait, isOnShowError)
            .then(data => {
                //Сохраняет результаты запроса в сессию страницы
                window.sessionStorage.setItem(key, JSON.stringify(data));
                return data;
            });
    }
}