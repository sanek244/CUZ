import App from "../App";
import Helper from "../Helper";
import Alert from "../components/Alert/Alert";
import WaitModal from "../components/WaitModal/WaitModal";
import Result from "../models/api/Result";
import OkResult from "../models/api/OkResult";
import ErrorResult from "../models/api/ErrorResult";

/**
 * Реализует функцию отправки запросов на сервер и базовую обработку ответа
 */
export default class BaseServiceController {

    /**
     * Запрос к сервису
     * @param {string} method - POST или GET
     * @param {string} url - url сервиса
     * @param {object} data - передаваемые данные
     * @param {string} serviceName - имя сервиса, куда отсылается запрос
     * @param {boolean} isCloseWaitModal - закрывать окно с ползунком отправки запроса после получения ответа?
     *        -> бывает полезно выключить, когда запрос фоновый или их несколько одновременно идёт
     * @param {boolean} isOpenWaitModal - открывать окно с ползунком отправки запроса? Надписью будет имя сервиса
     * @param {boolean} isOnShowError - показывать ошибки?
     * @return {Promise} параметр: object - данные от сервиса
     */
    static sendCoorsQuery(method: string, url: string, data: object | null = null, serviceName: string = '', isCloseWaitModal = false, isOpenWaitModal = false, isOnShowError = true) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();

            xhr.responseType = 'json';

            xhr.open(method, url, true);

            //токен пользователя
            const user = App.user;
            if (user && user.Token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + user.Token);
            }

            xhr.onload = () => {
                //закрытие окна загрузки
                if (isCloseWaitModal) {
                    WaitModal.Close();
                }

                //обработка по статусу ответа
                switch (xhr.status) {
                    case 200:
                        //если ответ соответствует json объекту с положительным результатом
                        if (xhr.response && typeof xhr.response === 'object' && (xhr.response as Result).IsSuccess) {
                            resolve((xhr.response as OkResult).Data); //возвращаем данные запроса
                            return;
                        }

                        if (isOnShowError) {
                            Alert.Open(Helper.format('С запросом к сервису "{0}" возникли проблемы! Попробуйте позже.', [serviceName]));
                        }
                        console.log('[Для разработчиков]: неверный формат данных с сервера! Используйте для возврата данных Ok или BadRequest из BaseController.cs');
                        break;

                    case 400:
                        //если ответ соответствует json объекту с отрицательным результатом
                        if (xhr.response && typeof xhr.response === 'object' && (xhr.response as Result).IsSuccess === false) {
                            const error = xhr.response as ErrorResult
                            if (error.ErrorData) {
                                console.log(error.ErrorData)
                            }

                            if (error.ErrorMessage) {
                                Alert.Open(error.ErrorMessage);
                                return;
                            }

                            //особая ошибка
                            reject(error.KeyErrors);
                            return;
                        }

                        if (isOnShowError) {
                            Alert.Open(Helper.format('С запросом к сервису "{0}" возникли проблемы! Попробуйте позже.', [serviceName]));
                        }
                        break;

                    case 401:
                        if (isOnShowError) {
                           // Alert.Open('У вас нет прав доступа или они уже истекли! Пожалуйста, войдите.');
                        }
                        App.History.push('/login');
                        App.user = null;
                        break;

                    case 404:
                        if (isOnShowError) {
                            Alert.Open(Helper.format('Сервис "{0}" не отвечает.', [serviceName]));
                        }
                        break;

                    case 502:
                        if (isOnShowError) {
                            Alert.Open('На сервере возникла ошибка! Попробуйте позже.');
                        }
                        break;

                    default:
                        if (isOnShowError) {
                            Alert.Open(Helper.format('С запросом к сервису "{0}" возникли проблемы! Попробуйте позже.', [serviceName]));
                        }
                        console.log('С запросом возникли проблемы. url: "' + url + '"', xhr);
                }

                //всё, что не обработалось - в ошибки
                reject(xhr.response);
            };

            //обработка ошибок запроса
            xhr.onerror = (res: object) => {
                //закрытие окна загрузки
                if (isCloseWaitModal) {
                    WaitModal.Close();
                }

                //зесли можно показывать ошибки
                if (isOnShowError) {
                    Alert.Open(Helper.format('Ошибка запроса к сервису "{0}"', [serviceName]));
                }

                console.log('Ошибка запроса к ' + url, res);

                reject(res);
            };

            //открытие окна загрузки
            if (isOpenWaitModal) {
                WaitModal.Open(serviceName || 'Отправка запроса');
            }

            //отправка запроса
            if (data) {
                xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
                xhr.send(JSON.stringify(data));
            }
            else {
                xhr.send();
            }
        });
    }
}