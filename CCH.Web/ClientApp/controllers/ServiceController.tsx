import BaseServiceController from './BaseServiceController';
import SessionServiceController from './SessionServiceController';
import Helper from "../Helper";
import RegistrationForm from "../models/forms/RegistrationForm";
import AppointmentForm from "../models/forms/AppointmentForm";
import Reminder from "../models/Reminder";
import SearchDoctrosForm from "../models/forms/SearchDoctrosForm";
import Clinic from "../models/Clinic";
import Doctor from "../models/Doctor";
import DoctorEdit from "../models/forms/DoctorEdit";

/**
 * Контроллер запросов к серверу 
 */
export default class ServiceController{

    public static host: string = '';

    /**
     * Клиники
     * @return {Promise<Clinic[]>}
     */
    static getClinics(forceSend: boolean) {
        return SessionServiceController.sendCoorsQuery('GET', this.host + '/api/clinics', null, 'Клиника', false, false, true, forceSend);
    }

    /**
     * Клиника по id
     * @param {number} id
     * @return {Promise<Clinic>}
     */
    static getClinic(id: string){
        return SessionServiceController.sendCoorsQuery('GET', this.host + 'api/clinics/' + id, null, 'Клиники');
    }

    /**
     * Врач по id
     * @param {number} id
     * @return {Promise<Doctor>}
     */
    static getDoctor(id: string) {
        return SessionServiceController.sendCoorsQuery('GET', this.host + 'api/doctors/' + id, null, 'Врачи', true, true, true);
    }

    /**
     * Специализации клиник
     * @return {Promise<ClinicSpecialization>}
     */
    static getClinicSpecializations(){
        return SessionServiceController.sendCoorsQuery('GET', this.host + '/api/clinicSpecializations', null, 'Специализация клиник');
    }

    /**
     * Специальности врачей
     * @return {Promise<ClinicSpecialization>}
     */
    static getDoctorSpecialities() {
        return SessionServiceController.sendCoorsQuery('GET', this.host + '/api/doctorSpecialities', null, 'Специализация врачей');
    }


    /**
     * Получение времени записи на приём
     * @return {Promise<Appointment[]>}
     */
    static getRecordingTimeAdmissionByDate(date: string, idDoctor: number, idClinic: number, isOnlyActiveStatus?: boolean) {
        return BaseServiceController.sendCoorsQuery(
            'GET',
            Helper.format('{0}/api/appointment?date={1}&idDoctor={2}&idClinic={3}{4}',
                [this.host, date, idDoctor, idClinic, isOnlyActiveStatus === true ? '&isOnlyActiveStatus=true' : '']),
            null,
            'График приёма');
    }

    /**
     * Получение своих записей на приём
     * @param isOnlyActiveStatus - все со статусом активен (ACTIVE, REQUIRES_CONFIRMATION)
     * @param isOnlyNotPassed - все с временем начала > чем текущее
     * @return {Promise<Appointment[]>}
     */
    static getMyRecordingTimeAdmission(isOnlyActiveStatus: boolean = false, isOnlyNotPassed: boolean = false) {
        return BaseServiceController.sendCoorsQuery(
            'GET',
            Helper.format('{0}/api/appointment/patient/my?isOnlyActiveStatus={1}&isOnlyNotPassed={2}', [this.host, isOnlyActiveStatus, isOnlyNotPassed]),
            null,
            'Записи на приём');
    }

    /**
     * Возвращает результаты поиска врачей
     * @return {Promise<Doctor[]>}
     */
    static getSearchResultDoctors(searchForm: SearchDoctrosForm) {
        return SessionServiceController.sendCoorsQuery('POST', this.host + '/api/doctors/search', searchForm, 'Поиск врачей');
    }

    /**
     * Получение расписания врача в клинике
     * @return {Promise<Schedule>}
     */
    static getSchedule(idDoctor: number, idClinic: number) {
        return SessionServiceController.sendCoorsQuery(
            'GET',
            this.host + '/api/doctors/schedule?idDoctor=' + idDoctor + '&idClinic=' + idClinic,
            null,
            'Расписание врача');
    }

    /**
     * Получает клиники, к которым привязан врач
     * @param idDoctor
     * @param isOnlyWithSheduleItems - Только те клиники, в которых врач имеет график приёма
     * @return {Promise<ClinicMin[]>}
     */
    static getDoctorClinics(idDoctor: number | string, isOnlyWithSheduleItems: boolean) {
        return SessionServiceController.sendCoorsQuery(
            'GET',
            this.host + '/api/doctors/clinics?idDoctor=' + idDoctor + '&isOnlyWithSheduleItems=' + isOnlyWithSheduleItems,
            null,
            'Клиники врача'
        );
    }

    /**
     * Получение своих настроек напоминаний
     * @return {Promise<Appointment[]>}
     */
    static getMyReminder() {
        return BaseServiceController.sendCoorsQuery('GET', this.host + '/api/reminder/my', null, 'Настройки напоминаний', true, true, true);
    }

    /**
     * Получение дней, в которых свободные записи кончились
     * @return {Promise<Appointment[]>}
     */
    static getBusyDay(idDoctor: number, idClinic: number) {
        return BaseServiceController.sendCoorsQuery('GET', this.host + '/api/doctors/busy_days?idDoctor=' + idDoctor + '&idClinic=' + idClinic, null, 'Проверка свободных записей', true, true, true);
    }









    /**
     * Авторизация
     * @return {Promise<object>}
     */
    static sendAuthorization(data: object) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/Tokens', data, 'Авторизация', true, true, true);
    }

    /**
     * Выход из системы
     * @return {Promise<object>}
     */
    static sendLogout() {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/Account/Signout', null, 'Выход из системы', false, false, false);
    }

    /**
     * Регистрация
     * @return {Promise<void>}
     */
    static sendRegistration(data: RegistrationForm) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/Account/Checkin', data, 'Регистрация', true, true, true);
    }

    /**
     * Запись на приём
     * @return {Promise<void>}
     */
    static sendAddAppointment(data: AppointmentForm) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/appointment/create', data, 'Запись на приём', true, true, true);
    }

    /**
     * Отмена записи на приём
     * @return {Promise<void>}
     */
    static sendCancelAppointment(idAppointment: number) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/appointment/cancel?idAppointment=' + idAppointment, null, 'Отмена записи на приём', true, true, true);
    }

    /**
     * Отправка данных о новых настройках напоминаний
     * @return {Promise<void>}
     */
    static sendSaveReminders(data: Reminder) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/reminder/my/change', data, 'Сохранение настроек напоминания', true, true, true);
    }

    /**
     * Отправка проверкиJWT токена
     * @return {Promise<void>}
     */
    static sendCheckTokenJWT() {
        return BaseServiceController.sendCoorsQuery('POST', this.host + '/api/Tokens/check', null, 'Проверка токена авторизации', false, false, true);
    }

    /**
     * Отправка данных о новых настройках клиники
     * @return {Promise<void>}
     */
    static sendUpdateClinic(data: Clinic) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + 'api/clinics/update', data, 'Обновление клиники', true, true, true);
    }

    /**
     * Отправка данных о новых настройках врача
     * @return {Promise<void>}
     */
    static sendUpdateDoctor(data: DoctorEdit) {
        return BaseServiceController.sendCoorsQuery('POST', this.host + 'api/doctors/update', data, 'Обновление данных врача', true, true, true);
    }
}