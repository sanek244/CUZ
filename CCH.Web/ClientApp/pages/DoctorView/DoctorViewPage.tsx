﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import * as moment from 'moment';

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as DoctorViewPageStore from './DoctorViewPageStore';
import DoctorSpecialty from '../../models/DoctorSpecialty';
import DoctorPosition from '../../models/DoctorPosition';
import Doctor from '../../models/Doctor';
import WorkExperience from '../../models/WorkExperience';
import * as RecordingAdmissionStore from '../../components/RecordingAdmission/RecordingAdmissionStore';
import RecordingAdmission from '../../components/RecordingAdmission/RecordingAdmission';
import App from "../../App";

import './DoctorViewPage.less';

type Props =
    DoctorViewPageStore.DoctorViewPageState       
    & typeof DoctorViewPageStore.actionCreators      
    & RouteComponentProps<{ id: string }>; 

class DoctorViewPage extends BaseComponent<Props> {

    public Name: string = 'DoctorViewPage';

    
    public componentDidMount() {
        this.loadData(this.props.match.params.id || "0");
    }

    public componentWillReceiveProps(nextProps: Props) {
        if(this.props.match.params.id !== nextProps.match.params.id){
            this.loadData(nextProps.match.params.id || "0");
        }
    }

    public loadData(id: string) {
        this.props.requestDoctor(id);
    }

    public render() {
        const doctor = new Doctor(this.props.doctor);

        if (!doctor.DOCTOR_SPECIALITY)
            doctor.DOCTOR_SPECIALITY = new DoctorSpecialty();

        if (!doctor.POSITION)
            doctor.POSITION = new DoctorPosition();

        return <div className={this.getClassName()} >
            <BreadcrumbsItem to='/clinics'>Справочник врачей и клиник</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/all'}>Все клиники</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/all/doctors'}>Врачи клиники</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/all/doctors/' + doctor.ID}>{doctor.FIO}</BreadcrumbsItem>

            <h1 className={this.getClassName('title')}>{doctor.FIOFull}</h1>
            <Link to={'/clinics/all/doctors'} className={this.getClassName('button-search-doctor')}></Link>
            <div className={this.getClassName('block-info')}>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Специализация:</div>
                    <div>{doctor.DOCTOR_SPECIALITY.NAME}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Первичный приём:</div>
                    <div>{doctor.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION ? 'Да' : 'Нет'}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Должность:</div>
                    <div>{doctor.POSITION.NAME}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Текущий стаж работы:</div>
                    <div>{doctor.EXPERIENCE}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Место ведения приёма:</div>
                    {this.renderClinics()}
                </div>

                <h3 className={this.getClassName('block-info-title')}>Опыт работы</h3>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Стаж работы:</div>
                    {this.renderWorkExperience()}
                </div>
            </div>
            <div>
                <button
                    className={"btn text-white mt-auto " + this.getClassName('button-recording')}
                    onClick={() => App.Store.dispatch(RecordingAdmissionStore.actionCreators.open(doctor, null, doctor.DOCTOR_SPECIALITY, null, null))}
                >
                    <span className={this.getClassName('button-recording-text')}>ЗАПИСЬ НА ПРИЕМ</span>
                </button>
            </div>
            <RecordingAdmission {...App.Store.getState().recordingAdmission} {...RecordingAdmissionStore.actionCreators} />
        </div>;
    }

    renderWorkExperience() {
        if (!this.props.doctor.WORK_EXPERIENCES || !this.props.doctor.WORK_EXPERIENCES.length) {
            return '-';
        }

        return <ul className={this.getClassName('list')}>
            {this.props.doctor.WORK_EXPERIENCES.map(x => <li className={this.getClassName('list-item')}>
                <div><strong>Место:</strong> {x.PLACE}</div>
                <div><strong>Должность:</strong> {x.POSITION}</div>
                <div><strong>С</strong> {moment(x.FROM).format('YYYY.MM.DD')} <strong>По</strong> {moment(x.TO).format('YYYY.MM.DD')} <strong>Всего:</strong> {x.DURATION}</div>
            </li>)}
        </ul>;
    }

    renderClinics() {
        if (!this.props.clinics || !this.props.clinics.length) {
            return '-';
        }

        return <ul className={this.getClassName('list')}>
            {this.props.clinics.map(x =>
                <li className={this.getClassName('list-item')}>{x.FULL_NAME}</li>)}
        </ul>;
    }
}

export default connect(
    (state: ApplicationState) => state.doctorViewPage, 
    DoctorViewPageStore.actionCreators                
)(DoctorViewPage) as typeof DoctorViewPage;