﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";

import Doctor from "../../models/Doctor";
import Clinic from "../../models/Clinic";
import { AppThunkAction, reducers } from "../../store";
import WaitModal from "../../components/WaitModal/WaitModal";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";

// STATE
export interface DoctorViewPageState {
    doctor: Doctor,
    clinics: Clinic[], //больницы, где доктор работает и имеет расписание приёма
    isShowFullInfo: boolean, //показать всю информацию
}

// ACTIONS
interface SuccessLoadAction { type: 'DoctorViewPage__SUCCESS_LOAD', id: string, doctor: Doctor }
interface ChangeClinicsAction { type: 'DoctorViewPage__CHANGE_CLINICS', clinics: Clinic[] }
interface ShowMoreInfoAction { type: 'DoctorViewPage__SHOW_MORE_INFO' }

type KnownAction = SuccessLoadAction | ChangeClinicsAction | ShowMoreInfoAction;

// ACTION CREATORS
export const actionCreators = {
    requestDoctor: (id: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

        // Пока данных нету или ещё не загрузились
        if (id !== getState().doctorViewPage.doctor.ID + '') {
            dispatch({ type: 'DoctorViewPage__SUCCESS_LOAD', id: '-1', doctor: new Doctor() });
            ServiceController
                .getDoctor(id)
                .then((data: Doctor) => dispatch({ type: 'DoctorViewPage__SUCCESS_LOAD', id: id, doctor: new Doctor(data) }));

            dispatch({ type: 'DoctorViewPage__CHANGE_CLINICS', clinics: [] });
            ServiceController
                .getDoctorClinics(id, true)
                .then((data: Clinic[]) => dispatch({ type: 'DoctorViewPage__CHANGE_CLINICS', clinics: data }));
        }
    },

    showMoreInfo: () => <ShowMoreInfoAction>{ type: 'DoctorViewPage__SHOW_MORE_INFO' },
};

// REDUCER 
export const reducer: Reducer<DoctorViewPageState> = (state: DoctorViewPageState, action: KnownAction) => {
    const stateNew: DoctorViewPageState = {
        doctor: state ? state.doctor : new Doctor(),
        clinics: state ? state.clinics : [],
        isShowFullInfo: state ? state.isShowFullInfo : false,
    }

    switch (action.type) {
        case 'DoctorViewPage__SUCCESS_LOAD':
            // Новые данные
            if (action.id !== state.doctor.ID + '') {
                stateNew.doctor = action.doctor;
                stateNew.isShowFullInfo = false;
            }
            break;

        case 'DoctorViewPage__CHANGE_CLINICS':
            stateNew.clinics = action.clinics;
            break;

        case 'DoctorViewPage__SHOW_MORE_INFO':
            stateNew.isShowFullInfo = true;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
