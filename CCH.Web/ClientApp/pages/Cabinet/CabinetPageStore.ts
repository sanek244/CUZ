﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";

import Clinic from "../../models/Clinic";
import { AppThunkAction, reducers } from "../../store";
import Alert from "../../components/Alert/Alert";
import * as AlertStore from "../../components/Alert/AlertStore";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";
import Reminder from "../../models/Reminder";


export enum ItemMenu {
    REMINDER, //Напоминания
    COMMON, //Общее
}

// STATE
export interface CabinetPageState {
    activeItemMenu: ItemMenu,
    reminderSetiings: Reminder
}

// ACTIONS
interface ChangeActiveItemMenuAction { type: 'CabinetPage__CHANGE_ACTIVE_ITEM_MENU', activeItemMenu: ItemMenu }
interface GetRemiderSettingsAction { type: 'CabinetPage__GET_REMINDER_SETTINGS', reminderSetiings: Reminder }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = ChangeActiveItemMenuAction | GetRemiderSettingsAction | BagAction;

// ACTION CREATORS
export const actionCreators = {
    changeActiveItemMenu: (newActiveItemMenu: ItemMenu) => <ChangeActiveItemMenuAction>{ type: 'CabinetPage__CHANGE_ACTIVE_ITEM_MENU', activeItemMenu: newActiveItemMenu },

    //Получаем настройки напоминаний
    getRemiderSettings: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        if (App.user && App.user.Reminder) {
            dispatch({ type: 'CabinetPage__GET_REMINDER_SETTINGS', reminderSetiings: App.user.Reminder });
            return;
        }

        ServiceController
            .getMyReminder()
            .then((data: Reminder) => {
                if (App.user) {
                    App.user.Reminder = data;
                }
                App.Store.dispatch({ type: 'CabinetPage__GET_REMINDER_SETTINGS', reminderSetiings: data });
            });
    },

    //Сохраняем изменения на странице 'Напоминания'
    requestSaveReminder: (form: Reminder): AppThunkAction<KnownAction> => (dispatch, getState) => {
        ServiceController
            .sendSaveReminders(form)
            .then(() => {
                Alert.Open('Настройки успешно сохранены!', AlertStore.HeaderState.success);
                if (App.user) {
                    App.user.Reminder = form;
                }
            });
    },
};

// REDUCER 
export const reducer: Reducer<CabinetPageState> = (state: CabinetPageState, action: KnownAction) => {
    const stateNew: CabinetPageState = {
        activeItemMenu: state ? state.activeItemMenu : ItemMenu.COMMON,
        reminderSetiings: state ? state.reminderSetiings : new Reminder(),
    }

    switch (action.type) {
        case 'CabinetPage__CHANGE_ACTIVE_ITEM_MENU':
            stateNew.activeItemMenu = action.activeItemMenu;
            break;

        case 'CabinetPage__GET_REMINDER_SETTINGS':
            stateNew.reminderSetiings = action.reminderSetiings;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
