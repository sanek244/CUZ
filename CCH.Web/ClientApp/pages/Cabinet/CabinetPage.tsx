﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { connect } from 'react-redux';

import BaseComponent from '../../models/BaseComponent';
import CheckBoxHorizontal from '../../components/CheckBoxHorizontal/CheckBoxHorizontal';
import { Option} from '../../components/Select/Select';
import Select from '../../components/Select/Select';
import Alert from '../../components/Alert/Alert';
import { ApplicationState } from '../../store';
import * as CabinetPageStore from './CabinetPageStore';
import Reminder, { ReminderFrequencyLabel } from "../../models/Reminder";
import App from "../../App";
import Helper from "../../Helper";

import './CabinetPage.less';

type Props =
    CabinetPageStore.CabinetPageState       
    & typeof CabinetPageStore.actionCreators      
    & RouteComponentProps<{ id: string }>; 

/**
 * Личный кабинет
 */
class CabinetPage extends BaseComponent<Props> {

    public Name: string = 'CabinetPage';

    componentDidMount() {
        this.props.getRemiderSettings();
    }

    public render() {
        return <div className={this.getClassName()}>
            <BreadcrumbsItem to='/cabinet'>Личный кабинет</BreadcrumbsItem>

            {this.renderCalendarRemindersSettings()}
        </div>;
    }

    /**
     * Прорисовка настройки уведомлений о записях на приём по календарю
     */
    renderCalendarRemindersSettings() {

        if (!App.user) {
            Alert.Open('Отсутствует User!');
            return null;
        }

        //для selector по времени напоминаний (интервал 1 час)
        const times = [];
        for (let i = 0; i < 24; i++) {
            const time = Helper.addZeroData(i) + ':00';
            times.push({ value: time, title: time } as Option);
        }

        //для selector по частоте напоминаний 
        const frequency = Object.keys(ReminderFrequencyLabel).map(key => {
            return {
                value: key,
                title: ReminderFrequencyLabel[key]
            } as Option
        })

        const area = 'reminders';

        return <div className={this.getClassName(area)}>
            <h5>Настройка уведомлений</h5>

            <div className={this.getClassName(area + '-panel')}>
                {this.renderCheckBox(area, 'on-off', 'Напоминания о приёме', this.props.reminderSetiings.IsOn)}

                {this.renderSelect(area, 'frequency', 'Как часто напоминать?', frequency, this.props.reminderSetiings.Frequency + '')}
                {this.renderSelect(area, 'time', 'В какое время напоминать?', times, this.props.reminderSetiings.Time)}

                <div className='form-group row'>
                    <label className={'col-form-label ' + this.getClassName(area + '-labels')}>Как напоминать?</label>
                    <div className='col-sm-9'>
                        {this.renderSelectSub(area, 'push', 'Push', this.props.reminderSetiings.IsOnMethodPush)}
                        {this.renderSelectSub(area, 'sms', 'СМС', this.props.reminderSetiings.IsOnMethodSms)}
                        {this.renderSelectSub(area, 'call', 'Звонок', this.props.reminderSetiings.IsOnMethodCall)}
                        {this.renderSelectSub(area, 'email', 'Email', this.props.reminderSetiings.IsOnMethodEmail)}
                    </div>
                </div>

                <button className='btn button' onClick={() => this.saveReminder()}>Сохранить</button>
            </div>
        </div>;
    }

    /**
     * Прорисовка настройки данных пользователя
     */
    renderUserSettings() {

    } 

    renderCheckBox(area: string, id: string, label: string, value: boolean) {
        return <div className='form-group row'>
            <label
                {...{ for: this.getClassName(area + '-checkbox-' + id) }}
                className={'col-form-label ' + this.getClassName(area + '-labels')}
            >
                {label}
                </label>
            <div className={this.getClassName(area + '-input-box')}>
                <CheckBoxHorizontal value={value} id={this.getClassName(area + '-checkbox-' + id)} />
            </div>
        </div>
    }

    renderSelect(area: string, id: string, label: string, data: Option[], value: string) {
        return <div className='form-group row'>
            <label
                {...{ for: this.getClassName(area + '-select-' + id) }}
                className={'col-form-label ' + this.getClassName(area + '-labels')}
            >
                {label}
                    </label>
            <div className={this.getClassName(area + '-select-box')}>
                <Select id={this.getClassName(area + '-select-' + id)} data={data} value={value}/>
            </div>
        </div>
    }

    renderSelectSub(area: string, method: string, label: string, value: boolean) {
        return <div className='form-group row'>
            <label
                {...{ for: this.getClassName(area + '-select-method-' + method) }}
                className={'col-form-label ' + this.getClassName(area + '-labels-sub')}
            >
                {label}
            </label>
            <div className={'col-sm-6 ' + this.getClassName(area + '-input-box')}>
                <CheckBoxHorizontal value={value} id={this.getClassName(area + '-select-method-' + method)} />
            </div>
        </div>
    }

    /**
     * Сохраняет настройки на странице 'Напоминание'
     */
    saveReminder() {
        console.log(this.props.reminderSetiings.Time, Select.getValue(this.getClassName('reminders-select-time'), this.props.reminderSetiings.Time));
        const form = new Reminder();
        form.IsOn = CheckBoxHorizontal.getValue(this.getClassName('reminders-checkbox-on-off'), this.props.reminderSetiings.IsOn);
        form.IsOnMethodPush = CheckBoxHorizontal.getValue(this.getClassName('reminders-select-method-push'), this.props.reminderSetiings.IsOnMethodPush);
        form.IsOnMethodSms = CheckBoxHorizontal.getValue(this.getClassName('reminders-select-method-sms'), this.props.reminderSetiings.IsOnMethodSms);
        form.IsOnMethodCall = CheckBoxHorizontal.getValue(this.getClassName('reminders-select-method-call'), this.props.reminderSetiings.IsOnMethodCall);
        form.IsOnMethodEmail = CheckBoxHorizontal.getValue(this.getClassName('reminders-select-method-email'), this.props.reminderSetiings.IsOnMethodEmail);
        form.Time = Select.getValue(this.getClassName('reminders-select-time'), this.props.reminderSetiings.Time);
        form.Frequency = Select.getValue(this.getClassName('reminders-select-frequency'), this.props.reminderSetiings.Frequency);

        this.props.requestSaveReminder(form);
    }

}

export default connect(
    (state: ApplicationState) => state.cabinetPage, 
    CabinetPageStore.actionCreators                
)(CabinetPage) as typeof CabinetPage;