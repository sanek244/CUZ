﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";

import Clinic from "../../models/Clinic";
import { AppThunkAction, reducers } from "../../store";
import WaitModal from "../../components/WaitModal/WaitModal";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";

// STATE
export interface ClinicViewPageState {
    clinic: Clinic,
    isShowFullInfo: boolean, //показать всю информацию
}

// ACTIONS
interface SuccessLoadAction { type: 'ClinicViewPage__SUCCESS_LOAD', id: string, clinic: Clinic }
interface ShowMoreInfoAction { type: 'ClinicViewPage__SHOW_MORE_INFO' }

type KnownAction = SuccessLoadAction | ShowMoreInfoAction;

// ACTION CREATORS
export const actionCreators = {
    requestClinic: (id: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

        // Пока данных нету или ещё не загрузились
        if (id !== getState().clinicViewPage.clinic.ID + '') {
            WaitModal.Open('Получение данных о клинике');

            ServiceController
                .getClinic(id)
                .then((data: Clinic) => {
                    WaitModal.Close();
                    return dispatch({ type: 'ClinicViewPage__SUCCESS_LOAD', id: id, clinic: data });
                })
                .catch(() => WaitModal.Close());
        }
    },
    showMoreInfo: () => <ShowMoreInfoAction>{ type: 'ClinicViewPage__SHOW_MORE_INFO' },
};

// REDUCER 
export const reducer: Reducer<ClinicViewPageState> = (state: ClinicViewPageState, action: KnownAction) => {
    const stateNew: ClinicViewPageState = {
        clinic: state ? state.clinic : new Clinic(),
        isShowFullInfo: state ? state.isShowFullInfo : false,
    }

    switch (action.type) {
        case 'ClinicViewPage__SUCCESS_LOAD':
            // Новые данные
            if (action.id !== state.clinic.ID + '') {
                stateNew.clinic = action.clinic;
                stateNew.isShowFullInfo = false;
            }
            break;

        case 'ClinicViewPage__SHOW_MORE_INFO':
            stateNew.isShowFullInfo = true;
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
