﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as ClinicViewPageStore from './ClinicViewPageStore';

import './ClinicViewPage.less';

type Props =
    ClinicViewPageStore.ClinicViewPageState       
    & typeof ClinicViewPageStore.actionCreators      
    & RouteComponentProps<{ id: string }>; 

class ClinicViewPage extends BaseComponent<Props> {

    public Name: string = 'ClinicViewPage';

    
    public componentDidMount() {
        this.loadData(this.props.match.params.id || "0");
    }

    public componentWillReceiveProps(nextProps: Props) {
        if(this.props.match.params.id !== nextProps.match.params.id){
            this.loadData(nextProps.match.params.id || "0");
        }
    }

    public loadData(id: string) {
        this.props.requestClinic(id);
    }

    public render() {
        return <div className={this.getClassName()} >
            <BreadcrumbsItem to='/clinics'>Справочник врачей и клиник</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/' + this.props.clinic.ID}>{this.props.clinic.FULL_NAME}</BreadcrumbsItem>

            <h1 className={this.getClassName('title')}>{(this.props.clinic.FULL_NAME || '').toUpperCase()}</h1>
            <Link to={'/clinics/' + this.props.clinic.ID + '/doctors'} className={this.getClassName('button-search-doctor')}></Link>
            <div className={this.getClassName('block-info')}>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Адрес:</div>
                    <div>{this.props.clinic.ADDRESS_DESCRIPTION}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Телефон:</div>
                    <div>{this.props.clinic.PHONE_REGISTRY}</div>
                </div>
                <div className={this.getClassName('block-info-section')}>
                    <div className={this.getClassName('block-info-section-caption')}>Руководство:</div>
                    <div>
                        <p>{this.props.clinic.LEADER_F + ' ' + this.props.clinic.LEADER_I + ' ' + this.props.clinic.LEADER_O}</p>
                        <p>{this.props.clinic.PHONE_LEADER}</p>
                    </div>
                </div>
                <div>
                    <p>{this.props.clinic.DESCRIPTION}</p>
                </div>
                <div>
                    {!this.props.isShowFullInfo
                        ? <a className={"btn text-white " + this.getClassName('button-show-more')} onClick={() => this.props.showMoreInfo()}>
                            <span className={this.getClassName('button-show-more-text')}>ПОКАЗАТЬ ЕЩЕ</span>
                        </a>
                        : <div>
                            <h3 className={this.getClassName('block-info-title')}>Контактная информация</h3>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>Факс:</div>
                                <div>{this.props.clinic.FAX}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>Email:</div>
                                <div>{this.props.clinic.EMAIL}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>Сайт:</div>
                                <div>{this.props.clinic.SITE}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>Бухгалтер:</div>
                                <div>{this.props.clinic.BOOKER}</div>
                            </div>

                            <h3 className={this.getClassName('block-info-title')}>Реквизиты</h3>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>ИНН:</div>
                                <div>{this.props.clinic.INN}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>КПП:</div>
                                <div>{this.props.clinic.KPP}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>ОГРН:</div>
                                <div>{this.props.clinic.OGRN}</div>
                            </div>
                            <div className={this.getClassName('block-info-section')}>
                                <div className={this.getClassName('block-info-section-caption')}>ОКТМО:</div>
                                <div>{this.props.clinic.OKTMO}</div>
                            </div>

                        </div>
                    }
                </div>
            </div>
        </div>;
    }
}

export default connect(
    (state: ApplicationState) => state.clinicViewPage, 
    ClinicViewPageStore.actionCreators                
)(ClinicViewPage) as typeof ClinicViewPage;