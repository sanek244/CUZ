﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";

import Doctor from "../../models/Doctor";
import DoctorSpecialty from "../../models/DoctorSpecialty";
import { AppThunkAction } from "../../store";
import WaitModal from "../../components/WaitModal/WaitModal";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";

// STATE
export interface DoctorsCrudPageState {
    doctorSpecialty: DoctorSpecialty[],
    editDoctor: Doctor,
}

// ACTIONS
interface SuccessLoadDoctorSpecialtyAction { type: 'DoctorsCrudPage__SUCCESS_LOAD_DOCTOR_SPECIALTY', doctorSpecialty: DoctorSpecialty[] }
interface SelectDoctorToEdit { type: 'DoctorsCrudPage__SELECT_DOCTOR_TO_EDIT', editDoctor: Doctor }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = SuccessLoadDoctorSpecialtyAction | SelectDoctorToEdit | BagAction;

// ACTION CREATORS
export const actionCreators = {

    selectDoctorToEdit: (doctor: Doctor | null): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (!doctor)
            doctor = new Doctor;
        dispatch({ type: 'DoctorsCrudPage__SELECT_DOCTOR_TO_EDIT', editDoctor: doctor });
    },

    switchDeleteState: (doctor: Doctor): AppThunkAction<KnownAction> => (dispatch, getState) => {
        doctor.IS_DELETED = !doctor.IS_DELETED;

        const promise = ServiceController.sendUpdateDoctor(doctor)
            .then(() => {
                //TODO: заново запрашиваем поиск
            });

        addTask(promise);
    }
};

// REDUCER 
export const reducer: Reducer<DoctorsCrudPageState> = (state: DoctorsCrudPageState, action: KnownAction) => {
    const stateNew: DoctorsCrudPageState = {
        doctorSpecialty: state ? state.doctorSpecialty : [],
        editDoctor: state ? state.editDoctor : new Doctor(),
    }

    switch (action.type) {

        case 'DoctorsCrudPage__SUCCESS_LOAD_DOCTOR_SPECIALTY':
            stateNew.doctorSpecialty = action.doctorSpecialty;
            break;

        case 'DoctorsCrudPage__SELECT_DOCTOR_TO_EDIT':
            stateNew.editDoctor = action.editDoctor;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
