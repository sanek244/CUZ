﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import Select from 'react-select';

import BaseComponent from "../../models/BaseComponent";
import Doctor from "../../models/Doctor";
import { ApplicationState } from '../../store';
import * as DoctorsCrudPageStore from './DoctorsCrudPageStore';
import SearchDoctorsForm from '../../components/SearchDoctorsForm/SearchDoctorsForm';
import * as SearchDoctorsFormStore from '../../components/SearchDoctorsForm/SearchDoctorsFormStore';
import * as RecordingAdmissionStore from '../../components/RecordingAdmission/RecordingAdmissionStore';
import * as DoctorEditStore from '../../components/DoctorEdit/DoctorEditStore';
import RecordingAdmission from '../../components/RecordingAdmission/RecordingAdmission';
import DoctorEdit from '../../components/DoctorEdit/DoctorEdit';
import DoctorSpecialty from "../../models/DoctorSpecialty";
import Helper from "../../Helper";
import App from "../../App";

import './DoctorsCrudPage.less';

type Props =
    DoctorsCrudPageStore.DoctorsCrudPageState
    & typeof DoctorsCrudPageStore.actionCreators
    & RouteComponentProps<{}>;

/**
 * Все врачи клиники с сортировкой, фильтрацией и админским управлением (crud)
 */
class DoctorsCrudPage extends BaseComponent<Props> {

    public Name: string = 'DoctorsCrudPage';

    public componentWillMount() {
        App.Store.subscribe(() => this.forceUpdate());
    }

    get stateSearch() {
        return App.Store.getState().searchDoctorsForm;
    }

    public render() {
        let isAdmin = App.user ? App.user.IsAdmin : false;

        if (!isAdmin) {
            App.History.push('/clinics/all/doctors/');
            return null;
        }

        return <div className={this.getClassName()}>
            <BreadcrumbsItem to='/doctors'>Справочник врачей</BreadcrumbsItem>

            <div>
                <h1 className={this.getClassName('header')}>ВРАЧИ</h1>
                <button onClick={() => this.edit()}>Добавить врача</button>
            </div>
            <SearchDoctorsForm {...this.stateSearch} {...SearchDoctorsFormStore.actionCreators} />
            {this.renderSearchResults()}

            <RecordingAdmission {...App.Store.getState().recordingAdmission} {...RecordingAdmissionStore.actionCreators} />
            <DoctorEdit {...App.Store.getState().doctorEdit} {...DoctorEditStore.actionCreators} />
        </div>;
    }


    /**
     * Отрисовка результатов поиска
     * @param doctor
     */
    protected renderSearchResults() {
        return <div className={this.getClassName('list-results')}>
            {this.stateSearch.resultsSearch.length
                ? this.stateSearch.resultsSearch.map(result => this.renderItemResultSearch(result))
                : <div className={this.getClassName('list-results-none')}>Ничего не найдено! </div>
            }
        </div>
    }

    /**
     * Отрисовка отдельного элемента результата поиска
     * @param doctor
     */
    protected renderItemResultSearch(doctor: Doctor) {
        const doctorSpecialty = this.stateSearch.doctorSpecialities.find(x => x.ID === doctor.ID_DOCTOR_SPECIALITY) || new DoctorSpecialty();

        return <div className={this.getClassName('list-results-item')}>
            <Link
                to={'/clinics/all/doctors/' + doctor.ID}
                className={this.getClassName('list-results-item-name')}
            >
                {doctor.DOCTOR_F + ' ' + doctor.DOCTOR_I + ' ' + doctor.DOCTOR_O}
            </Link>
            <div className={this.getClassName('list-results-item-role')}>{doctorSpecialty ? doctorSpecialty.NAME : ''}</div>
            <div>
                <button
                    className={"btn text-white mt-auto " + this.getClassName('button-recording')}
                    onClick={() => this.openModal(doctor, doctorSpecialty)}
                >
                    <span className={this.getClassName('button-recording-text')}>ЗАПИСЬ НА ПРИЕМ</span>
                </button>
            </div>
            <div>
                <button onClick={() => this.edit(doctor)}>Редактировать</button>
                <button onClick={() => this.copy(doctor)}>Копировать</button>
                <button onClick={() => this.props.switchDeleteState(doctor)}>{doctor.IS_DELETED ? "Восстановить" : "Удалить"}</button>
            </div>
        </div>
    }

    openModal(doctor: Doctor, doctorSpecialty: DoctorSpecialty) {
        App.Store.dispatch(RecordingAdmissionStore.actionCreators.open(doctor, this.stateSearch.clinic, doctorSpecialty, null, null));
    }

    copy(doctor: Doctor) {
        doctor.ID = 0;
        this.edit(doctor);
    }

    edit(doctor?: Doctor) {
        if (!doctor)
            doctor = new Doctor();

        console.log('edit');
        App.Store.dispatch(DoctorEditStore.actionCreators.open(doctor, this.stateSearch.doctorSpecialities)); 
    }
}
export default connect(
    (state: ApplicationState) => state.doctorCrudPage,
    DoctorsCrudPageStore.actionCreators
)(DoctorsCrudPage) as typeof DoctorsCrudPage;