﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";

import Clinic from "../../models/Clinic";
import ClinicSpecialization from "../../models/ClinicSpecialization";
import { AppThunkAction } from "../../store";
import WaitModal from "../../components/WaitModal/WaitModal";
import App from "../../App";
import ServiceController from "../../controllers/ServiceController";

// STATE
export interface ClinicsCrudPageState {
    clinics: Clinic[],
    clinicSpecializations: ClinicSpecialization[],
    editClinic: Clinic,
}

// ACTIONS
interface SuccessLoadClinicSpecializationAction { type: 'ClinicsCrudPage__SUCCESS_LOAD_CLINIC_SPECIALIZATION', clinicSpecializations: ClinicSpecialization[] }
interface SuccessLoadClinicsAction { type: 'ClinicsCrudPage__SUCCESS_LOAD_CLINICS', clinics: Clinic[] }
interface SelectClinicToEdit { type: 'ClinicsCrudPage__SELECT_CLINIC_TO_EDIT', editClinic: Clinic }
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = SuccessLoadClinicSpecializationAction | SuccessLoadClinicsAction | SelectClinicToEdit | BagAction;

// ACTION CREATORS
export const actionCreators = {
    requestClinics: (update: boolean): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Для корректного получениния обновлений необходим небольшой таймаут, чтоб изменения успели примениться
        const promises = [];
		WaitModal.Open('Получение данных о клиниках');

        //получаем специализации
        if (!getState().clinicsCrudPage.clinicSpecializations.length) {
            promises.push(ServiceController.getClinicSpecializations()
                .then((data: ClinicSpecialization[]) => dispatch({
                    type: 'ClinicsCrudPage__SUCCESS_LOAD_CLINIC_SPECIALIZATION',
                    clinicSpecializations: data
                })));
        }

        //получаем клиники
        promises.push(ServiceController.getClinics(update)
            .then((data: Clinic[]) => dispatch({ type: 'ClinicsCrudPage__SUCCESS_LOAD_CLINICS', clinics: data })));


        //проверка на ошибки запросов
        const promise = Promise.all(promises)
            .then(() => WaitModal.Close())
            .catch((err) => WaitModal.Close());

        addTask(promise); // Ensure server-side prerendering waits for this to complete
    },

    selectClinicToEdit: (clinic: Clinic | null): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (!clinic)
            clinic = new Clinic;
        dispatch({ type: 'ClinicsCrudPage__SELECT_CLINIC_TO_EDIT', editClinic: clinic });
    },

    switchClinicDeleteState: (clinic: Clinic): AppThunkAction<KnownAction> => (dispatch, getState) => {
        clinic.IS_DELETED = !clinic.IS_DELETED;

        const promise = ServiceController.sendUpdateClinic(clinic)
            .then(() => {
                ServiceController.getClinics(true)
                    .then((data: Clinic[]) => dispatch({ type: 'ClinicsCrudPage__SUCCESS_LOAD_CLINICS', clinics: data }))
            });

        addTask(promise);
    }
};

// REDUCER 
export const reducer: Reducer<ClinicsCrudPageState> = (state: ClinicsCrudPageState, action: KnownAction) => {
    const stateNew: ClinicsCrudPageState = {
        clinics: state ? state.clinics : [],
        clinicSpecializations: state ? state.clinicSpecializations : [],
        editClinic: state ? state.editClinic : new Clinic(),
    }

    switch (action.type) {

        case 'ClinicsCrudPage__SUCCESS_LOAD_CLINIC_SPECIALIZATION':
            stateNew.clinicSpecializations = action.clinicSpecializations;
            break;

        case 'ClinicsCrudPage__SUCCESS_LOAD_CLINICS':
            stateNew.clinics = action.clinics;
            break;

        case 'ClinicsCrudPage__SELECT_CLINIC_TO_EDIT':
            stateNew.editClinic = action.editClinic;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
