﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from "react-router-dom";
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { connect } from "react-redux";

import BaseComponent from "../../models/BaseComponent";
import Clinic from "../../models/Clinic";
import ClinicSpecialization from "../../models/ClinicSpecialization";
import { ApplicationState } from '../../store';
import * as ClinicsCrudPageStore from './ClinicsCrudPageStore';
import * as ClinicEditStore from '../../components/ClinicEdit/ClinicEditStore';
import ClinicEdit from '../../components/ClinicEdit/ClinicEdit';
import App from "../../App";

import './ClinicsCrudPage.less';

type Props =
    ClinicsCrudPageStore.ClinicsCrudPageState
    & typeof ClinicsCrudPageStore.actionCreators
    & RouteComponentProps<{}>;

class ClinicsCrudPage extends BaseComponent<Props> {

    public Name: string = 'ClinicsCrudPage';
    private isAdmin = App.user ? App.user.IsAdmin : false;

    public componentDidMount() {
        this.props.requestClinics(false);
    }

    public render() {

        return <div className={this.getClassName()}>
            <BreadcrumbsItem to='/clinics'>Справочник врачей и клиник</BreadcrumbsItem>
            {this.renderHeaderAdminPanel()}
            {this.props.clinicSpecializations.length === 0
                ? <div className={this.getClassName('text-empty-data')}>Не найдены специализации клиник.</div>
                : this.props.clinicSpecializations.map(el => this.renderSection(el))}
            <ClinicEdit {...App.Store.getState().clinicEdit} {...ClinicEditStore.actionCreators} />
        </div>;
    }

    renderHeaderAdminPanel() {
        if (this.isAdmin)
            return <button onClick={() => this.editClinicShow(null)}>Добавить клинику</button>;
        else
            return null;
    }

    renderSection(section: ClinicSpecialization) {
        const clinics = this.props
            .clinics
            .filter(clinic => clinic.ID_CLINIC_SPECIALIZATION === section.ID && (!clinic.IS_DELETED || this.isAdmin));

        return <div className={this.getClassName('section')} key={section.ID}>
            <div className={this.getClassName('section-title')} style={{ backgroundColor: section.COLOR }}>{
                (section.NAME || '').toUpperCase()}</div>
            <ul className="nav">
                {clinics.length
                    ? clinics.map(el => this.renderSectionItem(section, el))
                    : <div className={this.getClassName('text-empty-data')}>Клиник не найдено.</div>}
            </ul>
        </div>;
    }

    renderSectionItem(section: ClinicSpecialization, clinic: Clinic) {
        return <li className={'nav-item ' + this.getClassName('section-item')} key={clinic.ID}>
            <Link to={"/clinics/" + clinic.ID} className="nav-link">
                <img src={section.IMG || '/media/ClinicSpecialization/ico_lc_t1.png'} alt="" className={this.getClassName('section-item-img')} />
            </Link>
            <span className={this.getClassName('section-item-text')}> {clinic.NAME}<br /> {clinic.ADDRESS_SHORT}</span>
            {this.renderSectionAdminPanel(clinic)}
        </li>;
    }

    renderSectionAdminPanel(clinic: Clinic) {
        const delRestButtonText = clinic.IS_DELETED ? "Восстановить" : "Удалить";
        if (this.isAdmin)
            return <div>
                <button onClick={() => this.editClinicShow(clinic)}>Редактировать</button>
                <button onClick={() => this.copyClinic(clinic)}>Копировать</button>
                <button onClick={() => this.props.switchClinicDeleteState(clinic)}>{delRestButtonText}</button>
            </div>
        else
            return null;
    }

    copyClinic(clinic: Clinic) {
        clinic.ID = 0;
        this.editClinicShow(clinic);
    }

    editClinicShow(clinic: Clinic | null) {
        if (!clinic)
            clinic = new Clinic();
        App.Store.dispatch(ClinicEditStore.actionCreators.open(clinic, this.props.clinicSpecializations));
    }

}

export default connect(
    (state: ApplicationState) => state.clinicsCrudPage,
    ClinicsCrudPageStore.actionCreators
)(ClinicsCrudPage) as typeof ClinicsCrudPage;
