﻿import * as React from 'react';

import BaseComponent from "../../../models/BaseComponent";
import CalendarPage from '../CalendarPage';
import * as CalendarPageStore from '../CalendarPageStore';
import { StatusAppointmentEnum } from '../../../models/Appointment';
import * as PrintRoutingListStore from '../../../components/PrintRoutingList/PrintRoutingListStore';
import App from '../../../App';

import './CalendarToolBar.less';

let navigate = {
    PREVIOUS: 'PREV',
    NEXT: 'NEXT',
    TODAY: 'TODAY',
    DATE: 'DATE',
}

interface Props {
    view: any,
    views: any,
    label: any,
    messages: any,
    onNavigate: any,
    onViewChange: any,
}

/**
 * Шапка календаря
 * Код взят из https://github.com/intljusticemission/react-big-calendar/blob/master/src/Toolbar.js
 */
export default class CalendarToolBar extends BaseComponent<Props> {

    public Name: string = 'CalendarToolBar';


    buttonCalendarAgenda: HTMLButtonElement | null;

    //для снятия выделения
    onClickOutsideButtonAgenda(event: any) {
        //если есть выделение и клик был не по кнопке открытия периода
        if (App.Store.getState().calendarPage.view !== 'agenda' &&
            App.Store.getState().calendarPage.agendaStart &&
            this.buttonCalendarAgenda &&
            !this.buttonCalendarAgenda.contains(event.target))
        {
            App.Store.dispatch(CalendarPageStore.actionCreators.changeAgenda(null, null));
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutsideButtonAgenda.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutsideButtonAgenda.bind(this));
    }

    render() {
        let { messages, label, view } = this.props
        if (typeof label === 'string') {
            label = label[0].toUpperCase() + label.substr(1);
        }

        const calendarPageState = App.Store.getState().calendarPage;
        const isOnlyActiveEvents = calendarPageState.isOnlyActiveEvents;
        const activeAppointments = calendarPageState.myAppointments.map(x => x.appointment).filter(x => x.STATUS === StatusAppointmentEnum.ACTIVE);

        return (
            <div className={this.getClassName()}>
                <div className={this.getClassName('control-panel')}>
                    <span
                        className={this.getClassName('checkbox-container')}
                        onClick={() => CalendarPage.changeUrl(calendarPageState.view, calendarPageState.date, !isOnlyActiveEvents, calendarPageState.agendaLength)}
                    >
                        <div className={this.getClassName('checkbox', isOnlyActiveEvents ? 'active' : '')}>
                            <i className="fa fa-check"></i>
                        </div> 
                        <span className={this.getClassName('checkbox-text')}>Показывать только активные записи</span>
                    </span>
                    <button 
                        className={'btn ' + this.getClassName('control-button', 'print')}
                        onClick={() => App.Store.dispatch(PrintRoutingListStore.actionCreators.print(activeAppointments))}
                    >
                        НАПЕЧАТАТЬ МАРШРУТНЫЙ ЛИСТ
                    </button>
                </div>
                <div className={this.getClassName('navigation')}>
                    <span className={this.getClassName('navigation-block-left')}>
                        <button
                            type="button"
                            onClick={this.navigate.bind(null, navigate.PREVIOUS)}
                            className={'btn ' + this.getClassName('navigation-button', 'prev')}
                            disabled={view === 'agenda'}
                        >
                            <i className="fa fa-angle-left"></i>
                        </button>
                        <button
                            type="button"
                            onClick={this.navigate.bind(null, navigate.NEXT)}
                            className={'btn ' + this.getClassName('navigation-button', 'next')}
                            disabled={view === 'agenda'}
                        >
                            <i className="fa fa-angle-right"></i>
                        </button>
                        <button
                            type="button"
                            onClick={this.navigate.bind(null, navigate.TODAY)}
                            className={'btn ' + this.getClassName('navigation-button', 'today')}
                        >
                            {messages.today}
                        </button>
                    </span>

                    <h2 className={this.getClassName('navigation-block-label')}>{label}</h2>

                    <span className={this.getClassName('navigation-block-right')}>
                        {this.viewNameGroup(messages, 'agenda')}
                        {this.viewNameGroup(messages, 'month')}
                        {this.viewNameGroup(messages, 'week')}
                        {this.viewNameGroup(messages, 'day')}
                    </span>
                </div>
            </div>
        )
    }

    navigate = (action: any) => {
        this.props.onNavigate(action)
    }

    view = (view: any) => {
        this.props.onViewChange(view)
    }

    viewNameGroup(messages: any, view: 'month' | 'agenda' | 'week' | 'day') {
        return <button
            type="button"
            key={view}
            className={'btn ' + this.getClassName('navigation-button', [view, this.props.view === view ? 'active' : ''])}
            onClick={this.view.bind(null, view)}
            disabled={view === 'agenda' && !App.Store.getState().calendarPage.agendaStart}
            ref={button => {
                if (view === 'agenda') {
                    this.buttonCalendarAgenda = button;
                }
            }}
        >
            {messages[view]}
        </button>
    }
}
