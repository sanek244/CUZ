﻿import { Action, Reducer } from 'redux';
import { addTask } from "domain-task";
import * as moment from 'moment';

import Appointment from "../../models/Appointment";
import ServiceController from "../../controllers/ServiceController";
import { AppThunkAction } from "../../store";
import App from "../../App";

type viewEnum = 'week' | 'day' | 'month' | 'agenda';

export interface Event {
    start: Date,
    end: Date,
    appointment: Appointment
}

// STATE
export interface CalendarPageState {
    view: viewEnum,
    date: Date,
    agendaStart: Date | null,
    agendaLength: number,

    myAppointments: Event[],

    isOnlyActiveEvents: boolean //показывать только активные события?
}


// ACTIONS
interface ChangeViewAction { type: 'CalendarPage__CHANGE_VIEW', view: viewEnum }
interface ChangeDateAction { type: 'CalendarPage__CHANGE_DATE', date: Date }
interface ChangeIsOnlyActiveEventsAction { type: 'CalendarPage__CHANGE_IS_ONLY_ACTIVE_EVENTS', isOnlyActiveEvents: boolean }
interface ChangeAgendaAction { type: 'CalendarPage__CHANGE_AGENDA', agendaStart: Date | null, agendaLength: number }
interface ChangeAgendaLengthAction { type: 'CalendarPage__CHANGE_AGENDA_LENGTH', agendaLength: number }
interface LoadAppointmentsAction { type: 'CalendarPage__LOAD_APPOINTMENTS', myAppointments: Event[]}
interface BagAction { type: 'BAG' } //баг TypeScript - ругается, если нету ни одного Action с пустыми параметрами(кроме type)

type KnownAction = ChangeViewAction | ChangeDateAction | ChangeAgendaAction | LoadAppointmentsAction | ChangeIsOnlyActiveEventsAction | ChangeAgendaLengthAction | BagAction;

// ACTION CREATORS
export const actionCreators = {
    changeView: (view: viewEnum) => <ChangeViewAction>{ type: 'CalendarPage__CHANGE_VIEW', view: view},
    changeDate: (date: Date) => <ChangeDateAction>{ type: 'CalendarPage__CHANGE_DATE', date: date },
    changeIsOnlyActiveEvents: (isOnlyActiveEvents: boolean) => <ChangeIsOnlyActiveEventsAction>{ type: 'CalendarPage__CHANGE_IS_ONLY_ACTIVE_EVENTS', isOnlyActiveEvents: isOnlyActiveEvents },
    changeAgenda: (agendaStart: Date | null, agendaEnd: Date | null) => {
        if (agendaStart && agendaEnd) {
            var timeDiff = Math.abs(agendaStart.getTime() - agendaEnd.getTime());
            const agendaLength = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return <ChangeAgendaAction>{ type: 'CalendarPage__CHANGE_AGENDA', agendaStart: agendaStart, agendaLength: agendaLength }
        }

        return <ChangeAgendaAction>{ type: 'CalendarPage__CHANGE_AGENDA', agendaStart: null, agendaLength: 0 }
    },
    changeAgendaLength: (agendaLength: number) => <ChangeAgendaLengthAction>{ type: 'CalendarPage__CHANGE_AGENDA_LENGTH', agendaLength: agendaLength },

    //Получение записей на приём
    requestGetAppointments: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        //Отправляем запрос
        ServiceController.getMyRecordingTimeAdmission()
            .then((data: Appointment[]) => {
                //преобразуем в событие (нужно для Calendar)
                const events: Event[] = data.map((el: Appointment) => {
                    return {
                        start: new Date(el.FROM),
                        end: moment(el.FROM).add(el.DURATION, 'm').toDate(),
                        appointment: el
                    } as Event;
                });
                dispatch({ type: 'CalendarPage__LOAD_APPOINTMENTS', myAppointments: events });
            });
    },

    //открытие календаря
    onOpen: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        //запрашиваем новые данные
        App.Store.dispatch(actionCreators.requestGetAppointments());
    },
};


// REDUCER 
export const reducer: Reducer<CalendarPageState> = (state: CalendarPageState, action: KnownAction) => {
    const stateNew: CalendarPageState = {
        view: state ? state.view : 'month',
        date: state ? state.date : new Date(),
        agendaStart: state ? state.agendaStart : null,
        agendaLength: state ? state.agendaLength : 1,
        myAppointments: state ? state.myAppointments : [],
        isOnlyActiveEvents: state ? state.isOnlyActiveEvents : false,
    }

    switch (action.type) {
        case 'CalendarPage__CHANGE_VIEW':
            stateNew.view = action.view;
            break;

        case 'CalendarPage__CHANGE_DATE':
            stateNew.date = action.date;
            break;

        case 'CalendarPage__CHANGE_IS_ONLY_ACTIVE_EVENTS':
            stateNew.isOnlyActiveEvents = action.isOnlyActiveEvents;
            break;

        case 'CalendarPage__CHANGE_AGENDA':
            stateNew.agendaStart = action.agendaStart;
            stateNew.agendaLength = action.agendaLength;
            break;

        case 'CalendarPage__CHANGE_AGENDA_LENGTH':
            stateNew.agendaLength = action.agendaLength;
            break;

        case 'CalendarPage__LOAD_APPOINTMENTS':
            stateNew.myAppointments = action.myAppointments;
            break;

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return stateNew;
};
