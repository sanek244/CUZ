﻿import * as React from 'react';
import { connect } from "react-redux";
import * as moment from 'moment';

import BaseComponent from "../../../models/BaseComponent";
import * as CalendarPageStore from '../CalendarPageStore';
import * as CalendarEventPopoverStore from './CalendarEventPopoverStore';
import { StatusAppointmentType } from "../../../models/Appointment";
import { ApplicationState } from "../../../store";
import App from '../../../App';

import './CalendarEventPopover.less';

type Props =
    CalendarEventPopoverStore.CalendarEventPopoverState
    & CalendarEventPopoverStore.CalendarEventPopoverAction
    & {};


/**
 * Информер, всплывающий при клике на событие (как в bootstrap)
 */
class CalendarEventPopover extends BaseComponent<Props> {

    public Name: string = 'CalendarEventPopover';

    div: HTMLElement | null;

    //для закрытия окна при клике вне информера
    onClickOutsideDiv(event: any) {
        //если открыт див и клик был за пределы
        if (!App.Store.getState().calendarEventPopover.isClose &&
            this.div &&
            !this.div.contains(event.target)) {

            //если кликнули по элементу,  на котором сейчас находится форма
            if (this.props.divEventRef && this.props.divEventRef.contains(event.target)) {
                return;
            }

            App.Store.dispatch(CalendarEventPopoverStore.actionCreators.close());
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutsideDiv.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutsideDiv.bind(this));
    }

    render() {
        if (this.props.isClose || !this.props.event || !this.props.locationX || !this.props.locationY) {
            return null;
        }

        const appointment = this.props.event.appointment;
        const doctor = appointment.DOCTOR;
        const clinic = appointment.CLINIC;

        //позиция - изначально слева показывать
        let isLeft = true;
        let left = this.props.locationX - 320;

        //если не влезает, то справа
        if (left < 0 && this.props.divEventRef) {
            left = this.props.locationX + this.props.divEventRef.clientWidth + 25;
            isLeft = false;
        }

        return <div
            className={this.getClassName()}
            style={{ top: this.props.locationY - 80, left: left }}
            ref={el => this.div = el}
        >
            <div className={this.getClassName('arrow', !isLeft ? 'left' : 'right')}></div>
            <div className={this.getClassName('header', StatusAppointmentType[appointment.STATUS])}>{doctor.DOCTOR_F + ' ' + doctor.DOCTOR_I + ' ' + doctor.DOCTOR_O}</div>
            <div className={this.getClassName('panel-info')}>
                <div className={this.getClassName('panel-info-line')}>
                    <strong>Начало: </strong>
                    <span>{moment(appointment.FROM).format('HH:mm')}</span>
                </div>
                <div className={this.getClassName('panel-info-line')}>
                    <strong>Специализация: </strong>
                    <span>{doctor.DOCTOR_SPECIALITY.NAME}</span>
                </div>
                <div className={this.getClassName('panel-info-line')}>
                    <strong>Место: </strong>
                    <span>{clinic.NAME}</span>
                </div>
                <div className={this.getClassName('panel-info-line')}>
                    <strong>Напоминание: </strong>
                    <span>{'1 день'}</span>
                </div>
            </div>
            <div className={this.getClassName('panel-buttons')}>

                <button
                    className={'btn ' + this.getClassName('button', 'delete')}
                    onClick={this.props.requestCancelEvent}
                >
                    ОТМЕНИТЬ<br/> ЗАПИСЬ
                </button>

                <button
                    className={'btn ' + this.getClassName('button', 'print')}
                    onClick={this.props.print}
                >
                    <i className="fa fa-print"></i>
                </button>
            </div>
        </div>
    }
}


// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.calendarEventPopover,
    CalendarEventPopoverStore.actionCreators
)(CalendarEventPopover) as typeof CalendarEventPopover;
