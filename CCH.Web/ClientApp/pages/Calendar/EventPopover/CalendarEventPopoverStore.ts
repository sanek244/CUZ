﻿import { Action, Reducer } from 'redux';

import * as CalendarPageStore from '../CalendarPageStore';
import { AppThunkAction } from "../../../store";
import ServiceController from "../../../controllers/ServiceController";
import * as AlertStore from "../../../components/Alert/AlertStore";
import App from "../../../App";
import * as PrintTalonStore from "../../../components/PrintTalon/PrintTalonStore";
import { StatusAppointmentType, StatusAppointmentEnum } from "../../../models/Appointment";

// STATE
export interface CalendarEventPopoverState {
    isClose?: boolean;
    event?: CalendarPageStore.Event,
    locationX?: number,
    locationY?: number,
    divEventRef?: HTMLElement
}

// ACTIONS
interface CloseAction { type: 'CalendarEventPopover__CLOSE' }
interface ToggleAction { type: 'CalendarEventPopover__TOGGLE', isClose: boolean, event: CalendarPageStore.Event, locationX: number, locationY: number, divEventRef: HTMLElement }

type KnownAction = CloseAction | ToggleAction;

// ACTION CREATORS
//for TypeScript
export interface CalendarEventPopoverAction {
    close?: () => CloseAction;
    requestCancelEvent?: () => CloseAction;
    toggle?: () => ToggleAction;
    print?: () => CloseAction;
}
export const actionCreators = {
    close: () => <CloseAction>{ type: 'CalendarEventPopover__CLOSE' },

    toggle: (event: CalendarPageStore.Event, locationX: number, locationY: number, divEventRef: HTMLElement): AppThunkAction<KnownAction> => (dispatch, getState) => {

        //игнорируем все состояния, кроме 'Активна' и 'Ждёт подтверждения' 
        if (event.appointment.STATUS !== StatusAppointmentEnum.ACTIVE &&
            event.appointment.STATUS !== StatusAppointmentEnum.REQUIRES_CONFIRMATION) {
            return;
        }

        //переключать?
        const state = getState().calendarEventPopover;
        let isClose: boolean = false;
        if (state.locationX === locationX && state.locationY === locationY && !state.isClose) {
            isClose = true;
        }

        dispatch({ type: 'CalendarEventPopover__TOGGLE', isClose: isClose, event: event, locationX: locationX, locationY: locationY, divEventRef: divEventRef });
    },

    print: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const event = getState().calendarEventPopover.event;
        if (!event) {
            return;
        }

        dispatch({ type: 'CalendarEventPopover__CLOSE' });
        App.Store.dispatch(PrintTalonStore.actionCreators.print(event.appointment))
    },

    requestCancelEvent: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const event = getState().calendarEventPopover.event;
        if (!event) {
            return;
        }

        //Отправляем запрос
        ServiceController
            .sendCancelAppointment(event.appointment.ID)
            .then(() => {
                //перезапрашиваем данные
                App.Store.dispatch(CalendarPageStore.actionCreators.requestGetAppointments());
                dispatch({ type: 'CalendarEventPopover__CLOSE' });
            });
    }
       
};

// REDUCER 
export const reducer: Reducer<CalendarEventPopoverState> = (state: CalendarEventPopoverState, action: KnownAction) => {
    switch (action.type) {
        case 'CalendarEventPopover__CLOSE':
            return { isClose: true };

        case 'CalendarEventPopover__TOGGLE':
            return {
                isClose: action.isClose || false,
                event: action.event,
                locationX: action.locationX,
                locationY: action.locationY,
                divEventRef: action.divEventRef
            };
        default:
            const exhaustiveCheck: never = action;
    }

    return state || {
        isClose: true
    };
};
