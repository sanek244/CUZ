﻿import * as React from 'react';
import * as moment from 'moment';

import BaseComponent from "../../../models/BaseComponent";
import CalendarPage from "../CalendarPage";
import * as CalendarPageStore from '../CalendarPageStore';
import App from '../../../App';
import Helper from "../../../Helper";
import * as CalendarEventPopoverStore from '../EventPopover/CalendarEventPopoverStore';
import { StatusAppointmentType, StatusAppointmentEnum } from "../../../models/Appointment";
import * as $ from 'jquery';

import './CalendarListEvents.less';

let navigate = {
    PREVIOUS: 'PREV',
    NEXT: 'NEXT',
    TODAY: 'TODAY',
    DATE: 'DATE',
}

//import { navigate } from './utils/constants'
interface Props {
    date: Date,
    events: CalendarPageStore.Event[]
}

moment().locale('ru');

/**
 * Отображение событий списком на ТФ дня, недели и дипазона
 * Код взят из https://github.com/intljusticemission/react-big-calendar/blob/master/src/Week.js
 */
export default class CalendarListEvents extends BaseComponent<Props> {

    public Name: string = 'CalendarListEvents';

    divsEvent: any = {}; //object {idAppointment: HTMLElement}


    render() {
        //фильтруем по выбранному диапазону дат
        const range = CalendarListEvents.range(this.props.date);
        let events = this.props.events.filter(event => event.start >= range.start && event.end <= range.end);

        //пустой лист, если нету событий
        if (events.length === 0) {
            return <div className={this.getClassName()}>
                <div className={this.getClassName('empty')}>Нет событий для отображения</div>
            </div>
        }

        //Сортируем по дате 
        events = Helper.sortByField(events, (x: CalendarPageStore.Event) => x.start);

        //Добавляем в категории
        const eventsOfDays: any = {};
        const titleFull: any = {};

        events.map(event => {
            const key = CalendarListEvents.getBlockTitleLeft(event.start);
            if (!(key in eventsOfDays)) {
                eventsOfDays[key] = [];
                titleFull[key] = CalendarListEvents.getBlockTitleRight(event.start);
            }
            eventsOfDays[key].push(event);
        });

        return <div className={this.getClassName()}>
            {Object.keys(eventsOfDays).map(key => {
                return <div>
                    <div className={this.getClassName('line-header')}>
                        <strong
                            className={this.getClassName('line-header-date', 'left')}
                            onClick={() => this.onClickDate(eventsOfDays[key][0].start)}
                        >
                            {key}
                        </strong>
                        <strong
                            className={this.getClassName('line-header-date', 'right')}
                            onClick={() => this.onClickDate(eventsOfDays[key][0].start)}
                        >
                            {titleFull[key]}
                        </strong>
                    </div>

                    {eventsOfDays[key].map((event: CalendarPageStore.Event) => (
                        <div
                            className={this.getClassName('line', StatusAppointmentType[event.appointment.STATUS])}
                            ref={div => this.divsEvent[event.appointment.ID] = div}
                            onClick={() => {
                                const appoitmentStatus = event.appointment.STATUS;
                                const div = this.divsEvent[event.appointment.ID];

                                if (!div) {
                                    return;
                                }

                                App.Store.dispatch(CalendarEventPopoverStore.actionCreators.toggle(
                                    event,
                                    $(div).offset().left,
                                    $(div).offset().top - 12,
                                    div
                                ));
                            }}
                        >
                            <div className={this.getClassName('line-box')}>
                                <span className={this.getClassName('line-time')}>{moment(event.start).format('HH:mm')}</span>
                                <span className={this.getClassName('line-marker')}>
                                    <span className={this.getClassName('line-marker-circle')}></span>
                                </span>
                                <span className={this.getClassName('line-title')}>{event.appointment.TITLE}</span>
                            </div>
                        </div>
                    ))}
                </div>
            })}
        </div>
    }

    /**
     * Клик по дате
     * @param date
     */
    public onClickDate(date: Date) {
        //смена view и date яерез изменение url адреса
        CalendarPage.changeUrl('day', date);
    }

    /**
     * Отображаемый заголовок в левой части события
     * @param date
     */
    public static getBlockTitleLeft(date: Date) {
        const view = App.Store.getState().calendarPage.view;

        switch (view) {
            case 'day':
            case 'week':
                return moment(date).format('dddd');

            case 'agenda':
                return moment(date).format('DD MMMM YYYY г.');

        }

        return '';
    }

    /**
     * Отображаемый заголовок в правой части события
     * @param date
     */
    public static getBlockTitleRight(date: Date) {
        const view = App.Store.getState().calendarPage.view;

        switch (view) {
            case 'week':
                return moment(date).format('DD MMMM YYYY г.');
        }

        return '';
    }


    /**
     * Навигация по стрелочкам (вперёд/назад)
     * @param date
     * @param action
     */
    public static navigate(date: Date, action: string) {
        const view = App.Store.getState().calendarPage.view;

        if (view === 'agenda') {
            return date;
        }

        switch (action) {
            case navigate.PREVIOUS:
                return moment(date).add(-1, view).toDate()

            case navigate.NEXT:
                return moment(date).add(1, view).toDate()

            default:
                return date
        }
    }

    /**
     * Диапазон дат
     * @param date
     * @param param1
     */
    public static range(date: Date) {
        const view = App.Store.getState().calendarPage.view;

        switch (view) {
            case 'week':
                return {
                    start: moment(date).startOf('week').toDate(),
                    end: moment(date).endOf('week').toDate(),
                };

            case 'agenda':
                return {
                    start: moment(date).startOf('day').toDate(),
                    end: moment(date).endOf('day').add(App.Store.getState().calendarPage.agendaLength, 'days').toDate(),
                };
        }

        return {
            start: moment(date).startOf('day').toDate(),
            end: moment(date).endOf('day').toDate(),
        };
    }

    /**
     * Показывваемые даты
     * @param date
     * @param param1
     */
    public static title(date: Date, { formats, culture }: any) {
        const view = App.Store.getState().calendarPage.view;

        switch (view) {
            case 'week':
                return formats.dayRangeHeaderFormat(this.range(date), culture, null);

            case 'agenda':
                return formats.agendaHeaderFormat(this.range(date), culture, null);

            case 'day':
                return moment(date).format(formats.dayHeaderFormat);
        }

        return '';
    }
}


