﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { connect } from 'react-redux';
import BigCalendar from 'react-big-calendar';
import * as moment from 'moment';

import App from "../../App";
import BaseComponent from "../../models/BaseComponent";
import { ApplicationState } from '../../store';
import * as CalendarPageStore from './CalendarPageStore';
import CalendarToolBar from "./ToolBar/CalendarToolBar";
import CalendarEventMonth from "./EventMonth/CalendarEventMonth";
import CalendarListEvents from "./ListEvents/CalendarListEvents";
import CalendarEventPopover from "./EventPopover/CalendarEventPopover";
import { StatusAppointmentEnum } from '../../models/Appointment';

import 'react-big-calendar/lib/less/styles.less';
import './CalendarPage.less';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
moment.locale('ru');
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer


type Props =
    CalendarPageStore.CalendarPageState
    & typeof CalendarPageStore.actionCreators
    & RouteComponentProps<{}>;

/**
 * Календарь с записанными приёмами
 */
class CalendarPage extends BaseComponent<Props> {

    public Name: string = 'CalendarPage';

    //предыдущий url (для контроля изменений)
    oldUrl: string = '-';

    public componentDidMount() {
        this.props.onOpen();
        this.changePropsFromUrl();
    }

    public componentDidUpdate() {
        this.changePropsFromUrl();
    }

    //меняем props(reduxe state) параметры по url
    protected changePropsFromUrl() {
        const url = App.History.location.pathname;

        //url изменился
        if (this.oldUrl !== url) {
            this.oldUrl = url;
            const urlParams = App.History.location.pathname.split('/').filter(x => x)

            if (urlParams.length > 0) {
                //для TypeScript
                let view: 'week' | 'day' | 'month' | 'agenda' = 'month';
                switch (urlParams[1]) {
                    case 'week': view = 'week'; break;
                    case 'day': view = 'day'; break;
                    case 'month': view = 'month'; break;
                    case 'agenda': view = 'agenda'; break;
                }

                //меняем представление
                this.props.changeView(view);

                if (urlParams.length > 1) {
                    //меняем дату
                    this.props.changeDate(moment(urlParams[2], 'DD-MM-YYYY').toDate());

                    if (urlParams.length > 2) {
                        //меняем параметр показа активных записей
                        this.props.changeIsOnlyActiveEvents(urlParams[3] === '1');

                        //меняем длину периода
                        if (urlParams.length > 3) 
                            this.props.changeAgendaLength(parseInt(urlParams[4]));
                    }
                }
            }
        }
    }

    public render() {

        return <div className={this.getClassName()}>
            <BreadcrumbsItem to='/calendar'>Календарь клиента</BreadcrumbsItem>

            <BigCalendar //docs: http://intljusticemission.github.io/react-big-calendar/examples/index.html

                //записи на приём
                events={
                    this.props.isOnlyActiveEvents 
                        ? this.props.myAppointments.filter(x => x.appointment.STATUS === StatusAppointmentEnum.ACTIVE)
                        : this.props.myAppointments
                }

                //для контроля состояний
                view={this.props.view}

                //для контроля состояний
                date={this.props.date}

                //Показываем с сегодняшнего дня
                defaultDate={new Date()}

                //для диапазона (сколько дней выделено)
                length={this.props.agendaLength}

                //выделение ячеек в месяце
                selectable={this.props.view === 'month'}

                //Руссификация слов
                messages={{
                    allDay: 'Все дни',
                    today: 'Сегодня',
                    month: 'Месяц',
                    week: 'Неделя',
                    day: 'День',
                    agenda: 'За период',
                    date: 'Дата',
                    time: 'Время',
                    event: 'Записи'
                }}

                //Переделываем отображение по времени
                views={{
                    'month': true, //стандартно
                    'week': CalendarListEvents, 
                    'day': CalendarListEvents,
                    'agenda': CalendarListEvents
                }}

                //Переделываем элементы календаря
                components={{
                    toolbar: CalendarToolBar, //шапка (панель управления календарём)
                    month: { //Просмотр месяца
                        event: CalendarEventMonth //Отображение записи на приём 
                    },
                }}

                //встраивание класса и стиля ячейкам
                dayPropGetter={(date: Date) => {
                    //только для месяца
                    if (this.props.agendaStart && this.props.view === 'month') {
                        //Показывать выделенные ячейки (по умолчанию выделение пропадает при отпускании мышки)
                        const agendaEnd = moment(this.props.agendaStart).add(this.props.agendaLength, 'd').toDate();
                        if (date >= this.props.agendaStart && date <= agendaEnd) {1
                            return {
                                className: 'rbc-selected-cell'
                            };
                        }
                    }
                }}

                //изменяем формат показа дат
                formats={{
                    dateFormat: 'D',
                    weekdayFormat: 'dddd',
                    dayFormat: 'DD dddd',
                    dayHeaderFormat: 'DD MMMM YYYY',
                    dayRangeHeaderFormat: (date: { start: Date, end: Date }, culture: any, local: any) =>
                        moment(date.start).format('D') + ' — ' + moment(date.end).format(' D MMMM YYYY г.'),
                    agendaHeaderFormat: (date: { start: Date, end: Date }, culture: any, local: any) =>
                        moment(date.start).format('D') + ' — ' + moment(date.end).format(' D MMMM YYYY г.')
                }}

                //Выделение ячеек
                onSelectSlot={(slotInfo: { start: Date, end: Date, slots: Array<Date>, action: "select" | "click" | "doubleClick" }) => {
                    //только для месяца
                    if (this.props.view === 'month') {

                        //Если клик, то открываем день
                        if (slotInfo.action === 'click') {
                            this.changeUrl('day', slotInfo.end);
                            return;
                        }

                        //сохраняем выделенный диапазон
                        this.props.changeAgenda(slotInfo.start, slotInfo.end);
                    }
                }}

                //изменение показываемого вида (месяц, день, неделя, дапазон)
                onView={(view: 'week' | 'day' | 'month' | 'agenda') => {
                    if (view === 'agenda' && this.props.agendaStart !== null) {
                        this.changeUrl(view, this.props.agendaStart);
                        return;
                    }

                    this.changeUrl(view);
                }}

                //навигация через (предыдущий, следующий, сегодня)
                onNavigate={(date: Date, view: 'week' | 'day' | 'month' | 'agenda', action: 'NEXT' | 'PREV' | 'TODAY') => {
                    this.changeUrl(view, date);
                }}
            />

            <CalendarEventPopover />
        </div>;
    }

    /**
     * Изменить url адрес по параметрам отображения календаря, даты и длины периода
     * @param view
     * @param date
     */
    public changeUrl(view: 'week' | 'day' | 'month' | 'agenda', date?: Date) {
        CalendarPage.changeUrl(view, date || this.props.date, this.props.isOnlyActiveEvents, this.props.agendaLength);
    }

    /**
     * Изменить url адрес по параметрам отображения календаря, даты и длины периода
     * @param view
     * @param date
     * @param agendaLength
     */
    public static changeUrl(view: 'week' | 'day' | 'month' | 'agenda', date: Date, isOnlyActiveEvents: boolean = false, agendaLength: number = 0) {
        let urlParams = [
            'calendar',
            view, moment(date).format('DD-MM-YYYY'),
            isOnlyActiveEvents || agendaLength ? (isOnlyActiveEvents ? '1' : '0') : null,
            agendaLength ? agendaLength : null
        ];

        App.History.push('/' + urlParams.filter(x => x != null).join('/'));
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: ApplicationState) => state.calendarPage,
    CalendarPageStore.actionCreators
)(CalendarPage) as typeof CalendarPage;
