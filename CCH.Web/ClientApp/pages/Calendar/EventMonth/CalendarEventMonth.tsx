﻿import * as React from 'react';
import * as moment from 'moment';
import * as $ from 'jquery';

import BaseComponent from "../../../models/BaseComponent";
import * as CalendarPageStore from '../CalendarPageStore';
import * as CalendarEventPopoverStore from '../EventPopover/CalendarEventPopoverStore';
import { StatusAppointmentType, StatusAppointmentEnum } from "../../../models/Appointment";
import App from '../../../App';

import './CalendarEventMonth.less';

interface Props {
    event: CalendarPageStore.Event
}

/**
 * Отображение события в календаре на месячном шаблоне
 */
export default class CalendarEventMonth extends BaseComponent<Props> {

    public Name: string = 'CalendarEventMonth';

    divEvent: HTMLElement | null = null;

    render() {
        return <div
            className={this.getClassName() + ' ' + this.getClassName('body', StatusAppointmentType[this.props.event.appointment.STATUS])}
            data-container="body"
            data-toggle="popover"
            data-placement="left"
            data-html='true'
            data-content={"<div class='" + this.getClassName() + "'></div>"}
            ref={div => this.divEvent = div}
            onClick={() => {
                const appoitmentStatus = this.props.event.appointment.STATUS;

                if (!this.divEvent) {
                    return;
                }

                App.Store.dispatch(CalendarEventPopoverStore.actionCreators.toggle(
                    this.props.event,
                    $(this.divEvent).offset().left,
                    $(this.divEvent).offset().top,
                    this.divEvent
                ));
            }}
        >
            <strong>{moment(this.props.event.start).format('HH:mm')}</strong>
            <span> {this.props.event.appointment.TITLE}</span>
        </div>
    }
}
