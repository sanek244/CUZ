﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import Select from 'react-select';

import BaseComponent from "../../models/BaseComponent";
import Doctor from "../../models/Doctor";
import { ApplicationState } from '../../store';
import SearchDoctorsForm from '../../components/SearchDoctorsForm/SearchDoctorsForm';
import * as SearchDoctorsFormStore from '../../components/SearchDoctorsForm/SearchDoctorsFormStore';
import * as RecordingAdmissionStore from '../../components/RecordingAdmission/RecordingAdmissionStore';
import RecordingAdmission from '../../components/RecordingAdmission/RecordingAdmission';
import DoctorSpecialty from "../../models/DoctorSpecialty";
import SearchDoctrosForm from "../../models/forms/SearchDoctrosForm";
import SearchDoctrosUrlParams from "../../models/SearchDoctrosUrlParams";
import Helper from "../../Helper";
import App from "../../App";

import './ClinicDoctorsSearchPage.less';
import 'react-select/less/default.less';

interface Prop {
    idClinic: string,
}

/**
 * Поиск врачей по клиникам
 */
export default class ClinicDoctorsSearchPage extends BaseComponent<RouteComponentProps<Prop>> {

    public Name: string = 'ClinicDoctorsSearchPage';

    public componentWillMount() {
        App.Store.subscribe(() => this.forceUpdate());
    }

    get stateSearch() {
        return App.Store.getState().searchDoctorsForm;
    }

    public render() {
        const clinicId = this.stateSearch.clinic ? this.stateSearch.clinic.ID : 'all';
        const clinic = this.stateSearch.clinic;

        return <div className={this.getClassName()}>
            <BreadcrumbsItem to='/clinics'>Справочник врачей и клиник</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/' + (clinicId === 'all' ? '' : clinicId)}>{clinicId === 'all' || clinic == null ? 'Все клиники' : clinic.FULL_NAME}</BreadcrumbsItem>
            <BreadcrumbsItem to={'/clinics/' + clinicId + '/doctors'}>Врачи клиники</BreadcrumbsItem>

            <div>
                <h1 className={this.getClassName('header')}>ВРАЧИ</h1>
                <SearchDoctorsForm
                    isIdClinicInUrl={true}
                    isShowDateFilter={true}
                    defaultUrl='/clinics/all/doctors'
                    urlWithClinicId='/clinics/{0}/doctors'
                    {...this.stateSearch}
                    {...SearchDoctorsFormStore.actionCreators} />
                {this.renderSearchResults()}
            </div>

            <RecordingAdmission {...App.Store.getState().recordingAdmission} {...RecordingAdmissionStore.actionCreators} />
        </div>;
    }

    /**
     * Отрисовка результатов поиска
     * @param doctor
     */
    protected renderSearchResults() {
        return <div className={this.getClassName('list-results')}>
            {this.stateSearch.resultsSearch.length
                ? this.stateSearch.resultsSearch.map(result => this.renderItemResultSearch(result))
                : <div className={this.getClassName('list-results-none')}>Ничего не найдено! </div>
            }
        </div>
    }

    /**
     * Отрисовка отдельного элемента результата поиска
     * @param doctor
     */
    protected renderItemResultSearch(doctor: Doctor) {
        const doctorSpecialty = this.stateSearch.doctorSpecialities.find(x => x.ID === doctor.ID_DOCTOR_SPECIALITY) || new DoctorSpecialty();

        return <div className={this.getClassName('list-results-item')}>
            <Link
                to={'/clinics/all/doctors/' + doctor.ID}
                className={this.getClassName('list-results-item-name')}
            >
                {doctor.DOCTOR_F + ' ' + doctor.DOCTOR_I + ' ' + doctor.DOCTOR_O}
            </Link>
            <div className={this.getClassName('list-results-item-role')}>{doctorSpecialty ? doctorSpecialty.NAME : ''}</div>
            <div className={this.getClassName('list-results-item-info')}>[TODO: составлять описание, пример: 'Заведующий отделением онкологии, врач высшей квалификационной категории, кандидат медицинских наук']</div> 
            <div>
                <button
                    className={"btn text-white mt-auto " + this.getClassName('list-results-item-button-entry')}
                    onClick={() => this.openModal(doctor, doctorSpecialty)}
                >
                    <span className={this.getClassName('list-results-item-button-entry-text')}>ЗАПИСЬ НА ПРИЕМ</span>
                </button>
            </div>
        </div>
    }

    openModal(doctor: Doctor, doctorSpecialty: DoctorSpecialty) {
        let dateTimeFrom: Date | null = null;
        let dateTimeTo: Date | null = null;
        if (this.stateSearch.dateFrom &&
            this.stateSearch.dateTo &&
            this.stateSearch.timeFrom &&
            this.stateSearch.timeTo &&
            this.stateSearch.dateFrom <= this.stateSearch.dateTo &&
            this.stateSearch.timeFrom < this.stateSearch.timeTo)
        {
            dateTimeFrom = this.stateSearch.dateFrom;
            dateTimeFrom.setHours(parseInt(this.stateSearch.timeFrom.substring(0, 2)));
            dateTimeFrom.setMinutes(parseInt(this.stateSearch.timeFrom.substring(3, 5)));

            dateTimeTo = this.stateSearch.dateTo;
            dateTimeTo.setHours(parseInt(this.stateSearch.timeTo.substring(0, 2)));
            dateTimeTo.setMinutes(parseInt(this.stateSearch.timeTo.substring(3, 5)));
        }
        
        App.Store.dispatch(RecordingAdmissionStore.actionCreators.open(doctor, this.stateSearch.clinic, doctorSpecialty, dateTimeFrom, dateTimeTo));
    }
}