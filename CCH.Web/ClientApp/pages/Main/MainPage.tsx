﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

import BaseComponent from "../../models/BaseComponent";

import './MainPage.less';

export default class MainPage extends BaseComponent<RouteComponentProps<{}>> {

    public Name: string = 'MainPage';


    public render() {
        const path = '../media/mainPage/';

        return <div className={this.getClassName()}>
            <ul className={'nav justify-content-center ' + this.getClassName('line', 'top')}>
                {this.renderElement('ВЫЗОВ ВРАЧА  НА ДОМ', path + 'doctor1.png', '#')}
                {this.renderElement('ВЫЗОВ  СМП', path + 'signal.png', '#')}
                {this.renderElement('ЗАПИСЬ НА ПРИЁМ', path + 'table.png', '/clinics/all/doctors')}
                {this.renderElement('СПРАВОЧНИК ВРАЧЕЙ И КЛИНИК', path + 'books.png', '/clinics')}
            </ul>
            <ul className={'nav justify-content-center ' + this.getClassName('line', 'middle')}>
                {this.renderElement('ЗАПИСЬ РАЗГОВОРОВ', path + 'call.png', '#')}
                {this.renderElement('ТЕЛЕКОНСУЛЬ-ТАЦИЯ', path + 'doctor-listened.png', '#')}
                {this.renderElement('ВТОРОЕ МНЕНИЕ', path + 'doctor-advice.png', '#')}
                {this.renderElement('СОВОКУПНЫЕ РЕКОМЕНДАЦИИ', path + 'doctors.png', '#')}
                {this.renderElement('ЭМК (МЕД. КАРТА)', path + 'card.png', '#')}

            </ul>
            <ul className={'nav justify-content-center ' + this.getClassName('line', 'bottom')}>
                {this.renderElement('ЛИЧНАЯ АПТЕЧКА', path + 'first-aid kit.png', '#')}
                {this.renderElement('ПРИЁМ ПРЕПАРАТОВ', path + 'pills.png', '#')}
                {this.renderElement('ДНЕВНИК ЗДОРОВЬЯ', path + 'book.png', '#')}
                {this.renderElement('ПОКАЗАНИЯ УСТРОЙСТВ', path + 'readings.png', '#')}

            </ul>
        </div>;
    }

    public renderElement(text: string, imgUrl: string, link: string) {
        return <li className={'nav-item ' + this.getClassName('block-element')}>
            <Link to={link} className={'nav-link ' + this.getClassName('block-element-link')}>
                <img src={imgUrl} alt={text} className={this.getClassName('block-element-img')} />
            </Link>
            <span className={this.getClassName('block-element-text')}>{text}</span>
		</li>
    }
}
