﻿import { Action, Reducer } from 'redux';

import { AppThunkAction } from "../../store";
import * as AlertStore from "../../components/Alert/AlertStore";
import Alert from "../../components/Alert/Alert";
import * as AuthorizationPageStore from "../Authorization/AuthorizationPageStore";
import ServiceController from "../../controllers/ServiceController";
import App from "../../App";
import RegistrationForm from "../../models/forms/RegistrationForm";
import RegistrationFormError from "../../models/forms/errors/RegistrationFormError";

// STATE
export interface RegistrationPageState {
    errors: RegistrationFormError,
    commonError: string
}

// ACTIONS
interface GetErrorsAction { type: 'RegistrationPage__GET_ERRORS', errors: RegistrationFormError }
interface GetErrorsCommonAction { type: 'RegistrationPage__GET_ERRORS_COMMON', commonError: string }
interface BagAction { type: 'BAG' } //TypeScript bug - show error on the last Action if it has variables other than field 'type'

type KnownAction = GetErrorsAction | GetErrorsCommonAction | BagAction;

// ACTION CREATORS
export const actionCreators = {
    requestRegistration: (form: RegistrationForm): AppThunkAction<KnownAction> => (dispatch, getState) => {

        const errors = new RegistrationFormError();
        let isHasError = false;

        if (!form.Email) {
            errors.Email = 'Поле не заполнено!';
            isHasError = true;
        }

        if (form.Email.indexOf('@') === -1 || form.Email.indexOf('.') === -1) {
            errors.Email = 'Email адрес не является действительным!';
            isHasError = true;
        }

        if (!form.Name) {
            errors.Name = 'Поле не заполнено!';
            isHasError = true;
        }

        if (!form.Password) {
            errors.Password = 'Поле не заполнено!';
            isHasError = true;
        }

        if (!form.PasswordAgain) {
            errors.PasswordAgain = 'Поле не заполнено!';
            isHasError = true;
        }

        if (form.PasswordAgain !== form.Password) {
            errors.PasswordAgain = 'Пароли не совпадают!';
            isHasError = true;
        }

        if (isHasError) {
            dispatch({ type: 'RegistrationPage__GET_ERRORS', errors: errors });
            return;
        }

        //TODO
        RegistrationPage__GET_ERRORS_COMMON(dispatch, 'Регистрация временно не работает!');
        return;

        //send request
        /*ServiceController.sendRegistration(form)
            .then(res => {
                App.History.push('/login');
                Alert.Open('Вы успешно зарегистрированы!', AlertStore.HeaderState.success);

                //update state Authorization Page 
                App.Store.dispatch(AuthorizationPageStore.actionCreators.init());
            })
            .catch(err => {
                dispatch({ type: 'RegistrationPage__GET_ERRORS', errors: err as RegistrationFormError });
            });*/
    },
};

function RegistrationPage__GET_ERRORS_COMMON(dispatch: (action: KnownAction) => void, errorMessage: string) {
    Alert.Open(errorMessage);
    dispatch({ type: 'RegistrationPage__GET_ERRORS_COMMON', commonError: errorMessage });
}

// REDUCER 
export const reducer: Reducer<RegistrationPageState> = (state: RegistrationPageState, action: KnownAction) => {
    switch (action.type) {
        case 'RegistrationPage__GET_ERRORS':
            return { errors: action.errors, commonError: '' };

        case 'RegistrationPage__GET_ERRORS_COMMON':
            return {
                errors: new RegistrationFormError(),
                commonError: action.commonError
            };

        case 'BAG':
            break;

        default:
            const exhaustiveCheck: never = action;
    }

    return state || {
        errors: new RegistrationFormError(),
        commonError: ''
    };
};
