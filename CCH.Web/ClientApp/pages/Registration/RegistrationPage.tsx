﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

import BaseComponent from '../../models/BaseComponent';
import RegistrationForm from "../../models/forms/RegistrationForm";
import { ApplicationState } from '../../store';
import * as RegistrationPageStore from './RegistrationPageStore';

import './RegistrationPage.less';


type Props =
    RegistrationPageStore.RegistrationPageState
    & typeof RegistrationPageStore.actionCreators
    & RouteComponentProps<{}>; 


class RegistrationPage extends BaseComponent<Props> {

    public Name: string = 'RegistrationPage';


    inputEmail: HTMLInputElement | null = null;
    inputName: HTMLInputElement | null = null;
    inputPassword: HTMLInputElement | null = null;
    inputPasswordAgain: HTMLInputElement | null = null;

    public render() {
        return <div className={this.getClassName() + ' ' + (this.props.commonError ? 'has-error' : '')}>

            <div className={"text-white d-flex " + this.getClassName('header')}>
                <strong className="m-auto">Добро пожаловать в <br /> центр управления здоровьем!</strong>
            </div>
            <div className={"d-flex " + this.getClassName('body-container')}>
                <div className={"bg-white " + this.getClassName('body')}>
                    <div className={this.getClassName('body-header')}>Для входа в систему <br /> мы просим вас зарегистрироваться</div>
                    <div className="text-danger">{this.props.commonError}</div>
                    <div>
                        <div className={"form-group " + (this.props.errors.Name ? 'has-error' : '')}>
                            <input
                                type="text"
                                placeholder="ФИО"
                                className={this.getClassName('body-input')}
                                ref={input => this.inputName = input}
                            />
                            <small className="text-danger">{this.props.errors.Name}</small>
                        </div>
                        <div className={"form-group " + (this.props.errors.Email ? 'has-error' : '')}>
                            <input
                                type="text"
                                placeholder="Email"
                                className={this.getClassName('body-input')}
                                ref={input => this.inputEmail = input}
                            />
                            <small className="text-danger">{this.props.errors.Email}</small>
                        </div>
                        <div className={"form-group " + (this.props.errors.Password ? 'has-error' : '')}>
                            <input
                                type="password"
                                placeholder="пароль"
                                className={this.getClassName('body-input')}
                                ref={input => this.inputPassword = input}
                            />
                            <small className="text-danger">{this.props.errors.Password}</small>
                        </div>
                        <div className={"form-group " + (this.props.errors.PasswordAgain ? 'has-error' : '')}>
                            <input
                                type="password"
                                placeholder="повторите пароль"
                                className={this.getClassName('body-input')}
                                ref={input => this.inputPasswordAgain = input}
                            />
                            <small className="text-danger">{this.props.errors.PasswordAgain}</small>
                        </div>
                        <div className="form-group">
                            <button
                                className={"btn text-white " + this.getClassName('body-button')}
                                onClick={() => this.sendRegistration()}
                            >
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                    <div className={this.getClassName('body-login')}>Если вы уже зарегистрированы, <br/> нажмите <Link to="/login">сюда</Link></div>
                </div>
            </div>
        </div>
    }

    /**
     * Sending a registration request
     */
    public sendRegistration() {

        //data from inputs
        const form = new RegistrationForm();
        form.Email = this.inputEmail ? this.inputEmail.value : '';
        form.Name = this.inputName ? this.inputName.value : '';
        form.Password = this.inputPassword ? this.inputPassword.value : '';
        form.PasswordAgain = this.inputPasswordAgain ? this.inputPasswordAgain.value : '';

        //send request
        this.props.requestRegistration(form);
    }
}


export default connect(
    (state: ApplicationState) => state.registrationPage,
    RegistrationPageStore.actionCreators
)(RegistrationPage) as typeof RegistrationPage;