﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';

import BaseComponent from '../../models/BaseComponent';

import './NotFoundPage.less';


export default class NotFoundPage extends BaseComponent<RouteComponentProps<{}>> {

    public Name: string = 'NotFoundPage';


    public render() {
        return <div className={this.getClassName()}>
            <h1 className={this.getClassName('header')}>404</h1>
            <p className={this.getClassName('text')}>Страница не найдена!</p>
            <Link className={this.getClassName('link')} to="/">Вернуться на главную</Link>
        </div>
    }
}