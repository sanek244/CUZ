﻿import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

import BaseComponent from '../../models/BaseComponent';
import User from "../../models/User";
import AuthorizationForm from "../../models/forms/AuthorizationForm";
import ServiceController from "../../controllers/ServiceController";
import App from "../../App";
import * as AuthorizationPageStore from './AuthorizationPageStore';
import { ApplicationState } from '../../store';

import './AuthorizationPage.less';

type Props =
    AuthorizationPageStore.AuthorizationPageState
    & typeof AuthorizationPageStore.actionCreators
    & RouteComponentProps<{}>; 

class AuthorizationPage extends BaseComponent<Props> {

    public Name: string = 'AuthorizationPage';


    loginInput: HTMLInputElement | null = null;
    passwordInput: HTMLInputElement | null = null;
    
    public render() {
        return <div
            className={"text-center " + this.getClassName() + ' ' + (this.props.commonError ? 'has-error' : '')}
            onKeyDown={(e: any) => { if (e.keyCode === 13) this.requestAuthorization() }}
        >
            <div className={"text-white d-flex " + this.getClassName('header')}>
                <strong className="m-auto">Добро пожаловать в <br /> центр управления здоровьем!</strong>
            </div>
            <div className={"d-flex " + this.getClassName('body-container')}>
                <div className={"bg-white " + this.getClassName('body')}>
                    <div className={this.getClassName('body-header')}>Для входа в систему <br /> мы просим вас авторизовааться</div>
                    <div className="text-danger">{this.props.commonError}</div>
                    <div>
                        <div className={"form-group " + (this.props.errors.Login ? 'has-error' : '')}>
                            <input
                                type="text"
                                placeholder="логин"
                                className={this.getClassName('body-input')}
                                ref={input => this.loginInput = input}
                            />
                            <small className="text-danger">{this.props.errors.Login}</small>
                        </div>
                        <div className={"form-group " + (this.props.errors.PasswordHash ? 'has-error' : '')}>
                            <input
                                type="password"
                                placeholder="пароль"
                                className={this.getClassName('body-input')}
                                ref={input => this.passwordInput = input}
                            />
                            <small className="text-danger">{this.props.errors.PasswordHash}</small>
                        </div>
                        <div className="form-group">
                            <button
                                className={"btn text-white " + this.getClassName('body-button')}
                                onClick={() => this.requestAuthorization()}
                            >
                                Войти
                            </button>
                        </div>
                    </div>
                    <div className={this.getClassName('body-registration')}>Если вы впервые у нас, пожалуйста, <Link to="/registration">зарегистрируйтесь</Link></div>
                    <div className={this.getClassName('body-recovery')}>Забыли пароль? Нажмите <Link to="#">сюда</Link></div>
                </div>
            </div>
        </div>;
    }

    private requestAuthorization() {
        const login = this.loginInput ? this.loginInput.value : '';
        const password = this.passwordInput ? this.passwordInput.value : '';

        this.props.requestAuthorization(new AuthorizationForm(login, password))
    }
}

export default connect(
    (state: ApplicationState) => state.authorizationPage,
    AuthorizationPageStore.actionCreators
)(AuthorizationPage) as typeof AuthorizationPage; 