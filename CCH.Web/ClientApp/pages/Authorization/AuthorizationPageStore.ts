﻿import { Action, Reducer } from 'redux';

import { AppThunkAction } from "../../store";
import Alert from "../../components/Alert/Alert";
import ServiceController from "../../controllers/ServiceController";
import App from "../../App";
import User from "../../models/User";
import AuthorizationForm from "../../models/forms/AuthorizationForm";
import AuthorizationFormError from "../../models/forms/errors/AuthorizationFormError";
import Helper from "../../Helper";

// STATE
export interface AuthorizationPageState {
    errors: AuthorizationFormError,
    commonError: string,
    urlReturn: string
}

// ACTIONS
interface GetErrorsAction { type: 'AuthorizationPage__GET_ERRORS', errors: AuthorizationFormError }
interface GetErrorsCommonAction { type: 'AuthorizationPage__GET_ERRORS_COMMON', commonError: string }
interface ChangeUrlReturnAction { type: 'AuthorizationPage__CHANGE_URL_RETURN', urlReturn: string }
interface InitAction { type: 'INIT' }

type KnownAction = GetErrorsAction | GetErrorsCommonAction | ChangeUrlReturnAction | InitAction;

// ACTION CREATORS
export const actionCreators = {
    requestAuthorization: (form: AuthorizationForm): AppThunkAction<KnownAction> => (dispatch, getState) => {

        if (!form.Login || !form.PasswordHash) {
            AuthorizationPage__GET_ERRORS_COMMON(dispatch, 'Логин или пароль не введены!');
            return;
        }

        form.PasswordHash = Helper.md5(form.PasswordHash);

        //send request
        ServiceController.sendAuthorization(form)
            .then((res : User) => {
                App.user = res;

                //redirect to home
                App.History.push(getState().authorizationPage.urlReturn);
            })
            .catch((keyErrors: object) => {
                dispatch({ type: 'AuthorizationPage__GET_ERRORS', errors: keyErrors as AuthorizationFormError });
            });
    },


    requestLogout: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        App.user = null;

        //send request
        ServiceController.sendLogout();

        dispatch({ type: 'AuthorizationPage__CHANGE_URL_RETURN', urlReturn: '/' });
    },

    changeUrlReturn: (urlReturn: string) => <ChangeUrlReturnAction>{ type: 'AuthorizationPage__CHANGE_URL_RETURN', urlReturn: urlReturn },

    init: () => <InitAction>{ type: 'INIT' },
};

function AuthorizationPage__GET_ERRORS_COMMON(dispatch: (action: KnownAction) => void, errorMessage: string) {
    Alert.Open(errorMessage);
    dispatch({ type: 'AuthorizationPage__GET_ERRORS_COMMON', commonError: errorMessage });
}

// REDUCER 
export const reducer: Reducer<AuthorizationPageState> = (state: AuthorizationPageState, action: KnownAction) => {
    const newState: AuthorizationPageState = {
        errors: state ? state.errors : new AuthorizationFormError(),
        commonError: state ? state.commonError : '',
        urlReturn: state ? state.urlReturn : '/'
    }

    switch (action.type) {
        case 'AuthorizationPage__GET_ERRORS':
            newState.errors = action.errors;
            newState.commonError = '';
            break;

        case 'AuthorizationPage__GET_ERRORS_COMMON':
            newState.commonError = action.commonError;
            newState.errors = new AuthorizationFormError();
            break;

        case 'AuthorizationPage__CHANGE_URL_RETURN':
            newState.urlReturn = action.urlReturn;
            break;

        case 'INIT':
            return {
                errors: new AuthorizationFormError(),
                commonError: '',
                urlReturn: '/'
            };

        default:
            const exhaustiveCheck: never = action;
    }

    return newState;
};
