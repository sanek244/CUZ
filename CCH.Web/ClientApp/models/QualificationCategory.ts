﻿/**
 * Квалификационные категории
 */
export default class QualificationCategory {
    public ID: number; // код
    public NAME: string; // Название

    constructor() {
        this.ID = 0;
        this.NAME = '';
    }
}