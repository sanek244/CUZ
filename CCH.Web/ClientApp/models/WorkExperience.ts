﻿/**
 * Стаж работы (1 экземпляр - 1 место прежней работы)
 */
export default class WorkExperience {
    public ID_DOCTOR: number; //id врача
    public PLACE: string; //Место прежней работы
    public POSITION: string; //Занимаемая должность
    public FROM: Date; //Дата начала работы
    public TO: Date; // Дата окончания работы
    public DURATION: string; //Продолжительность работы
}