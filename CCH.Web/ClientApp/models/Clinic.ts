﻿import BaseModel from "./BaseModel";

/**
 * Клиника
 */
export default class Clinic extends BaseModel {
    public ID: number; // код
    public NAME: string; // краткое наименование
    public FULL_NAME: string; // полное наименование
    public ID_CLINIC_SPECIALIZATION: number; // специализация клиники
    public ADDRESS_DESCRIPTION: string; // адрес
    public PHONE_REGISTRY: string; // телефон регистратуры
    public SCHEDULE: string; // режим работы
    public LEADER_F: string; // фамилия руководителя
    public LEADER_I: string; // имя руководителя
    public LEADER_O: string; // отчество руководителя
    public PHONE_LEADER: string; // телефон руководителя
    public IS_DELETED: boolean; // признак "удален"

    public DESCRIPTION: string; //описание
    public ADDRESS_SHORT: string; //короткий адрес

    //Реквизиты
    public INN: string;
    public KPP: string;
    public OGRN: string;
    public OKTMO: string;

    //Контактная информация
    public FAX: string; //факс
    public EMAIL: string; //электронная почта
    public SITE: string; //сайт
    public BOOKER: string; //Бухгалтер

    init() {
        this.ID = 0;
        this.ID_CLINIC_SPECIALIZATION = -1;
    }
}