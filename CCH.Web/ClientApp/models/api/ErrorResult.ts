﻿
/**
 * Реезультат с сервера с ответом Bad
 */
export default class ErrorResult {
    public ErrorMessage: string;
    public ErrorData: object;

    /**
     * Ошибки по ключам
     */
    public KeyErrors: object; //{key: message, key2: message2, ...}; 
}


