﻿import ScheduleItem from "./ScheduleItem";
import Doctor from "./Doctor";
import Clinic from "./Clinic";

export enum StatusAppointmentEnum {
    ACTIVE, //Активна
    REQUIRES_CONFIRMATION, //Требует подтвердения
    CANCELED, //Отменена
    COMPLETED, //Завершена
}

export const StatusAppointmentType: any = {
    [StatusAppointmentEnum.ACTIVE]: 'active',
    [StatusAppointmentEnum.REQUIRES_CONFIRMATION]: 'requires_confirmation',
    [StatusAppointmentEnum.CANCELED]: 'canceled',
    [StatusAppointmentEnum.COMPLETED]: 'completed',
}

/**
 * Запись на приём
 */
export default class Appointment extends ScheduleItem {
    public ID_PATIENT: string;
    public STATUS: StatusAppointmentEnum;
    public PHONE_PATIENT: string;
    public TITLE: string;

    public DOCTOR: Doctor;
    public CLINIC: Clinic;

    /**
     * Загрузка данных в модель
     * @param {object} data
     * @testOk
     */
    parse(data?: any) {
        super.parse(data);

        if (data && typeof data === 'object') {
            if ('DOCTOR' in data && typeof data.DOCTOR === 'object') {
                this.DOCTOR = new Doctor(this.DOCTOR);
            }
            if ('CLINIC' in data && typeof data.CLINIC === 'object') {
                this.CLINIC = new Clinic(this.CLINIC);
            }
        }
    }
}


