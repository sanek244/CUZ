﻿/**
 * Специальности врачей
 */
export default class DoctorSpecialty {
    public ID: number; // код
    public NAME: string; // Название
    public SNOMED_CT: string; // Код SNOMED CT
    public IS_RECORD_AVAILABLE_PRIMARY_RECEPTION: boolean; // доступна ли для записи на первичный приём
}