﻿import Helper from "../Helper";


/**
 * Базовая модель с дополнительной внутренней логикой для всех моделей приложения
 * С сервера по Api приходят объекты с типом object в формате json, что бы они могли иметь функции заданные в их моделях, нужно их обернуть их же моделью:
 *      new Clinic(data)
 * Оборачивать можно глобально в ServiceController, сразу после вызова запроса: 
 *      return SessionServiceController.sendCoorsQuery('GET', this.host + '/api/clinics', ...).then((data : object[]) => data.map(x => new Clinic(x)));
 * А можно локально в компоненте приёма данных таким же способом.
 * Что бы данная обёртка сработала, класс должен быть унаследован от BaseModel
 */
export default class BaseModel {

    /**
     * Вызывается при создании класса
     */
    constructor(data?:any) {
        this.init(data);

        this.afterInit(data);
    }

    /**
     * @protected
     * Кастомная Инициализация класса
     */
    init(data?: any) {
    }

    /**
     * @protected
     * @param {object} data
     * @testOk
     */
    afterInit(data?:any) {
        //загрузка данных
        this.parse(data);
    }

    //*** Функции ***//
    /**
     * Загрузка данных в модель
     * @param {object} data
     * @testOk
     */
    parse(data?:any) {
        if (data && typeof data === 'object') {
            Object.assign(this, data);
        }
    }
}
