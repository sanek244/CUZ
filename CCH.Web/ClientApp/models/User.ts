﻿import Reminder from "./Reminder";

/**
 * Текущий пользователь (Singleton через User.instance)
 */

export default class User {
    public Id: string;

    public Name: string;
    public Surname: string;
    public Patronymic: string;

    public PhoneNumber: string;
    public Email: string;

    public Img: string = 'media/user-default.png'; 
    public SelectQualityServiceScore: number = 0; //выбранная оценка качества сервиса

    public Token: string;

    public Reminder?: Reminder;

    public IsAdmin: boolean;

    public static instance: User;
}
User.instance = new User();