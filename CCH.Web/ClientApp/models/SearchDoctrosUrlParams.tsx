export default class SearchDoctrosUrlParams{
    public ClinicId: string;
    public ClinicAdress: string;
    public DoctorFio: string;
    public IsOnlyAvailableRecording: string;
    public DoctorSpecialityIds: string;
    public ClinicSpecializationIds: string;
    public DateFrom: string;
    public DateTo: string;
    public TimeFrom: string;
    public TimeTo: string;

    constructor() {
        this.ClinicId = '';
        this.ClinicAdress = '';
        this.DoctorFio = '';
        this.IsOnlyAvailableRecording = '';
        this.DoctorSpecialityIds = '';
        this.ClinicSpecializationIds = '';
        this.DateFrom = '';
        this.DateTo = '';
        this.TimeFrom = '';
        this.TimeTo = '';
    }
}