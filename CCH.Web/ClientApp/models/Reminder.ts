﻿
//Частота напоминаний
export enum ReminderFrequency {
    PER_DAY, //1 раз в день
    EVERY_4_HOURS, //1 раз в 4 часа
    ONCE_PER_HOUR, //1 раз в час
}

export const ReminderFrequencyLabel: any = {
    [ReminderFrequency.PER_DAY]: '1 раз в день',
    [ReminderFrequency.EVERY_4_HOURS]: '1 раз в 4 часа',
    [ReminderFrequency.ONCE_PER_HOUR]: '1 раз в час',
}

/**
 * Напоминания пользователя
 */
export default class Reminder {

    public Id: number;

    public Id_user: string;

    //Напоминания влючены?
    public IsOn: boolean;

    //Частота напоминаний
    public Frequency: ReminderFrequency;

    //время напоминаний
    public Time: string;

    //Включён метод напоминаний push ?
    public IsOnMethodPush: boolean;

    //Включён метод напоминаний через смс ?
    public IsOnMethodSms: boolean;

    //Включён метод напоминаний через звонки ?
    public IsOnMethodCall: boolean;

    //Включён метод напоминаний по Email ?
    public IsOnMethodEmail: boolean;
}
