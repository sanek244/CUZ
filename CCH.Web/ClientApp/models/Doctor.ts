﻿import DoctorSpecialty from "./DoctorSpecialty";
import DoctorPosition from "./DoctorPosition";
import Helper from "../Helper";
import BaseModel from "./BaseModel";
import WorkExperience from "./WorkExperience";

/**
 * Врач
 */

export default class Doctor extends BaseModel {
    public ID: number; // код
    public ID_DOCTOR_SPECIALITY: number; // id специальности
    public ID_POSITION: number; // id должности
    public ID_QUALIFICATION_CATEGORY: number; // Квалификационная категория
    public DOCTOR_F: string; // фамилия
    public DOCTOR_I: string; // имя
    public DOCTOR_O: string; // отчество
    public EXPERIENCE: string; // стаж работы
    //public SCHEDULE: string; //График приема
    public IS_RECORD_AVAILABLE_PRIMARY_RECEPTION: boolean; //Доступна запись на первичный приём?
    public IS_DELETED: boolean; //удалён?

    public DOCTOR_SPECIALITY: DoctorSpecialty; 
    public WORK_EXPERIENCES: WorkExperience[];
    public POSITION: DoctorPosition; 

    /**
     * формат: Фамилия И.О.
     */
    public get FIO() {
        return Helper.upFirstChar(this.DOCTOR_F) + ' ' +
            (this.DOCTOR_I ? this.DOCTOR_I[0].toUpperCase() + '.' : '') +
            (this.DOCTOR_O ? this.DOCTOR_O[0].toUpperCase() + '.' : '');
    }

    /**
     * формат: Фамилия Имя Отчество
     */
    public get FIOFull() {
        return Helper.upFirstChar(this.DOCTOR_F) + ' ' + Helper.upFirstChar(this.DOCTOR_I) + ' ' + Helper.upFirstChar(this.DOCTOR_O);
    }

    init() {
        this.ID = 0;
        this.ID_DOCTOR_SPECIALITY = -1;
        this.ID_POSITION =  -1;
        this.ID_QUALIFICATION_CATEGORY = -1;
    }
}