﻿/**
 * Клиника (миниатюра)
 */
export default class ClinicMin {
    public ID: number; // код
    public NAME: string; // краткое наименование
    public FULL_NAME: string; // полное наименование
    public ADDRESS_DESCRIPTION: string; // адрес
}