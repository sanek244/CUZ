﻿import ScheduleItem from "./ScheduleItem";

/**
 * Элемент записи на приём, на все дни недели
 * Пример: на все пятницы с 10:00 до 14:00
 */
export default class ScheduleItemDayWeek extends ScheduleItem {
    public DAY: number;
}


