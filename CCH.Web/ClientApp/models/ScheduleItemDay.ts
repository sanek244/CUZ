﻿import ScheduleItem from "./ScheduleItem";

/**
 * Элемент записи на приём, на конкретный день (по приоритету выше, чем остальные дни)
 * Например: 30.12.2019 Приём с 10:00 до 12:00
 */
export default class ScheduleItemDay extends ScheduleItem {
    public DATE: string;
}


