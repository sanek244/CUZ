﻿/**
 * Должность врача
 */
export default class DoctorPosition {
    public ID: number; // код
    public NAME: string; // Название
}