﻿import BaseModel from "./BaseModel";

/**
 * График работы
 */

export default class ScheduleItem extends BaseModel{
    public ID: number;

    //Со скольки начинается приём посетителей (если не Appointment, то без даты - только время с часовым поясом)
    public FROM: string;

    //До скольки идёт приём (если не Appointment, то без даты - только время с часовым поясом)
    public TO: string;

    // Продолжительность (каждого) приёма в минутах
    public DURATION: number;

    //Кабинет
    public CABINET: string;

    //Этаж
    public STOREY: string;

    public get fromInt() {
        return this.dateToInt(this.FROM);
    }
    public get toInt() {
        return this.dateToInt(this.TO);
    }

    protected dateToInt(date: string) {
        try {
            const strHourse = date.substr(date.indexOf('T') + 1, 2);
            const strMinutes = date.substr(date.indexOf('T') + 4, 2);

            return parseInt(strHourse) * 60 + parseInt(strMinutes);
        }
        catch (err) {
            return 0;
        }
    }
}


