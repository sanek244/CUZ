export default class RegistrationForm{
    public Email: string;
    public Name: string;
    public Password: string;
    public PasswordAgain: string;

    constructor() {
        this.Email = '';
        this.Name = '';
        this.Password = '';
        this.PasswordAgain = '';
    }
}