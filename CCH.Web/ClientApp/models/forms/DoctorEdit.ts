﻿import BaseModel from "../BaseModel";

/**
 * Редактирование данных врача
 */
export default class DoctorEdit extends BaseModel {
    public ID: number; // код
    public ID_DOCTOR_SPECIALITY: number; // id специальности
    public ID_POSITION: number; // id должности
    public ID_QUALIFICATION_CATEGORY: number; // Квалификационная категория
    public DOCTOR_F: string; // фамилия
    public DOCTOR_I: string; // имя
    public DOCTOR_O: string; // отчество
    public IS_RECORD_AVAILABLE_PRIMARY_RECEPTION: boolean; //Доступна запись на первичный приём?
    public IS_DELETED: boolean; //удалён?
}