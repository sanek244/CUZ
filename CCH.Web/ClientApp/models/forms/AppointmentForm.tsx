export default class AppointmentForm{
    public idSchedule: number;
    public datetime: string;
    public phone: string;
}