﻿export default class ClinicEditFormError {
    public FullName: string;
    public Name: string;
    public AddressShort: string;
    public PhoneRegistry: string;
    public Specialization: string;
    public INN: string;
}