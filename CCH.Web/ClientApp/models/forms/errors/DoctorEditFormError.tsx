﻿export default class DoctorEditFormError {
    public DOCTOR_I: string;
    public DOCTOR_F: string;
    public ID_QUALIFICATION_CATEGORY: string;
    public ID_POSITION: string;
    public ID_DOCTOR_SPECIALITY: string;
}