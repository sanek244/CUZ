export default class RegistrationFormError{
    public Email: string;
    public Name: string;
    public Password: string;
    public PasswordAgain: string;
}