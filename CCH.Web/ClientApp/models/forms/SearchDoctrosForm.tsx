import * as moment from 'moment';
import SearchDoctrosUrlParams from "../SearchDoctrosUrlParams";
import Helper from "../../Helper";


export default class SearchDoctrosForm{
    public ClinicId: number | null;
    public ClinicAdress: string;
    public DoctorFio: string;
    public IsOnlyAvailableRecording: boolean;
    public DoctorSpecialityIds: number[];
    public ClinicSpecializationIds: number[];
    public DateFrom: Date | null;
    public DateTo: Date | null;
    public TimeFrom: Date | null;
    public TimeTo: Date | null;

    constructor(searchDoctrosUrlParams?: SearchDoctrosUrlParams) {
        this.ClinicId = null;
        this.ClinicAdress = '';
        this.DoctorFio = '';
        this.IsOnlyAvailableRecording = false;
        this.DoctorSpecialityIds = [];
        this.ClinicSpecializationIds = [];
        this.DateFrom = null;
        this.DateTo = null;
        this.TimeFrom = null;
        this.TimeTo = null;
        
        if (searchDoctrosUrlParams) {
            this.ClinicId = parseInt(searchDoctrosUrlParams.ClinicId);
            this.ClinicAdress = searchDoctrosUrlParams.ClinicAdress;
            this.DoctorFio = searchDoctrosUrlParams.DoctorFio;
            this.IsOnlyAvailableRecording = !!searchDoctrosUrlParams.IsOnlyAvailableRecording;
            this.DoctorSpecialityIds = searchDoctrosUrlParams.DoctorSpecialityIds.split(',').map(x => parseInt(x)).filter(x => x > -1);
            this.ClinicSpecializationIds = searchDoctrosUrlParams.ClinicSpecializationIds.split(',').map(x => parseInt(x)).filter(x => x > -1);

            this.DateFrom = searchDoctrosUrlParams.DateFrom !== "" ? new Date(searchDoctrosUrlParams.DateFrom) : null;
            this.DateTo = searchDoctrosUrlParams.DateTo !== "" ? new Date(searchDoctrosUrlParams.DateTo) : null;
            this.TimeFrom = searchDoctrosUrlParams.TimeFrom !== "" ?
                moment(`2000-01-01 ${searchDoctrosUrlParams.TimeFrom}`).add(-new Date().getTimezoneOffset(), "m").toDate() : null;
            this.TimeTo = searchDoctrosUrlParams.TimeTo !== "" ?
                moment(`2000-01-01 ${searchDoctrosUrlParams.TimeTo}`).add(-new Date().getTimezoneOffset(), "m").toDate() : null;
        }
    }
}