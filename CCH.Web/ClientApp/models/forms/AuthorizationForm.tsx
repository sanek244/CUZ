export default class AuthorizationForm{
    public Login: string;
    public PasswordHash: string;

    constructor(login?: string, password?: string) {
        this.Login = login || '';
        this.PasswordHash = password || '';
    }
}