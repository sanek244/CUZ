﻿import ScheduleItemDay from "./ScheduleItemDay";
import ScheduleItemDayWeek from "./ScheduleItemDayWeek";


/**
 * График работы
 */
export default class Schedule {
    public DayWeek: ScheduleItemDayWeek[];
    public SpecialDays: ScheduleItemDay[];
}
