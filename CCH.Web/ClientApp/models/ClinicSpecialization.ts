﻿/**
 * Специализация клиники
 */
export default class ClinicSpecialization {
    public ID: number; // код
    public NAME: string; // наименование

    public COLOR: string; // наименование
    public IMG: string; // наименование

    constructor() {
        this.ID = 0;
        this.COLOR = '#000';
    }
}