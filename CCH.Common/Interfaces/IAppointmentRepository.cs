﻿using CCH.Common.Entities;
using CCH.Common.Entities.Form;
using System;
using System.Collections.Generic;

namespace CCH.Common.Interfaces
{
    public interface IAppointmentRepository : IRepository<Appointment, long> {
        IEnumerable<Appointment> Get(DateTime date, long idDoctor, long idClinic, bool isOnlyActiveStatus);
        IEnumerable<Appointment> GetByIdUser(string idUser, bool isOnlyActiveStatus, bool isOnlyNotPassed);
        bool Cancel(long idAppointment, string idPatient);
        Appointment Add(string idPatient, FormAppointment form);
    }
}
