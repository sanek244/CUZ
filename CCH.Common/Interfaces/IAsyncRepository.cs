﻿using CCH.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CCH.Common.Interfaces
{
    public interface IAsyncRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        Task<TEntity> GetByIdAsync(TKey id);
        Task<List<TEntity>> ListAllAsync();
        Task<List<TEntity>> ListAsync(ISpecification<TEntity, TKey> spec);
        Task<TEntity> AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}