﻿using System;
using System.Collections.Generic;
using CCH.Common.Entities;
using CCH.Common.Entities.Form;
using CCH.Common.Entities.View;

namespace CCH.Common.Interfaces
{
    public interface IDoctorRepository: IRepository<Doctor, long> {

        Schedule GetSchedule(long idDoctor, long idClinic);
        IEnumerable<DateTime> GetBusyDays(long idDoctor, long idClinic); 
        IEnumerable<Doctor> Search(FormSearchDoctors form);
        IEnumerable<Clinic> GetClinics(long idDoctor, bool isOnlyWithSheduleItems = false);
    }
}
