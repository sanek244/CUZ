﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IReminderRepository : IRepository<Reminder, long> {
        Reminder GetByUserId(string idUser);
    }
}
