﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IScheduleItemDayWeekRepository : IRepository<ScheduleItemDayWeek, long> { }
}
