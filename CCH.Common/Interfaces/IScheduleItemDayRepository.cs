﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IScheduleItemDayRepository : IRepository<ScheduleItemDay, long> { }
}
