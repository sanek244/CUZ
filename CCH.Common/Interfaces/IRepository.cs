﻿using CCH.Common.Entities;
using System.Collections.Generic;

namespace CCH.Common.Interfaces
{
    public interface IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        TEntity GetById(TKey id);
        TEntity GetSingleBySpec(ISpecification<TEntity, TKey> spec);
        IEnumerable<TEntity> ListAll();
        IEnumerable<TEntity> List(ISpecification<TEntity, TKey> spec);
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}