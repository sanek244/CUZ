﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IClinicRepository: IRepository<Clinic, long> { }
}
