﻿using System.Threading.Tasks;

namespace CCH.Common.Interfaces
{

    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
