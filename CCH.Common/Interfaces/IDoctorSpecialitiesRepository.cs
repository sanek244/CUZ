﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IDoctorSpecialitiesRepository : IRepository<DoctorSpeciality, long> { }
}
