﻿using CCH.Common.Entities;

namespace CCH.Common.Interfaces
{
    public interface IClinicSpecializationRepository: IRepository<ClinicSpecialization, long> { }
}
