﻿using System.Security.Principal;

namespace CCH.Common.Interfaces
{
    public interface IIdentityParser<T>
    {
        T Parse(IPrincipal principal);
    }
}
