﻿using System;

namespace CCH.Common.Interfaces
{
    /// <summary>
    /// Логер
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAppLogger<T>
    {
        void LogInformation(string message);
        void LogDebug(string message);
        void LogError(Exception exception, string message);
        void LogTrace(string message);
        void LogWarning(string message);
    }
}
