﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CCH.Common.Settings
{
    public class AuthOptions
    {
        public const string ISSUER = "http://localhost:5000";   // издатель токена
        public const string AUDIENCE = "http://localhost:5000"; // потребитель токена
        const string KEY = "mysupersecret_secretkey!12345";       // ключ для шифрования
        public const int LIFETIME = 1 * 60 * 24;                // время жизни токена
        public static SymmetricSecurityKey GetIssuerSigningKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
        }
    }
}
