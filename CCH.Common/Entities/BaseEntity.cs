﻿using System.ComponentModel.DataAnnotations;

namespace CCH.Common.Entities
{
    public class BaseEntity<TKey>
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public TKey ID { get; set; }
    }
}