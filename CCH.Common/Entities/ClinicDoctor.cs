﻿
namespace CCH.Common.Entities
{
    /// <summary>
    /// Связи между клиниками и врачами
    /// </summary>
    public class ClinicDoctor : BaseEntity<long>
    {
        /// <summary>
        /// Ссылка на клинику 
        /// </summary>
        // Ссылка на клинику 	ID_CLINIC	FK, int	  Not null	Обязательное
        public long ID_CLINIC { get; set; }

        /// <summary>
        /// Ссылка на врача 
        /// </summary>
        // Ссылка на врача 	ID_DOCTOR	FK, int	  Not null	Обязательное
        public long ID_DOCTOR { get; set; }

        /// <summary>
        /// Должность в клинике 
        /// </summary>
        // Должность в клинике 	ID_POSITION	FK, int	  Not null	Обязательное
        public long ID_POSITION { get; set; }

        /// <summary>
        /// Табельный номер  
        /// </summary>
        // Табельный номер  	PERSONNEL_NUMBER	NVARCHAR(255)	Not null	Обязательное
        public string PERSONNEL_NUMBER { get; set; }
    }
}