﻿
namespace CCH.Common.Entities
{
    /// <summary>
    /// Квалификационные категории 
    /// </summary>
    public class QualificationCategory : BaseEntity<long>
    {
        /// <summary>
        /// Наименование
        /// </summary>
        // Наименование	NAME	NVARCHAR (510)	пользователь	Not null	Обязательное
        public string NAME { get; set; }
    }
}