﻿using System;

namespace CCH.Common.Entities.DB
{
    public class CountByDate
    {
        public long Count;
        public DateTime Date;
    }
}
