﻿using System;

namespace CCH.Common.Entities
{
    /// <summary>
    /// Стаж работы (1 экземпляр - 1 место прежней работы)
    /// </summary>
    public class WorkExperience : BaseEntity<long>
    {
        /// <summary>
        /// id врача
        /// </summary>
        public long ID_DOCTOR { get; set; }

        /// <summary>
        /// Место прежней работы
        /// не привязываемся к ID_CLINIC, т.к. место прежней работы могло быть за пределами текущих клиник или имело другое название
        /// </summary>
        public string PLACE { get; set; }

        /// <summary>
        /// Занимаемая должность
        /// не привязываемся к ID_POSTION, т.к. прежняя должность могла быть за пределами текущих должностей или иметь другое название
        /// </summary>
        public string POSITION { get; set; }

        /// <summary>
        /// Дата начала работы
        /// </summary>
        public DateTime FROM { get; set; }

        /// <summary>
        /// Дата окончания работы
        /// </summary>
        public DateTime TO { get; set; }

        /// <summary>
        /// Продолжительность работы
        /// </summary>
        public string DURATION
        {
            get
            {
                int difMonths = TO.Month - FROM.Month; //с 0000.01.01 по 0000.12.01 = 11 месяцев 
                int difYears = TO.Year - FROM.Year + (difMonths < 0 ? -1 : 0); //с 2001.12.01 по 2002.01.01 = 0 лет 1 месяц 

                if(difMonths < 0) {
                    difMonths = 12 + difMonths;
                }

                string res = "";
                if (difYears > 0) {
                    //1-4 года 5-20 лет 21-24 года 25-30 лет... 101-104 год 105-120 лет, ...
                    res = difYears + (difYears % 100 > 4 && difYears % 100  < 20 || difYears % 10 > 4 ? "лет" : (difYears % 10 > 1 ? "года" : "год")) + " ";
                };

                if (difMonths > 0)
                {
                    res += difMonths + "мес.";
                };

                return res;
            }
        }
    }
}
