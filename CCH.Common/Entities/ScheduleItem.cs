﻿using System;

namespace CCH.Common.Entities
{
    /// <summary>
    /// расписание врача на день - абстрактный класс
    /// </summary>
    public abstract class ScheduleItem: BaseEntity<long>
    {
        /// <summary>
        /// Время начала приём(а/ов)
        /// </summary>
        public DateTime FROM { get; set; }

        /// <summary>
        /// Время окончания приём(а/ов)
        /// </summary>
        public DateTime TO { get; set; }

        /// <summary>
        /// Доктор
        /// </summary>
        public long ID_DOCTOR { get; set; }

        /// <summary>
        /// Клиника
        /// </summary>
        public long ID_CLINIC { get; set; }

        /// <summary>
        /// Продолжительность (каждого) приёма в минутах
        /// </summary>
        public short DURATION { get; set; }

        /// <summary>
        /// Кабинет приёма
        /// </summary>
        public string CABINET { get; set; }

        /// <summary>
        /// Этаж
        /// </summary>
        public short STOREY { get; set; }



        public Doctor DOCTOR { get; set; }
        public Clinic CLINIC { get; set; }
    }
}
