﻿
namespace CCH.Common.Entities
{
    /// <summary>
    /// Специальности врачей 
    /// https://nsi.rosminzdrav.ru/#!/refbook/1.2.643.5.1.13.13.11.1002
    /// </summary>
    public class DoctorSpeciality : BaseEntity<long>
    {
        /// <summary>
        /// Наименование
        /// </summary>
        // Наименование	NAME	NVARCHAR (510)	пользователь	Not null	Обязательное
        public string NAME { get; set; }

        /// <summary>
        /// Код SNOMED CT
        /// </summary>
        // Код SNOMED CT	SNOMED_CT	NVARCHAR (510)	пользователь	Not null	Обязательное
        public string SNOMED_CT { get; set; }

        /// <summary>
        /// Доступна запись на первичный приём?
        /// </summary>
        public bool IS_RECORD_AVAILABLE_PRIMARY_RECEPTION { get; set; }
    }
}