﻿namespace CCH.Common.Entities.View
{
    public class ClinicMin
    {
        public long ID { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        //Полное наименование FULL_NAME   NVARCHAR(510)  пользователь Not null	Обязательное
        public string FULL_NAME { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        //Краткое наименование NAME    NVARCHAR(510)  пользователь Not null	Обязательное
        public string NAME { get; set; }

        /// <summary>
        /// Адрес, описание
        /// </summary>
        //Адрес описание ADRESS_DESCRIPTION  NVARCHAR(510)   введение описания адреса пользователем  Not null	Не обязательное
        public string ADDRESS_DESCRIPTION { get; set; }
    }
}
