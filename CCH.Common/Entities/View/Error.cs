﻿
namespace CCH.Common.Entities.View
{
    public class Error
    {
        public Error(string errorMessage) {
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage;
    }
}
