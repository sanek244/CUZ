﻿namespace CCH.Common.Entities.View
{
    /// <summary>
    /// График работы
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// График работы по дням недели
        /// </summary>
        public ScheduleItemDayWeek[] DayWeek { get; set; }

        /// <summary>
        /// График работы в особые дни (праздники, ...)
        /// </summary>
        public ScheduleItemDay[] SpecialDays { get; set; }
    }
}
