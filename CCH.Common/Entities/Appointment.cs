﻿namespace CCH.Common.Entities
{
    /// <summary>
    /// Запись на приём
    /// </summary>
    public class Appointment: ScheduleItem
    {
        /// <summary>
        /// Пациент
        /// </summary>
        public string ID_PATIENT { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public StatusAppointmentEnum STATUS { get; set; }

        /// <summary>
        /// Телефон, указанный пациентом при записи
        /// </summary>
        public string PHONE_PATIENT { get; set; }

        /// <summary>
        /// Подпись приёма
        /// Формат 1: <Название приёма>; <Краткое название больницы> 
        /// Пример: Первичный приём; Поликлиника №34
        /// Формат 2: <ФИО врача>; <специальность врача>; <Краткое название больницы>; 
        /// Пример: Первухина Е.Ф.; врач-онколог; Поликлиника №34
        /// </summary>
        public string TITLE { get; set; }

        public bool IsActiveStatus
        {
            get
            {
                return STATUS == StatusAppointmentEnum.ACTIVE || STATUS == StatusAppointmentEnum.REQUIRES_CONFIRMATION;
            }
        }
    }

    /// <summary>
    /// Статус приёма
    /// </summary>
    public enum StatusAppointmentEnum
    {
        ACTIVE, //Активна
        REQUIRES_CONFIRMATION, //Требует подтвердения
        CANCELED, //Отменена
        COMPLETED, //Завершена
    }
}
