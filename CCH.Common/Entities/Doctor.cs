﻿using CCH.Common.Entities.View;
using System;
using System.Collections.Generic;

namespace CCH.Common.Entities
{
    public class Doctor : BaseEntity<long>
    {
        /// <summary>
        /// Фамилия 
        /// </summary>
        // Фамилия 	Doctor_F	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_F { get; set; }

        /// <summary>
        /// Имя  
        /// </summary>
        // Имя  	Doctor_I	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_I { get; set; }

        /// <summary>
        /// Отчество  
        /// </summary>
        // Отчество  Doctor_O	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_O { get; set; }

        /// <summary>
        /// Специальность
        /// </summary>
        //Специальность ID_DOCTOR_SPECIALITY    FK, int  Not null	Обязательное
        public long ID_DOCTOR_SPECIALITY { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        //Должность POSITION    FK, int  Not null	Обязательное
        public long ID_POSITION { get; set; }

        /// <summary>
        /// Стаж работы  
        /// </summary>
        // Стаж работы  EXPERIENCE	NVARCHAR (50)	Вычисляется системой как разница между полями Начало и Завершение по всем организациям. Not null	Обязательное
        public string EXPERIENCE { get; set;  } //TODO: сделать вычисляемым и убрать set

        /// <summary>
        /// Квалификационная категория
        /// </summary>
        //Квалификационная категория ID_QUALIFICATION_CATEGORY    FK, int  Not null	Обязательное
        public long ID_QUALIFICATION_CATEGORY { get; set; }

        /// <summary>
        /// Доступна запись на первичный приём?
        /// При создании/смене специализации  брать значение из специализации
        /// </summary>
        public bool IS_RECORD_AVAILABLE_PRIMARY_RECEPTION { get; set; }

        /// <summary>
        /// Признак "удален"
        /// </summary>
        // Признак "удален" 
        public bool IS_DELETED { get; set; }


        public DoctorSpeciality DOCTOR_SPECIALITY { get; set; }
        public IEnumerable<WorkExperience> WORK_EXPERIENCES { get; set; }
    }
}