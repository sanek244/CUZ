﻿namespace CCH.Common.Entities
{
    /// <summary>
    /// Пренадлежит классу ApplicationUser - содержит настройки Напоминаний по календарю
    /// </summary>
    public class Reminder : BaseEntity<long>
    {
        public string IdUser { get; set; }

        /// <summary>
        /// Напоминания влючены?
        /// </summary>
        public bool IsOn { get; set; }

        /// <summary>
        /// Частота напоминаний
        /// </summary>
        public ReminderFrequencyEnum Frequency { get; set; }

        /// <summary>
        /// Время напоминаний
        /// Формат: HH:mm
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Включён метод напоминаний push ?
        /// </summary>
        public bool IsOnMethodPush { get; set; }

        /// <summary>
        /// Включён метод напоминаний через смс ?
        /// </summary>
        public bool IsOnMethodSms { get; set; }

        /// <summary>
        /// Включён метод напоминаний через звонки ?
        /// </summary>
        public bool IsOnMethodCall { get; set; }

        /// <summary>
        /// Включён метод напоминаний по Email ?
        /// </summary>
        public bool IsOnMethodEmail { get; set; }

        public Reminder() { }
        public Reminder(string idUser)
        {
            IdUser = idUser;
        }
    }


    /// <summary>
    /// Частота напоминаний
    /// </summary>
    public enum ReminderFrequencyEnum
    {
        PER_DAY, //1 раз в день
        EVERY_4_HOURS, //1 раз в 4 часа
        ONCE_PER_HOUR, //1 раз в час
    }

}
