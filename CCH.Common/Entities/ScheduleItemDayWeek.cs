﻿using System;

namespace CCH.Common.Entities
{
    /// <summary>
    /// Расписание, на день недели
    /// Пример: на все пятницы с 10:00 до 14:00
    /// </summary>
    public class ScheduleItemDayWeek: ScheduleItem
    {
        /// <summary>
        /// День недели
        /// </summary>
        public DayOfWeek DAY { get; set; }
    }
}
