﻿using System.Collections.Generic;

namespace CCH.Common.Entities
{
    public class ClinicSpecialization : BaseEntity<long>
    {
        /// <summary>
        /// Наименование
        /// </summary>
        // Наименование	NAME	NVARCHAR (510)	пользователь	Not null	Обязательное
        public string NAME { get; set; }
    }
}