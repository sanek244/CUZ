﻿using System;

namespace CCH.Common.Entities.Form
{
    /// <summary>
    /// Форма поиска врачей
    /// </summary>
    public class FormSearchDoctors
    {
        public long? ClinicId { get; set; }
        
        public string ClinicAdress { get; set; }

        public string DoctorFio { get; set; }

        public bool IsOnlyAvailableRecording { get; set; }

        public long[] DoctorSpecialityIds { get; set; }

        public long[] ClinicSpecializationIds { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public DateTime? TimeFrom { get; set; }

        public DateTime? TimeTo { get; set; }
    }
}
