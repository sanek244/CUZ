﻿using System.ComponentModel.DataAnnotations;

namespace CCH.Common.Entities.Form
{
    /// <summary>
    /// Форма авторизации
    /// </summary>
    public class FormAuthorization
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string PasswordHash { get; set; }
    }
}
