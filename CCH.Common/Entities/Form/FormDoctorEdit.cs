﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCH.Common.Entities.Form
{
    public class FormDoctorEdit
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Фамилия 
        /// </summary>
        // Фамилия 	Doctor_F	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_F { get; set; }

        /// <summary>
        /// Имя  
        /// </summary>
        // Имя  	Doctor_I	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_I { get; set; }

        /// <summary>
        /// Отчество  
        /// </summary>
        // Отчество  Doctor_O	NVARCHAR (50)	Not null	Обязательное
        public string DOCTOR_O { get; set; }

        /// <summary>
        /// Специальность
        /// </summary>
        //Специальность ID_DOCTOR_SPECIALITY    FK, int  Not null	Обязательное
        public long ID_DOCTOR_SPECIALITY { get; set; }

        /// <summary>
        /// Доступна запись на первичный приём?
        /// При создании/смене специализации  брать значение из специализации
        /// </summary>
        public bool IS_RECORD_AVAILABLE_PRIMARY_RECEPTION { get; set; }

        /// <summary>
        /// Признак "удален"
        /// </summary>
        // Признак "удален" 
        public bool IS_DELETED { get; set; }
    }
}