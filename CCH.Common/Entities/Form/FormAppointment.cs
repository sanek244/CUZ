﻿using System;

namespace CCH.Common.Entities.Form
{
    public class FormAppointment
    {
        public string Phone;
        public DateTime Datetime;
        public long IdSchedule;
    }
}
