﻿using System;

namespace CCH.Common.Entities
{
    /// <summary>
    /// Расписание, на конкретный день (по приоритету выше, чем остальные дни)
    /// Пример: 30.12.2019 Приём с 10:00 до 12:00
    /// </summary>
    public class ScheduleItemDay: ScheduleItem
    {
        /// <summary>
        /// День
        /// </summary>
        public DateTime DATE { get; set; }
    }
}
