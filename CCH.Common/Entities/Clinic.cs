﻿using CCH.Common.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCH.Common.Entities
{
    public class Clinic: BaseEntity<long>, IValidatableObject
    {
        //Код ID  PK, int Генерируется системой при сохранении записи в БД    Not null	Обязательное
        // ID

        #region Основное

        /// <summary>
        /// Полное наименование
        /// </summary>
        //Полное наименование FULL_NAME   NVARCHAR(510)  пользователь Not null	Обязательное
        public string FULL_NAME { get; set; }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        //Краткое наименование NAME    NVARCHAR(510)  пользователь Not null	Обязательное
        public string NAME { get; set; }

        /// <summary>
        /// Специализация клиники
        /// </summary>
        //Специализация клиники ID_CLINIC_SPECIALIZATION    FK, int выбор из справочника Специализация клиники  Not null	Обязательное
        public long ID_CLINIC_SPECIALIZATION { get; set; }

        /// <summary>
        /// Краткое описание адреса: 'на Рижском шоссе'
        /// </summary> 
        public string ADDRESS_SHORT { get; set; }

        /// <summary>
        /// Адрес, описание
        /// </summary>
        //Адрес описание ADRESS_DESCRIPTION  NVARCHAR(510)   введение описания адреса пользователем  Not null	Не обязательное
        public string ADDRESS_DESCRIPTION { get; set; }

        /// <summary>
        /// Адрес ФИАС
        /// </summary>
        //Адрес ФИАС*	ID_ADRESS FK, int выбор адреса из справочника ФИАС    Not null	Обязательное
        public long ID_ADDRESS { get; set; }

        /// <summary>
        /// Телефон регистратуры
        /// </summary>
        //Телефон регистратуры PHONE_REGISTRY  NVARCHAR(510)   пользователь Not null	Обязательное
        public string PHONE_REGISTRY { get; set; }

        /// <summary>
        /// Режим работы
        /// </summary>
        //Режим работы SHEDULE DATETIME пользователь    Not null	Обязательное
        public DateTime SCHEDULE_DATETIME { get; set; }

        /// <summary>
        /// Фамилия руководителя
        /// </summary>
        //Фамилия руководителя LEADER_F    NVARCHAR(50)    пользователь Not null	Не обязательное
        public string LEADER_F { get; set; }

        /// <summary>
        /// Имя руководителя
        /// </summary>
        //Имя руководителя    LEADER_I NVARCHAR(50)    пользователь Not null	Не обязательное
        public string LEADER_I { get; set; }

        /// <summary>
        /// Отчество руководителя
        /// </summary>
        //Отчество руководителя   LEADER_O NVARCHAR(50)    пользователь Not null	Не обязательное
        public string LEADER_O { get; set; }

        /// <summary>
        /// Телефон руководителя
        /// </summary>
        //Телефон руководителя    PHONE_LEADER NVARCHAR(510)   пользователь Not null	Не обязательное
        public string PHONE_LEADER { get; set; }

        /// <summary>
        /// Признак "удален"
        /// </summary>
        // Признак "удален" 
        public bool IS_DELETED { get; set; }

        #endregion

        #region Реквизиты клиники

        /// <summary>
        /// ИНН
        /// </summary>
        //ИНН INN NVARCHAR(12)   пользователь Not null	Обязательное
        public string INN { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        //КПП KPP NVARCHAR(12)   пользователь Not null	Не обязательное
        public string KPP { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        //ОГРН OGRN    NVARCHAR(15)   пользователь Not null	Не обязательное
        public string OGRN { get; set; }

        /// <summary>
        /// OKTMO
        /// </summary>
        //ОКТМО OKTMO   NVARCHAR(15)   пользователь Not null	Не обязательное
        public string OKTMO { get; set; }

        #endregion

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!this.INN.IsValidINN())
                yield return new ValidationResult("Неверный ИНН", new[] { "INN" });
        }
    }
}
