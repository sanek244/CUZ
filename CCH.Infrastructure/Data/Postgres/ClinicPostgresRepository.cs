﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;

namespace CCH.Infrastructure.Data.Postgres
{
    public class ClinicPostgresRepository: PostgresRepository<Clinic, long>, IClinicRepository
    {
        public ClinicPostgresRepository(CchContext context) : base(context) { }
    }
}