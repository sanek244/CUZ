﻿using System;
using System.Data;
using Dapper;
using Npgsql;
using NpgsqlTypes;

namespace CCH.Infrastructure.Data.Postgres.ParameterTypes
{
    internal class DateParameter : SqlMapper.ICustomQueryParameter
    {
        #region Fields

        private readonly string _value;

        #endregion

        #region Constructors

        public DateParameter(DateTime? value)
        {
            _value = value?.ToString("yyyy-MM-dd");
        }

        public DateParameter(DateTime value)
        {
            _value = value.ToString("yyyy-MM-dd");
        }

        #endregion

        #region Implementation of interfaces

        public void AddParameter(IDbCommand command, string name)
        {
            if (_value != null)
                command.Parameters.Add(new NpgsqlParameter
                {
                    ParameterName = name,
                    NpgsqlDbType = NpgsqlDbType.Date,
                    Value = _value ?? ""
                });
        }

        #endregion
    }
}
