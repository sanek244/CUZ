﻿using System;
using System.Data;
using Dapper;
using Npgsql;
using NpgsqlTypes;

namespace CCH.Infrastructure.Data.Postgres.ParameterTypes
{
    internal class TimeParameter : SqlMapper.ICustomQueryParameter
    {
        #region Fields

        private readonly string _value;

        #endregion

        #region Constructors

        public TimeParameter(DateTime? value)
        {
            _value = value?.ToString("HH:mm:ss");
        }

        public TimeParameter(DateTime value)
        {
            _value = value.ToString("HH:mm:ss");
        }

        #endregion

        #region Implementation of interfaces

        public void AddParameter(IDbCommand command, string name)
        {
            if (_value != null)
                command.Parameters.Add(new NpgsqlParameter
                {
                    ParameterName = name,
                    NpgsqlDbType = NpgsqlDbType.Time,
                    Value = _value ?? ""
                });
        }

        #endregion
    }
}