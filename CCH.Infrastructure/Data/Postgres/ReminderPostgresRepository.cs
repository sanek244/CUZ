﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CCH.Infrastructure.Data.Postgres
{
    public class ReminderPostgresRepository : PostgresRepository<Reminder, long>, IReminderRepository
    {
        public ReminderPostgresRepository(CchContext context) : base(context) { }


        public Reminder GetByUserId(string idUser)
        {
            return _context.Reminder.AsNoTracking().FirstOrDefault(x => x.IdUser == idUser)?? Add(new Reminder(idUser));
        }
    }
}