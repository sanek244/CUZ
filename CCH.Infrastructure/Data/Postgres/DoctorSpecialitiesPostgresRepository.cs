﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;

namespace CCH.Infrastructure.Data.Postgres
{
    public class DoctorSpecialitiesPostgresRepository : PostgresRepository<DoctorSpeciality, long>, IDoctorSpecialitiesRepository
    {
        public DoctorSpecialitiesPostgresRepository(CchContext context) : base(context) { }
    }
}