﻿using CCH.Common.Entities;
using CCH.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCH.Infrastructure.Data.Postgres
{
    public class ClinicSpecializationPostgresRepository: PostgresRepository<ClinicSpecialization, long>, IClinicSpecializationRepository
    {
        public ClinicSpecializationPostgresRepository(CchContext context) : base(context) { }
    }
}