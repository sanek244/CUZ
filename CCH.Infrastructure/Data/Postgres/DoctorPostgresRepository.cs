﻿using CCH.Common.Entities;
using CCH.Common.Entities.View;
using CCH.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Npgsql;
using CCH.Common.Entities.Form;
using CCH.Infrastructure.Data.Postgres.ParameterTypes;
using CCH.Common.Entities.DB;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CCH.Infrastructure.Data.Postgres
{
    public class DoctorPostgresRepository: PostgresRepository<Doctor, long>, IDoctorRepository
    {
        public DoctorPostgresRepository(CchContext context) : base(context)
        {
        }

        /// <summary>
        /// Выдаёт врача по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Doctor GetById(long id)
        {
            return _context.Doctor
                .Include(x => x.DOCTOR_SPECIALITY)
                .Include(x => x.WORK_EXPERIENCES)
                .FirstOrDefault(x => x.ID == id);
        }

        /// <summary>
        /// Выдаёт расписание врача в клинике
        /// </summary>
        /// <param name="idDoctor"></param>
        /// <param name="idClinic"></param>
        /// <returns></returns>
        public Schedule GetSchedule(long idDoctor, long idClinic)
        {
            return new Schedule(){
                SpecialDays = _context.ScheduleItemDay
                    .Where(x => x.ID_DOCTOR == idDoctor && x.ID_CLINIC == idClinic && x.DATE > DateTime.Now.AddDays(-1))
                    .ToArray(),

                DayWeek = _context.ScheduleItemDayWeek
                    .Where(x => x.ID_DOCTOR == idDoctor && x.ID_CLINIC == idClinic)
                    .ToArray()
            };
        }

        /// <summary>
        /// Поиск врачей по параметрам
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public IEnumerable<Doctor> Search(FormSearchDoctors form)
        {
            using (var conn = new NpgsqlConnection(_context.ConnectionString))
            {
                object param = new
                {
                    ids_doctor_speciality = form.DoctorSpecialityIds,
                    ids_clinic_speciality = form.ClinicSpecializationIds,
                    id_clinic = form.ClinicId,
                    fio = form.DoctorFio,
                    adress_clinic = form.ClinicAdress,
                    is_available_recording = form.IsOnlyAvailableRecording,
                    date_from = new DateParameter(form.DateFrom),
                    date_to = new DateParameter(form.DateTo),
                    time_from = new TimeParameter(form.TimeFrom),
                    time_to = new TimeParameter(form.TimeTo)
                };
                return conn.Query<Doctor>("public.doctor_search", param, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Возвращает клиники, к которым привязан доктор
        /// </summary>
        /// <param name="idDoctor"></param>
        /// <param name="isOnlyWithSheduleItems">вернуть только те клиники, в которых врач имеет расписание приёма</param>
        /// <returns></returns>
        public IEnumerable<Clinic> GetClinics(long idDoctor, bool isOnlyWithSheduleItems = false)
        {
            var clinicDoctors = _context.ClinicDoctor.Where(x => x.ID_DOCTOR == idDoctor);

            if (isOnlyWithSheduleItems)
            {
                clinicDoctors = clinicDoctors.Where(x => 
                    _context.ScheduleItemDay.FirstOrDefault(s => s.ID_CLINIC == x.ID_CLINIC && s.ID_DOCTOR == idDoctor) != null ||
                    _context.ScheduleItemDayWeek.FirstOrDefault(s => s.ID_CLINIC == x.ID_CLINIC && s.ID_DOCTOR == idDoctor) != null);
            }

            return clinicDoctors.Select(x => _context.Clinics.FirstOrDefault(c => c.ID == x.ID_CLINIC)).ToArray();
        }

        /// <summary>
        /// Выдаёт все дни врача, в которых все записи на приём уже заняты
        /// </summary>
        /// <param name="idDoctor"></param>
        /// <param name="idClinic"></param>
        /// <returns></returns>
        public IEnumerable<DateTime> GetBusyDays(long idDoctor, long idClinic)
        {
            CountByDate[] appointmentCountByDates = null;

            //получаем количество записей на дни
            using (var conn = new NpgsqlConnection(_context.ConnectionString))
            {
                object param = new
                {
                    id_clinic = idClinic,
                    id_doctor = idDoctor,
                };
                appointmentCountByDates = conn.Query<CountByDate>("public.count_appointments_by_day", param, commandType: CommandType.StoredProcedure).ToArray();
            }

            //если нету, то ок
            if(appointmentCountByDates.Length == 0)
            {
                return new DateTime[0];
            }

            //считаем, сколько по графику имееться возможных записей
            var shedule = GetSchedule(idDoctor, idClinic);

            //особые дни
            var countAppointmentsByDates = new Dictionary<DateTime, short>();
            shedule.SpecialDays.ToList().ForEach(specialDay =>
            {
                //если на этот день есть записи
                if (appointmentCountByDates.FirstOrDefault(appointmentCountByDate => appointmentCountByDate.Date.Date == specialDay.FROM.Date) != null)
                {
                    //считаем количество возможных записей на особый день
                    var dif = specialDay.TO - specialDay.FROM;
                    short count = (short)(dif.TotalMinutes / specialDay.DURATION);

                    if (countAppointmentsByDates.ContainsKey(specialDay.DATE)) {
                        countAppointmentsByDates[specialDay.DATE] += count;
                    }
                    else {
                        countAppointmentsByDates.Add(specialDay.DATE, count);
                    }
                }
            });

            //будние дни
            var countAppointmentsByWeekDay = new Dictionary<short, short>();
            shedule.DayWeek.ToList().ForEach(dayWeek =>
            {
                //если на этот день есть записи
                if (appointmentCountByDates.FirstOrDefault(appointmentCountByDate => appointmentCountByDate.Date.DayOfWeek == dayWeek.DAY) != null)
                {
                    //считаем количество возможных записей на будний день
                    var dif = dayWeek.TO - dayWeek.FROM;
                    short count = (short)(dif.TotalMinutes / dayWeek.DURATION);

                    if (countAppointmentsByWeekDay.ContainsKey((short)dayWeek.DAY)) {
                        countAppointmentsByWeekDay[(short)dayWeek.DAY] += count;
                    }
                    else {
                        countAppointmentsByWeekDay.Add((short)dayWeek.DAY, count);
                    }
                }
            });

            //по занятым записям смотрим, заняли ли они весь график на день или нет
            var res = new List<DateTime>();
            appointmentCountByDates.ToList().ForEach(appointmentCountByDate =>
            {
                //сперва на спец дни смотрим
                var date = appointmentCountByDate.Date.Date;
                if (countAppointmentsByDates.ContainsKey(date))
                {
                    if(appointmentCountByDate.Count >= countAppointmentsByDates[date]){
                        res.Add(date);
                    }
                    return; //continue для ForEach
                }

                //будние дни
                var day = (short)appointmentCountByDate.Date.DayOfWeek;
                if (countAppointmentsByWeekDay.ContainsKey(day) && appointmentCountByDate.Count >= countAppointmentsByWeekDay[day])
                {
                    res.Add(date);
                }
            });

            return res;
        }


        public new Doctor Add(Doctor entity)
        {
            AddLogic(entity);
            _context.Doctor.Add(entity);
            _context.SaveChanges();

            return entity;
        }
        public new async Task<Doctor> AddAsync(Doctor entity)
        {
            AddLogic(entity);
            _context.Doctor.Add(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public new void Update(Doctor entity)
        {
            UpdateLogic(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public new async Task UpdateAsync(Doctor entity)
        {
            UpdateLogic(entity);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Логика изменения модели
        /// </summary>
        /// <param name="entity"></param>
        private void UpdateLogic(Doctor entity)
        {
            //если изменилась специальность
            Doctor oldValue = GetById(entity.ID);
            if(oldValue.ID_DOCTOR_SPECIALITY != entity.ID_DOCTOR_SPECIALITY) {
                //берём первичный приём из специальности
                AddLogic(entity);
            }
        }

        /// <summary>
        /// Логика добавления модели
        /// </summary>
        /// <param name="entity"></param>
        private void AddLogic(Doctor entity)
        {
            //первичный приём берём из специальности, если указана
            DoctorSpeciality ds = _context.DoctorSpeciality.FirstOrDefault(x => x.ID == entity.ID_DOCTOR_SPECIALITY);
            if (ds != null) {
                entity.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION = ds.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION;
            }
        }
    }
}