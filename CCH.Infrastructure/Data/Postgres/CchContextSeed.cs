﻿using CCH.Common.Entities;
using CCH.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCH.Infrastructure.Data.Postgres
{
    /// <summary>
    /// Заполнение БД начальными данными
    /// </summary>
    public class CchContextSeed
    {
        public static async Task SeedAsync(CchContext context, ILoggerFactory loggerFactory, int? retry = 0)
        {
            int retryForAvailability = retry.Value;
            try
            {

                // TODO: Only run this if using a real database
                context.Database.Migrate();

                if (!context.ClinicSpecializations.Any())
                {
                    context.ClinicSpecializations.AddRange(GetPreconfiguredClinicSpecializations());
                    await context.SaveChangesAsync();
                }

                if (!context.Clinics.Any())
                {
                    context.Clinics.AddRange(GetPreconfiguredClinics());
                    await context.SaveChangesAsync();
                }

                if (!context.DoctorSpeciality.Any())
                {
                    context.DoctorSpeciality.AddRange(GetPreconfiguredDoctorSpecialities());
                    await context.SaveChangesAsync();
                }

                if (!context.Doctor.Any())
                {
                    context.Doctor.AddRange(GetPreconfiguredDoctor());
                    await context.SaveChangesAsync();
                }

                if (!context.ClinicDoctor.Any())
                {
                    context.ClinicDoctor.AddRange(GetPreconfiguredClinicDoctor());
                    await context.SaveChangesAsync();
                }

                if (!context.ScheduleItemDayWeek.Any())
                {
                    context.ScheduleItemDayWeek.AddRange(GetPreconfiguredScheduleItemDayWeek());
                    await context.SaveChangesAsync();
                }

                if (!context.ScheduleItemDay.Any())
                {
                    context.ScheduleItemDay.AddRange(GetPreconfiguredScheduleItemDay());
                    await context.SaveChangesAsync();
                }

                if (!context.WorkExperience.Any())
                {
                    context.WorkExperience.AddRange(GetPreconfiguredWorkExperience());
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvailability < 10)
                {
                    retryForAvailability++;
                    var log = loggerFactory.CreateLogger<CchContextSeed>();
                    log.LogError(ex.Message);
                    await SeedAsync(context, loggerFactory, retryForAvailability);
                }
            }
        }

        static IEnumerable<Clinic> GetPreconfiguredClinics()
        {
            return new List<Clinic>()
            {
                // Клиники первичного приема
                new Clinic() {
                    ID = 1,
                    NAME = "Городская поликлиника №218",
                    FULL_NAME = "Городская поликлиника №218 (Москва, Медведково, пр. Шокальского)",
                    ID_CLINIC_SPECIALIZATION = 1,
                    ADDRESS_DESCRIPTION = @"127642, г. Москва, проезд Шокальского, д. 8",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (499) 477 42 04",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Гончаров",
                    LEADER_I = "Алексей",
                    LEADER_O = "Владимирович",
                    PHONE_LEADER = "+7 (499) 790 32 68",
                    INN = "770000000056",
                    KPP = "5698746",
                    OGRN = "998855447",
                    OKTMO = "8885447885",
                    ADDRESS_SHORT = "Проезд Шокальского, д. 8",
                },
                new Clinic() {
                    ID = 2,
                    NAME = "Городская поликлиника №219",
                    FULL_NAME = "Городская поликлиника №219, Тушино, Куркино (Москва)",
                    ID_CLINIC_SPECIALIZATION = 1,
                    ADDRESS_DESCRIPTION = @"125480, г. Москва, бульвар Яна Райниса, д. 47",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (495) 948-20-65",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Сергеев",
                    LEADER_I = "Анатолий",
                    LEADER_O = "Иванович",
                    PHONE_LEADER = "+7 (495) 948-06-20",
                    INN = "775698544711",
                    KPP = "5698547",
                    OGRN = "9998887744",
                    OKTMO = "7788996655",
                    ADDRESS_SHORT = "бульвар Яна Райниса, д. 47",
                },
                new Clinic() {
                    ID = 3,
                    NAME = "Городская поликлиника №97",
                    FULL_NAME = "Городская поликлиника №97 Москва, Северное Тушино, ул. Лациса (Филиал №1 ГП №219)",
                    ID_CLINIC_SPECIALIZATION = 1,
                    ADDRESS_DESCRIPTION = @"125480, г. Москва, ул. Вилиса Лациса, д. 23, корп. 2",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (495) 494-45-78",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Афанасьева",
                    LEADER_I = "Ольга",
                    LEADER_O = "Вячеславовна",
                    PHONE_LEADER = "+7 (495) 494-45-99",
                    INN = "770000000015",
                    KPP = "5896874",
                    OGRN = "99966554122",
                    OKTMO = "7865951328",
                    ADDRESS_SHORT = "ул. Вилиса Лациса, д. 23, корп. 2",
                },
                // диагностические центры
                new Clinic() {
                    ID = 4,
                    NAME = "АКТИВМЕД",
                    FULL_NAME = "Международный клинический центр АКТИВМЕД на улице Сущёвский Вал",
                    ID_CLINIC_SPECIALIZATION = 2,
                    ADDRESS_DESCRIPTION = @"125480, г.  Москва, ул. Сущёвский Вал, 31с1",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (495) 494-45-78",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Гуревич",
                    LEADER_I = "Татьяна",
                    LEADER_O = "Вячеславовна",
                    PHONE_LEADER = "+7 (495) 494-45-99",
                    INN = "770000000123",
                    KPP = "5896874",
                    OGRN = "99966554122",
                    OKTMO = "7865951328",
                    ADDRESS_SHORT = "ул. Сущёвский Вал, 31с1",
                },
                // больницы
                new Clinic() {
                    ID = 5,
                    NAME = "Городская (первая градская) больница №1",
                    FULL_NAME = "Городская (первая градская) больница №1 им. Пирогова (Москва)",
                    ID_CLINIC_SPECIALIZATION = 3,
                    ADDRESS_DESCRIPTION = @"125125, г. Москва, Ленинский проспект, д. 8",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (495) 120-06-05",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Чебурков",
                    LEADER_I = "Геннадий",
                    LEADER_O = "Викторович",
                    PHONE_LEADER = "+7 (499) 764-50-02",
                    INN = "770000000234",
                    KPP = "5896874",
                    OGRN = "99966554122",
                    OKTMO = "7865951328",
                    ADDRESS_SHORT = "Ленинский проспект, д. 8",
                },
                // перинатальные центры
                new Clinic() {
                    ID = 6,
                    NAME = "Роддом №4",
                    FULL_NAME = "Роддом №4, филиал ГКБ им. Виноградова, Москва",
                    ID_CLINIC_SPECIALIZATION = 4,
                    ADDRESS_DESCRIPTION = @"119461, г. Москва,  ул. Новаторов, 3, стр. 1",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (495) 936-10-96",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Сергеев",
                    LEADER_I = "Павел",
                    LEADER_O = "Игоревич",
                    PHONE_LEADER = "+7 (495) 103-46-46",
                    INN = "770000000012",
                    KPP = "35698544",
                    OGRN = "88774411222",
                    OKTMO = "99555547745",
                    ADDRESS_SHORT = "ул. Новаторов, 3, стр. 1",
                },
                new Clinic() {
                    ID = 7,
                    NAME = "Роддом №25",
                    FULL_NAME = "Роддом №25, Москва (ГКБ №1 им. Пирогова)",
                    ID_CLINIC_SPECIALIZATION = 4,
                    ADDRESS_DESCRIPTION = @"119333, г. Москва, ул. Фотиевой, 6",
                    ID_ADDRESS = 0,
                    PHONE_REGISTRY = "+7 (499) 137-35-35",
                    SCHEDULE_DATETIME = new DateTime(2018, 04, 01, 1, 1, 1),
                    LEADER_F = "Агапова",
                    LEADER_I = "Екатерина",
                    LEADER_O = "Викторовна",
                    PHONE_LEADER = "+7 (499) 137-39-93",
                    INN = "770000000022",
                    KPP = "35698555",
                    OGRN = "8877711233",
                    OKTMO = "99555547756",
                    ADDRESS_SHORT = "ул. Фотиевой, 6",
                }
            };
        }

        static IEnumerable<ClinicSpecialization> GetPreconfiguredClinicSpecializations()
        {
            return new List<ClinicSpecialization>()
            {
                new ClinicSpecialization() { ID = 1, NAME = "КЛИНИКИ ПЕРВИЧНОГО ПРИЕМА" },
                new ClinicSpecialization() { ID = 2, NAME = "КЛИНИКО-ДИАГНОСТИЧЕСКИЕ ЦЕНТРЫ" },
                new ClinicSpecialization() { ID = 3, NAME = "БОЛЬНИЦЫ" },
                new ClinicSpecialization() { ID = 4, NAME = "ПЕРИНАТАЛЬНЫЕ ЦЕНТРЫ" }
            };
        }

        static IEnumerable<DoctorSpeciality> GetPreconfiguredDoctorSpecialities()
        {
            return new List<DoctorSpeciality>() //https://nsi.rosminzdrav.ru/#!/refbook/1.2.643.5.1.13.13.11.1002
            {
                //что такое SNOMED_CT?

                //1.1. Должности руководителей:
                //главный врач (начальник) медицинской организации;
                new DoctorSpeciality() { NAME = "Главный врач", SNOMED_CT = "" }, 
                //директор больницы (дома) сестринского ухода, хосписа;
                new DoctorSpeciality() { NAME = "Директор больницы", SNOMED_CT = "" },
                //заместитель руководителя (начальника) медицинской организации;
                new DoctorSpeciality() { NAME = "Заместитель руководителя", SNOMED_CT = "" },
                //заведующий (начальник) структурного подразделения (отдела, отделения, лаборатории, кабинета, отряда и другое) медицинской организации - врач-специалист;
                new DoctorSpeciality() { NAME = "Заведующий", SNOMED_CT = "" },
                //заведующий (главный врач, начальник) структурного подразделения, осуществляющего медицинскую деятельность, иной организации;
                //new DoctorSpeciality() { NAME = "Заведующий", SNOMED_CT = "" },
                //главная медицинская сестра (главная акушерка, главный фельдшер).
                new DoctorSpeciality() { NAME = "Главная медицинская сестра", SNOMED_CT = "" },


                //1.2. Должности специалистов с высшим профессиональным (медицинским) образованием (врачи):
                //а) врачи-специалисты, в том числе:
                new DoctorSpeciality() { NAME = "Врач-акушер-гинеколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-акушер-гинеколог цехового врачебного участка", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-аллерголог-иммунолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-анестезиолог-реаниматолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-бактериолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-вирусолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-гастроэнтеролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-гематолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-генетик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-гериатр", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-дезинфектолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-дерматовенеролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-детский кардиолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-детский онколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-детский уролог-андролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-детский хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-детский эндокринолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-диабетолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-диетолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач здравпункта", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-инфекционист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-кардиолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач клинической лабораторной диагностики", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-клинический миколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-клинический фармаколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-колопроктолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-косметолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-лаборант;", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-лабораторный генетик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-лабораторный миколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач мануальной терапии", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-методист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-невролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-нейрохирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-неонатолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-нефролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач общей практики (семейный врач)", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-онколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-ортодонт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-остеопат", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-оториноларинголог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-офтальмолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-офтальмолог-протезист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-паразитолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-патологоанатом", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-педиатр", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-педиатр городской (районный)", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-педиатр участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-пластический хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по авиационной и космической медицине", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по водолазной медицине", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по гигиене детей и подростков", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по гигиене питания", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по гигиене труда", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по гигиеническому воспитанию", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по коммунальной гигиене", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по лечебной физкультуре", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по медико-социальной экспертизе", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по медицинской профилактике", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по медицинской реабилитации", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по общей гигиене", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по паллиативной медицинской помощи", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по радиационной гигиене", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по рентгенэндоваскулярным диагностике и лечению", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по санитарно-гигиеническим лабораторным исследованиям", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач по спортивной медицине", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач приемного отделения", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-профпатолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр детский", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр детский участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр подростковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр подростковый участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр-нарколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психиатр-нарколог участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-психотерапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-пульмонолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-радиолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-радиотерапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-ревматолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-рентгенолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-рефлексотерапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-сексолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-сердечно-сосудистый хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач скорой медицинской помощи", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-статистик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-стоматолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-стоматолог детский", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-стоматолог-ортопед", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-стоматолог-терапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-стоматолог-хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-судебно-медицинский эксперт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-судебно-психиатрический эксперт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-сурдолог-оториноларинголог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-сурдолог-протезист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-терапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-терапевт подростковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-терапевт участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-терапевт участковый цехового врачебного участка", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-токсиколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-торакальный хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-травматолог-ортопед", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-трансфузиолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач ультразвуковой диагностики", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-уролог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-физиотерапевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-фтизиатр", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-фтизиатр участковый", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач функциональной диагностики", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-челюстно-лицевой хирург", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-эндокринолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-эндоскопист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Врач-эпидемиолог", SNOMED_CT = "" },
                //старший врач станции (отделения) скорой медицинской помощи;
                new DoctorSpeciality() { NAME = "Старший врач станции ", SNOMED_CT = "" },
                //старший врач станции (отделения) скорой медицинской помощи горноспасательных частей;
                //new DoctorSpeciality() { NAME = "Судовой врач", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Судовой врач", SNOMED_CT = "" },

                //б) врач-стажер.
                new DoctorSpeciality() { NAME = "Врач-стажер", SNOMED_CT = "" },


                //1.3. Должности специалистов с высшим профессиональным (немедицинским) образованием:
                new DoctorSpeciality() { NAME = "Биолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Зоолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Инструктор-методист по лечебной физкультуре", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский психолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский физик", SNOMED_CT = "" },
                //судебный эксперт (эксперт-биохимик, эксперт-генетик, эксперт-химик);
                new DoctorSpeciality() { NAME = "Судебный эксперт-биохимик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Судебный эксперт-генетик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Судебный эксперт-химик", SNOMED_CT = "" },

                new DoctorSpeciality() { NAME = "Химик-эксперт медицинской организации", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Эксперт-физик по контролю за источниками ионизирующих и неионизирующих излучений", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Эмбриолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Энтомолог", SNOMED_CT = "" },


                //1.4. Должности специалистов со средним профессиональным (медицинским) образованием (средний медицинский персонал):
                new DoctorSpeciality() { NAME = "Акушер", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Гигиенист стоматологический", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий молочной кухней", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий здравпунктом - фельдшер (медицинская сестра)", SNOMED_CT = "" },
                //заведующий фельдшерско-акушерским пунктом - фельдшер (акушер, медицинская сестра);
                new DoctorSpeciality() { NAME = "Заведующий фельдшерско-акушерским пунктом", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий кабинетом медицинской профилактики - фельдшер (медицинская сестра)", SNOMED_CT = "" },
                //заведующий производством учреждений (отделов, отделений, лабораторий) зубопротезирования;
                new DoctorSpeciality() { NAME = "Заведующий производством учреждений", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Зубной врач", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Зубной техник", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Инструктор-дезинфектор", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Инструктор по гигиеническому воспитанию", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Инструктор по лечебной физкультуре", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Инструктор по трудовой терапии", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Лаборант", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра-анестезист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра врача общей практики (семейного врача)", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра диетическая", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра медико-социальной помощи", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра палатная (постовая)", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра патронажная", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра перевязочной", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра по косметологии", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра по массажу", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра (фельдшер) по приему вызовов скорой медицинской помощи и передаче их выездным бригадам скорой медицинской помощи", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра приемного отделения", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра процедурной", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра по реабилитации", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра стерилизационной", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра участковая", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинская сестра по физиотерапии", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский дезинфектор", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский лабораторный техник (фельдшер-лаборант)", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский оптик-оптометрист", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский регистратор", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский статистик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Медицинский технолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "операционная медицинская сестра", SNOMED_CT = "" },
                //помощник: врача-эпидемиолога, врача-паразитолога, врача по гигиене детей и подростков, врача по гигиене питания, врача по гигиене труда, врача по гигиеническому воспитанию, врача по коммунальной гигиене, врача по общей гигиене, врача по радиационной гигиене;
                new DoctorSpeciality() { NAME = "Помощник", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Помощник энтомолога", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Рентгенолаборант", SNOMED_CT = "" },
                //старшая медицинская сестра (акушер, фельдшер, операционная медицинская сестра, зубной техник);
                new DoctorSpeciality() { NAME = "Старшая медицинская сестра", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Фельдшер", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Фельдшер скорой медицинской помощи", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Фельдшер-нарколог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Фельдшер-водитель скорой медицинской помощи", SNOMED_CT = "" },

                //1.5. Иные должности медицинских работников (младший медицинский персонал):
                new DoctorSpeciality() { NAME = "Младшая медицинская сестра по уходу за больными", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Санитар", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Санитар-водитель", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Сестра-хозяйка", SNOMED_CT = "" },

                //II. Фармацевтические работники
                //2.1. Должности руководителей:
                //директор (заведующий, начальник) аптечной организации;
                new DoctorSpeciality() { NAME = "Директор", SNOMED_CT = "" },
                //заместитель директора (заведующего, начальника) аптечной организации;
                new DoctorSpeciality() { NAME = "Заместитель директора", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий складом организации оптовой торговли лекарственными средствами", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий медицинским складом мобилизационного резерва", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заместитель заведующего складом организации оптовой торговли лекарственными средствами", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Заведующий (начальник) структурного подразделения (отдела) аптечной организации", SNOMED_CT = "" },
                
                //2.2. Должности специалистов с высшим профессиональным (фармацевтическим) образованием (провизоры):
                new DoctorSpeciality() { NAME = "Провизор", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Провизор-аналитик", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Провизор-стажер", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Провизор-технолог", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Старший провизор", SNOMED_CT = "" },

                //2.3. Должности специалистов со средним профессиональным (фармацевтическим) образованием (средний фармацевтический персонал):
                new DoctorSpeciality() { NAME = "Младший фармацевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Старший фармацевт", SNOMED_CT = "" },
                new DoctorSpeciality() { NAME = "Фармацевт", SNOMED_CT = "" },

                //2.4. Иные должности фармацевтических работников (младший фармацевтический персонал):
                new DoctorSpeciality() { NAME = "Фасовщик", SNOMED_CT = "" },
            };
        }

        static IEnumerable<Doctor> GetPreconfiguredDoctor()
        {
            return new List<Doctor>()
            {
                new Doctor()
                {
                    ID = 1,
                    DOCTOR_F = "Лебедева",
                    DOCTOR_I = "Инна",
                    DOCTOR_O = "Сергеевна",
                    ID_DOCTOR_SPECIALITY = 22,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 2,
                    DOCTOR_F = "Быков",
                    DOCTOR_I = "Сергей",
                    DOCTOR_O = "Анатольевич",
                    ID_DOCTOR_SPECIALITY = 22,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 3,
                    DOCTOR_F = "Пискун",
                    DOCTOR_I = "Анна",
                    DOCTOR_O = "Михайловна",
                    ID_DOCTOR_SPECIALITY = 22,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 4,
                    DOCTOR_F = "Симонова",
                    DOCTOR_I = "Альбина",
                    DOCTOR_O = "Валерьевна",
                    ID_DOCTOR_SPECIALITY = 22,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                // гинеколог
                new Doctor()
                {
                    ID = 5,
                    DOCTOR_F = "Тихомиров",
                    DOCTOR_I = "Александр",
                    DOCTOR_O = "Леонидович",
                    ID_DOCTOR_SPECIALITY = 33,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 6,
                    DOCTOR_F = "Киладзе",
                    DOCTOR_I = "Лиана",
                    DOCTOR_O = "Галактионовна",
                    ID_DOCTOR_SPECIALITY = 33,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 7,
                    DOCTOR_F = "Хангельдова",
                    DOCTOR_I = "Карина",
                    DOCTOR_O = "Григорьевна",
                    ID_DOCTOR_SPECIALITY = 33,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                //  иммунолог
                new Doctor()
                {
                    ID = 8,
                    DOCTOR_F = "Баранова",
                    DOCTOR_I = "Ирина",
                    DOCTOR_O = "Дмитриевна",
                    ID_DOCTOR_SPECIALITY = 40,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 9,
                    DOCTOR_F = "Кузнецова",
                    DOCTOR_I = "Светлана",
                    DOCTOR_O = "Владимировна",
                    ID_DOCTOR_SPECIALITY = 40,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 10,
                    DOCTOR_F = "Суркичин",
                    DOCTOR_I = "Сергей",
                    DOCTOR_O = "Иванович",
                    ID_DOCTOR_SPECIALITY = 55,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 11,
                    DOCTOR_F = "Черноморов",
                    DOCTOR_I = "Владимир",
                    DOCTOR_O = "Евгеньевич",
                    ID_DOCTOR_SPECIALITY = 55,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },

                new Doctor()
                {
                    ID = 12,
                    DOCTOR_F = "Пушкин",
                    DOCTOR_I = "Михаил",
                    DOCTOR_O = "Валерьевич",
                    ID_DOCTOR_SPECIALITY = 33,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 13,
                    DOCTOR_F = "Тодин",
                    DOCTOR_I = "Дмитрий",
                    DOCTOR_O = "Егорович",
                    ID_DOCTOR_SPECIALITY = 22,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 14,
                    DOCTOR_F = "Зубрин",
                    DOCTOR_I = "Генадий",
                    DOCTOR_O = "Дмитриевич",
                    ID_DOCTOR_SPECIALITY = 40,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                },
                new Doctor()
                {
                    ID = 15,
                    DOCTOR_F = "Осипов",
                    DOCTOR_I = "Евгений",
                    DOCTOR_O = "Александрович",
                    ID_DOCTOR_SPECIALITY = 55,
                    ID_QUALIFICATION_CATEGORY = 0,
                    ID_POSITION = 0,
                }
            };
        }

        static IEnumerable<WorkExperience> GetPreconfiguredWorkExperience()
        {
            return new List<WorkExperience>()
            {
                new WorkExperience()
                {
                    ID_DOCTOR = 1,
                    PLACE = "Томс, Городская поликлиника №2",
                    POSITION = "Фельдшер",
                    FROM = new DateTime(1998, 5, 1),
                    TO = new DateTime(2001, 1, 23)
                },
                new WorkExperience()
                {
                    ID_DOCTOR = 1,
                    PLACE = "Москва, Городская поликлиника №5",
                    POSITION = "Медицинский психолог",
                    FROM = new DateTime(2001, 2, 1),
                    TO = new DateTime(2004, 11, 12)
                },
                new WorkExperience()
                {
                    ID_DOCTOR = 1,
                    PLACE = "Международный клинический центр АКТИВМЕД на улице Сущёвский Вал",
                    POSITION = "Судебный эксперт-генетик",
                    FROM = new DateTime(2004, 12, 1),
                    TO = new DateTime(2005, 1, 1)
                },
                new WorkExperience()
                {
                    ID_DOCTOR = 1,
                    PLACE = "Роддом №4, филиал ГКБ им. Виноградова, Москва",
                    POSITION = "Врач-методист",
                    FROM = new DateTime(2005, 1, 1),
                    TO = new DateTime(2017, 5, 25)
                },
                new WorkExperience()
                {
                    ID_DOCTOR = 2,
                    PLACE = "Москва, Городская поликлиника №244",
                    POSITION = "Врач-паразитолог",
                    FROM = new DateTime(2002, 2, 1),
                    TO = new DateTime(2012, 7, 5)
                },
            };
        }

        static IEnumerable<ClinicDoctor> GetPreconfiguredClinicDoctor()
        {
            return new List<ClinicDoctor>()
            {
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 1, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 2, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 3, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 4, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 5, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 1, ID_DOCTOR = 6, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 2, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 3, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 4, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 5, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 6, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 7, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 8, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 9, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 2, ID_DOCTOR = 10, ID_POSITION = 0, PERSONNEL_NUMBER = "" },

                new ClinicDoctor() { ID_CLINIC = 3, ID_DOCTOR = 11, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 4, ID_DOCTOR = 12, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 5, ID_DOCTOR = 13, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 6, ID_DOCTOR = 14, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
                new ClinicDoctor() { ID_CLINIC = 7, ID_DOCTOR = 15, ID_POSITION = 0, PERSONNEL_NUMBER = "" },
            };
        }

        static IEnumerable<ScheduleItemDayWeek> GetPreconfiguredScheduleItemDayWeek()
        {
            return new List<ScheduleItemDayWeek>()
            {
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 30, CABINET = "404", STOREY = 4, DAY = DayOfWeek.Monday, FROM = new DateTime(1, 1, 1, 10, 0, 0), TO = new DateTime(1, 1, 1, 15, 30, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 30, CABINET = "404", STOREY = 4, DAY = DayOfWeek.Monday, FROM = new DateTime(1, 1, 1, 17, 0, 0), TO = new DateTime(1, 1, 1, 20, 30, 0)},

                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 30, CABINET = "301", STOREY = 3, DAY = DayOfWeek.Tuesday, FROM = new DateTime(1, 1, 1, 11, 0, 0), TO = new DateTime(1, 1, 1, 17, 0, 0)},

                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 40, CABINET = "555", STOREY = 5, DAY = DayOfWeek.Wednesday, FROM = new DateTime(1, 1, 1, 12, 0, 0), TO = new DateTime(1, 1, 1, 19, 0, 0)},

                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 30, CABINET = "404", STOREY = 4, DAY = DayOfWeek.Friday, FROM = new DateTime(1, 1, 1, 14, 0, 0), TO = new DateTime(1, 1, 1, 20, 30, 0)},

                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 2, DURATION = 10, CABINET = "102", STOREY = 1, DAY = DayOfWeek.Monday, FROM = new DateTime(1, 1, 1, 6, 0, 0), TO = new DateTime(1, 1, 1, 8, 0, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 2, DURATION = 10, CABINET = "105", STOREY = 1, DAY = DayOfWeek.Tuesday, FROM = new DateTime(1, 1, 1, 7, 0, 0), TO = new DateTime(1, 1, 1, 8, 0, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 2, DURATION = 30, CABINET = "457", STOREY = 4, DAY = DayOfWeek.Wednesday, FROM = new DateTime(1, 1, 1, 12, 0, 0), TO = new DateTime(1, 1, 1, 12, 30, 0)},

                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 5, DURATION = 10, CABINET = "102", STOREY = 1, DAY = DayOfWeek.Monday, FROM = new DateTime(1, 1, 1, 6, 0, 0), TO = new DateTime(1, 1, 1, 8, 0, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 5, DURATION = 10, CABINET = "105", STOREY = 1, DAY = DayOfWeek.Tuesday, FROM = new DateTime(1, 1, 1, 7, 0, 0), TO = new DateTime(1, 1, 1, 8, 0, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 1, ID_DOCTOR = 5, DURATION = 30, CABINET = "457", STOREY = 4, DAY = DayOfWeek.Wednesday, FROM = new DateTime(1, 1, 1, 12, 0, 0), TO = new DateTime(1, 1, 1, 12, 30, 0)},
                new ScheduleItemDayWeek() { ID_CLINIC = 2, ID_DOCTOR = 5, DURATION = 10, CABINET = "303", STOREY = 3, DAY = DayOfWeek.Tuesday, FROM = new DateTime(1, 1, 1, 10, 0, 0), TO = new DateTime(1, 1, 1, 11, 0, 0)},
            };
        }

        static IEnumerable<ScheduleItemDay> GetPreconfiguredScheduleItemDay()
        {
            return new List<ScheduleItemDay>()
            {
                new ScheduleItemDay() { ID_CLINIC = 1, ID_DOCTOR = 1, DURATION = 20, CABINET = "404", STOREY = 4, DATE = new DateTime(2018, 5, 22), FROM = new DateTime(1, 1, 1, 17, 0, 0), TO = new DateTime(1, 1, 1, 19, 30, 0)},
            };
        }
    }
}