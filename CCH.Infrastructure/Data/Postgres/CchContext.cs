﻿using System.Linq;
using CCH.Common.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CCH.Infrastructure.Data.Postgres
{
    public class CchContext: DbContext
    {
        public readonly string ConnectionString;

        public CchContext(DbContextOptions<CchContext> postgresContextOptions) : base(postgresContextOptions)
        {
            var extensions = postgresContextOptions.Extensions;
            var npgsqlOptions = from x in extensions where x is NpgsqlOptionsExtension select (NpgsqlOptionsExtension)x;
            ConnectionString = (from x in npgsqlOptions where !string.IsNullOrEmpty(x.ConnectionString) select x.ConnectionString).First();
        }

        public DbSet<Clinic> Clinics { get; private set; }

        public DbSet<ClinicSpecialization> ClinicSpecializations { get; private set; }

        public DbSet<Doctor> Doctor { get; private set; }

        public DbSet<ClinicDoctor> ClinicDoctor { get; private set; }

        public DbSet<DoctorSpeciality> DoctorSpeciality { get; private set; }

        public DbSet<QualificationCategory> QualificationCategory { get; private set; }

        

        public DbSet<ScheduleItem> ScheduleItem{ get; private set; }

        public DbSet<ScheduleItemDay> ScheduleItemDay { get; private set; }

        public DbSet<ScheduleItemDayWeek> ScheduleItemDayWeek { get; private set; }

        public DbSet<Appointment> Appointment { get; private set; }

        public DbSet<Reminder> Reminder { get; private set; }

        public DbSet<WorkExperience> WorkExperience { get; private set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // set connection options from config
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Clinic>(ConfigureClinic);
            modelBuilder.Entity<ClinicSpecialization>(ConfigureClinicSpecialization);
            modelBuilder.Entity<Doctor>(ConfigureDoctor);
            modelBuilder.Entity<DoctorSpeciality>(ConfigureDoctorSpeciality);

            modelBuilder.Entity<ScheduleItem>(ConfigureScheduleItem);
            modelBuilder.Entity<ScheduleItemDay>(ConfigureScheduleItemDay);
            modelBuilder.Entity<ScheduleItemDayWeek>(ConfigureScheduleItemDayWeek);
            modelBuilder.Entity<Appointment>(ConfigureAppointment);
            modelBuilder.Entity<Reminder>(ConfigureReminder);
            modelBuilder.Entity<WorkExperience>(ConfigureWorkExperience);
        }

        private void ConfigureClinic(EntityTypeBuilder<Clinic> builder)
        {
            builder.ToTable("CLINIC");

            builder.HasKey(c => new { c.ID });

            // TODO: foreign key Clinic -> ClinicSpecialization

            builder.Property(c => c.ID)
                .ForNpgsqlUseSequenceHiLo("clinic_hilo")
                .HasColumnName("ID")
                .IsRequired();

            #region Основное

            builder.Property(c => c.NAME)
                .IsRequired(true)
                .HasColumnName("NAME")
                .HasMaxLength(510);

            builder.Property(c => c.FULL_NAME)
                .IsRequired(true)
                .HasColumnName("FULL_NAME")
                .HasMaxLength(510);

            builder.Property(c => c.ID_CLINIC_SPECIALIZATION)
                .IsRequired(true)
                .HasColumnName("ID_CLINIC_SPECIALIZATION");

            builder.Property(c => c.ADDRESS_DESCRIPTION)
                .IsRequired(false)
                .HasColumnName("ADDRESS_DESCRIPTION")
                .HasMaxLength(510);

            builder.Property(c => c.ADDRESS_SHORT)
                .IsRequired(true)
                .HasColumnName("ADDRESS_SHORT")
                .HasMaxLength(510);

            builder.Property(c => c.ID_ADDRESS)
                 .IsRequired(true)
                 .HasColumnName("ID_ADDRESS");

            builder.Property(c => c.PHONE_REGISTRY)
                .IsRequired(true)
                .HasColumnName("PHONE_REGISTRY")
                .HasMaxLength(510);
            
            builder.Property(c => c.SCHEDULE_DATETIME)
                .IsRequired(true)
                .HasColumnName("SCHEDULE_DATETIME")
                .HasMaxLength(510);

            builder.Property(c => c.LEADER_F)
                .IsRequired(false)
                .HasColumnName("LEADER_F")
                .HasMaxLength(50);

            builder.Property(c => c.LEADER_I)
                .IsRequired(false)
                .HasColumnName("LEADER_I")
                .HasMaxLength(50);

            builder.Property(c => c.LEADER_O)
                .IsRequired(false)
                .HasColumnName("LEADER_O")
                .HasMaxLength(50);

            builder.Property(c => c.PHONE_LEADER)
                .IsRequired(false)
                .HasColumnName("PHONE_LEADER")
                .HasMaxLength(510);

            #endregion

            #region Реквизиты клиники

            builder.Property(c => c.INN)
                .IsRequired(true)
                .HasColumnName("INN")
                .HasMaxLength(12);

            builder.Property(c => c.KPP)
                .IsRequired(false)
                .HasColumnName("KPP")
                .HasMaxLength(12);

            builder.Property(c => c.OGRN)
                .IsRequired(false)
                .HasColumnName("OGRN")
                .HasMaxLength(15);

            builder.Property(c => c.OKTMO)
                .IsRequired(false)
                .HasColumnName("OKTMO")
                .HasMaxLength(15);

            #endregion
        }

        private void ConfigureClinicSpecialization(EntityTypeBuilder<ClinicSpecialization> builder)
        {
            builder.ToTable("CLINIC_SPECIALIZATION");

            builder.HasKey(cs => new { cs.ID });

            builder.Property(cs => cs.ID)
                .ForNpgsqlUseSequenceHiLo("clinic_specialization_hilo")
                .HasColumnName("ID")
                .IsRequired();

            builder.Property(cs => cs.NAME)
                .IsRequired(true)
                .HasColumnName("NAME")
                .HasMaxLength(510);
        }

        private void ConfigureDoctor(EntityTypeBuilder<Doctor> builder)
        {
            builder.ToTable("DOCTOR");

            builder.HasKey(cs => new { cs.ID });

            builder.Property(cs => cs.ID)
                .ForNpgsqlUseSequenceHiLo("doctor_hilo")
                .HasColumnName("ID")
                .IsRequired();

            builder.Property(cs => cs.DOCTOR_F)
                .IsRequired(true)
                .HasColumnName("DOCTOR_F")
                .HasMaxLength(50);

            builder.Property(cs => cs.DOCTOR_I)
                .IsRequired(true)
                .HasColumnName("DOCTOR_I")
                .HasMaxLength(50);

            builder.Property(cs => cs.DOCTOR_O)
                .IsRequired(true)
                .HasColumnName("DOCTOR_O")
                .HasMaxLength(50);

            builder.Property(c => c.ID_DOCTOR_SPECIALITY)
                .IsRequired(true)
                .HasColumnName("ID_DOCTOR_SPECIALITY");

            builder.Property(c => c.ID_POSITION)
                .IsRequired(true)
                .HasColumnName("ID_POSITION");

            builder.Property(c => c.ID_QUALIFICATION_CATEGORY)
                .IsRequired(true)
                .HasColumnName("ID_QUALIFICATION_CATEGORY");

            builder.Property(cs => cs.EXPERIENCE)
                .IsRequired(true)
                .HasColumnName("EXPERIENCE")
                .HasMaxLength(50);

            builder.HasOne(cs => cs.DOCTOR_SPECIALITY)
                .WithMany()
                .HasForeignKey(cs => cs.ID_DOCTOR_SPECIALITY);

            builder.HasMany(cs => cs.WORK_EXPERIENCES)
                .WithOne()
                .HasForeignKey(x => x.ID_DOCTOR)
                .HasPrincipalKey(cs => cs.ID);
        }

        private void ConfigureDoctorSpeciality(EntityTypeBuilder<DoctorSpeciality> builder)
        {
            builder.ToTable("DOCTOR_SPECIALITY");

            builder.HasKey(cs => new { cs.ID });

            builder.Property(cs => cs.ID)
                .ForNpgsqlUseSequenceHiLo("doctor_speciality_hilo")
                .HasColumnName("ID")
                .IsRequired();

            builder.Property(cs => cs.NAME)
                .IsRequired(true)
                .HasColumnName("NAME")
                .HasMaxLength(510);

            builder.Property(cs => cs.SNOMED_CT)
                .IsRequired(true)
                .HasColumnName("SNOMED_CT")
                .HasMaxLength(510);

            builder.Property(cs => cs.IS_RECORD_AVAILABLE_PRIMARY_RECEPTION)
                .HasDefaultValue(true);
        }

        private void ConfigureScheduleItem(EntityTypeBuilder<ScheduleItem> builder)
        {
            builder.HasKey(cs => new { cs.ID });

            builder.Property(cs => cs.FROM)
                .IsRequired(true);

            builder.Property(cs => cs.TO)
                .IsRequired(true);

            builder.Property(cs => cs.ID_DOCTOR)
                .IsRequired(true);

            builder.Property(cs => cs.ID_CLINIC)
                .IsRequired(true);

            builder.Property(cs => cs.DURATION)
                .IsRequired(true);

            builder.Property(cs => cs.CABINET)
                .HasMaxLength(32);

            builder.HasOne(cs => cs.DOCTOR)
                .WithMany()
                .HasForeignKey(cs => cs.ID_DOCTOR);

            builder.HasOne(cs => cs.CLINIC)
                .WithMany()
                .HasForeignKey(cs => cs.ID_CLINIC);

        }

        private void ConfigureScheduleItemDay(EntityTypeBuilder<ScheduleItemDay> builder)
        {
            builder.ToTable("SCHEDULE_ITEM_DAY");

            builder.HasBaseType<ScheduleItem>();

            builder.Property(cs => cs.DATE)
                .IsRequired(true);
        }

        private void ConfigureScheduleItemDayWeek(EntityTypeBuilder<ScheduleItemDayWeek> builder)
        {
            builder.ToTable("SCHEDULE_ITEM_DAY_WEEK");

            builder.HasBaseType<ScheduleItem>();

            builder.Property(cs => cs.DAY)
                .IsRequired(true);
        }

        private void ConfigureAppointment(EntityTypeBuilder<Appointment> builder)
        {
            builder.ToTable("APPOINTMENT");

            builder.HasBaseType<ScheduleItem>();

            builder.Property(cs => cs.ID_PATIENT)
                .IsRequired(true);

            builder.Property(cs => cs.STATUS)
                .IsRequired(true);

            builder.Property(cs => cs.PHONE_PATIENT)
                .IsRequired(true);
        }

        private void ConfigureReminder(EntityTypeBuilder<Reminder> builder)
        {
            builder.ToTable("REMINDER");

            builder.Property(cs => cs.Time)
                .HasMaxLength(5);

            builder.Property(cs => cs.Frequency)
                .HasDefaultValue(ReminderFrequencyEnum.PER_DAY);

            builder.Property(cs => cs.IsOn)
                .HasDefaultValue(false);

            builder.Property(cs => cs.IsOnMethodCall)
                .HasDefaultValue(false);

            builder.Property(cs => cs.IsOnMethodEmail)
                .HasDefaultValue(false);

            builder.Property(cs => cs.IsOnMethodPush)
                .HasDefaultValue(false);

            builder.Property(cs => cs.IsOnMethodSms)
                .HasDefaultValue(false);
        }

        private void ConfigureWorkExperience(EntityTypeBuilder<WorkExperience> builder)
        {
            builder.ToTable("WORK_EXPERIENCE");

            builder.Property(x => x.ID_DOCTOR)
                .IsRequired(true);

            builder.Property(x => x.FROM)
                .HasColumnType("Date")
                .IsRequired(true);

            builder.Property(x => x.TO)
                .HasColumnType("Date")
                .IsRequired(true);

            builder.Property(x => x.PLACE)
                .HasMaxLength(256)
                .IsRequired(true);

            builder.Property(x => x.POSITION)
                .HasMaxLength(256)
                .IsRequired(true);
        }
    }
}