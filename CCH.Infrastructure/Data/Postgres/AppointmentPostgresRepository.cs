﻿using CCH.Common.Entities;
using CCH.Common.Entities.Form;
using CCH.Common.Exceptions;
using CCH.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CCH.Infrastructure.Data.Postgres
{
    public class AppointmentPostgresRepository : PostgresRepository<Appointment, long>, IAppointmentRepository
    {
        public AppointmentPostgresRepository(CchContext context) : base(context)
        {
        }

        public IEnumerable<Appointment> Get(DateTime date, long idDoctor, long idClinic, bool isOnlyActiveStatus)
        {
            return _context.Appointment
                .Where(x => !isOnlyActiveStatus || x.IsActiveStatus)
                .Where(x => x.ID_DOCTOR == idDoctor && x.ID_CLINIC == idClinic && x.FROM.Date == date.Date);
        }

        public IEnumerable<Appointment> GetByIdUser(string idPatient, bool isOnlyActiveStatus, bool isOnlyNotPassed)
        {
            return _context.Appointment
                .Include(x => x.DOCTOR)
                .Include(x => x.DOCTOR.DOCTOR_SPECIALITY)
                .Include(x => x.CLINIC)
                .Where(x => x.ID_PATIENT == idPatient)
                .Where(x => !isOnlyActiveStatus || x.IsActiveStatus)
                .Where(x => !isOnlyNotPassed || x.FROM > DateTime.Now)
                .ToArray();
        }

        public Boolean Cancel(long id, string idPatient)
        {
            var appointment = GetById(id);

            if (appointment == null)
            {
                return false;
            }

            if(appointment.ID_PATIENT != idPatient)
            {
                throw new MethodAccessException("У вас нет прав доступа");
            }

            appointment.STATUS = StatusAppointmentEnum.CANCELED;

            _context.Appointment.Update(appointment);
            _context.SaveChanges();

            return true;
        }

        public Appointment Add(string idPatient, FormAppointment form)
        {
            ScheduleItem scheduleItem = _context
                .ScheduleItem
                .Include(x => x.DOCTOR)
                .FirstOrDefault(x => x.ID == form.IdSchedule);

            if (scheduleItem == null)
            {
                throw new AppException("Выбранное время на приём не найдено!");
            }

            var appointmentsThisDate = Get(form.Datetime, scheduleItem.ID_DOCTOR, scheduleItem.ID_CLINIC, false).ToList();

            //Удаляем все записи пациента на текущий день к данному врачу в данную клинику
            appointmentsThisDate
                .Where(x => x.ID_PATIENT == idPatient)
                .ToList()
                .ForEach(x => Delete(x));

            //Удаляем все записи пациента на текущую специальность со статусом актвен и ожидает регистрации
            _context.Appointment
                .Include(x=> x.DOCTOR)
                .Where(x => x.ID_PATIENT == idPatient && 
                    x.IsActiveStatus && 
                    x.FROM > DateTime.Now && 
                    x.DOCTOR.ID_DOCTOR_SPECIALITY == scheduleItem.DOCTOR.ID_DOCTOR_SPECIALITY)
                .ToList()
                .ForEach(x => Delete(x));

            //удаляем все записи пациента на текущее время
            _context.Appointment
                .Where(x => x.ID_PATIENT == idPatient && CheckTimePeriod(x.FROM, x.DURATION, form.Datetime, scheduleItem.DURATION))
                .ToList()
                .ForEach(x => Delete(x));

            //Добавляем новую запись
            Appointment appointment = new Appointment();
            appointment.CABINET = scheduleItem.CABINET;
            appointment.DURATION = scheduleItem.DURATION;
            appointment.ID_CLINIC = scheduleItem.ID_CLINIC;
            appointment.ID_DOCTOR = scheduleItem.ID_DOCTOR;
            appointment.STOREY = scheduleItem.STOREY;
            appointment.ID_PATIENT = idPatient;
            appointment.FROM = form.Datetime;
            appointment.PHONE_PATIENT = form.Phone;

            var doctor = _context.Doctor.FirstOrDefault(x => x.ID == scheduleItem.ID_DOCTOR);
            var doctorSpeciality = _context.DoctorSpeciality.FirstOrDefault(x => x.ID == doctor.ID_DOCTOR_SPECIALITY);
            var clinic = _context.Clinics.FirstOrDefault(x => x.ID == scheduleItem.ID_CLINIC);

            appointment.TITLE = String.Format("{0} {1}.{2}.; {3}; {4}", doctor?.DOCTOR_F, doctor?.DOCTOR_I[0], doctor?.DOCTOR_O[0], doctorSpeciality?.NAME, clinic?.NAME);

            Add(appointment);

            return appointment;
        }

        private bool CheckTimePeriod(DateTime from, long duration, DateTime from2, long duration2)
        {
            DateTime to = from.AddMinutes(duration);
            DateTime to2 = from2.AddMinutes(duration2);

            return from <= from2 && to > from2 ||
                from < to2 && to >= to2;
        }
    }
}