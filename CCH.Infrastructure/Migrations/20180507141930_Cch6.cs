﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "doctor_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "doctor_speciality_hilo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ClinicDoctor",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ID_CLINIC = table.Column<long>(nullable: false),
                    ID_DOCTOR = table.Column<long>(nullable: false),
                    ID_POSITION = table.Column<long>(nullable: false),
                    PERSONNEL_NUMBER = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClinicDoctor", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DOCTOR",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    DOCTOR_F = table.Column<string>(maxLength: 50, nullable: false),
                    DOCTOR_I = table.Column<string>(maxLength: 50, nullable: false),
                    DOCTOR_O = table.Column<string>(maxLength: 50, nullable: false),
                    EXPERIENCE = table.Column<string>(maxLength: 50, nullable: false),
                    ID_DOCTOR_SPECIALITY = table.Column<long>(nullable: false),
                    ID_QUALIFICATION_CATEGORY = table.Column<long>(nullable: false),
                    POSITION = table.Column<long>(nullable: false),
                    SCHEDULE = table.Column<DateTime>(maxLength: 510, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DOCTOR", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DOCTOR_SPECIALITY",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    NAME = table.Column<string>(maxLength: 510, nullable: false),
                    SNOMED_CT = table.Column<string>(maxLength: 510, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DOCTOR_SPECIALITY", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "QualificationCategory",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    NAME = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QualificationCategory", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClinicDoctor");

            migrationBuilder.DropTable(
                name: "DOCTOR");

            migrationBuilder.DropTable(
                name: "DOCTOR_SPECIALITY");

            migrationBuilder.DropTable(
                name: "QualificationCategory");

            migrationBuilder.DropSequence(
                name: "doctor_hilo");

            migrationBuilder.DropSequence(
                name: "doctor_speciality_hilo");
        }
    }
}
