﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "clinic_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "clinic_specialization_hilo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "CLINIC",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    ADDRESS_DESCRIPTION = table.Column<string>(maxLength: 510, nullable: true),
                    FULL_NAME = table.Column<string>(nullable: true),
                    ID_ADDRESS = table.Column<long>(nullable: false),
                    ID_CLINIC_SPECIALIZATION = table.Column<long>(nullable: false),
                    LEADER_F = table.Column<string>(maxLength: 50, nullable: true),
                    LEADER_I = table.Column<string>(maxLength: 50, nullable: true),
                    LEADER_O = table.Column<string>(maxLength: 50, nullable: true),
                    NAME = table.Column<string>(maxLength: 510, nullable: false),
                    PHONE_LEADER = table.Column<string>(maxLength: 510, nullable: true),
                    PHONE_REGISTRY = table.Column<string>(maxLength: 510, nullable: false),
                    SCHEDULE_DATETIME = table.Column<DateTime>(maxLength: 510, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLINIC", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CLINIC_SPECIALIZATION",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    NAME = table.Column<string>(maxLength: 510, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLINIC_SPECIALIZATION", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CLINIC");

            migrationBuilder.DropTable(
                name: "CLINIC_SPECIALIZATION");

            migrationBuilder.DropSequence(
                name: "clinic_hilo");

            migrationBuilder.DropSequence(
                name: "clinic_specialization_hilo");
        }
    }
}
