﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ADDRESS_SHORT",
                table: "CLINIC",
                maxLength: 510,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_CLINIC_CLINIC_SPEZIALIZATION",
                table: "CLINIC",
                column: "ID_CLINIC_SPECIALIZATION",
                schema: "public",
                principalTable: "CLINIC_SPECIALIZATION",
                principalColumn: "ID",
                principalSchema: "public");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ADDRESS_SHORT",
                table: "CLINIC");

            migrationBuilder.DropForeignKey(
                name: "FK_CLINIC_CLINIC_SPEZIALIZATION",
                table: "CLINIC",
                schema: "public");
        }
    }
}
