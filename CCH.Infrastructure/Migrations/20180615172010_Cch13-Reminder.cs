﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch13Reminder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "REMINDER",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Frequency = table.Column<int>(nullable: false, defaultValue: 0),
                    Id_user = table.Column<string>(nullable: true),
                    IsOn = table.Column<bool>(nullable: false, defaultValue: false),
                    IsOnMethodCall = table.Column<bool>(nullable: false, defaultValue: false),
                    IsOnMethodEmail = table.Column<bool>(nullable: false, defaultValue: false),
                    IsOnMethodPush = table.Column<bool>(nullable: false, defaultValue: false),
                    IsOnMethodSms = table.Column<bool>(nullable: false, defaultValue: false),
                    Time = table.Column<string>(maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REMINDER", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "REMINDER");
        }
    }
}
