﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IS_RECORD_AVAILABLE_PRIMARY_RECEPTION",
                table: "DOCTOR_SPECIALITY",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IS_RECORD_AVAILABLE_PRIMARY_RECEPTION",
                table: "DOCTOR_SPECIALITY");
        }
    }
}
