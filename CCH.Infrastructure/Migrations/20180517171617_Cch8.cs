﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SCHEDULE",
                table: "DOCTOR");

            migrationBuilder.CreateTable(
                name: "ScheduleItem",
                columns: table => new
                {
                    ID_PATIENT = table.Column<string>(nullable: true),
                    STATUS = table.Column<int>(nullable: true),
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CABINET = table.Column<string>(maxLength: 32, nullable: true),
                    DURATION = table.Column<long>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    FROM = table.Column<DateTime>(nullable: false),
                    ID_CLINIC = table.Column<long>(nullable: false),
                    ID_DOCTOR = table.Column<long>(nullable: false),
                    STOREY = table.Column<short>(nullable: false),
                    TO = table.Column<DateTime>(nullable: false),
                    DATE = table.Column<DateTime>(nullable: true),
                    DAY = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleItem", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduleItem");

            migrationBuilder.AddColumn<DateTime>(
                name: "SCHEDULE",
                table: "DOCTOR",
                maxLength: 510,
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
