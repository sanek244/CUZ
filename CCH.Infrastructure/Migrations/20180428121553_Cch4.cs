﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "INN",
                table: "CLINIC",
                maxLength: 12,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "KPP",
                table: "CLINIC",
                maxLength: 12,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OGRN",
                table: "CLINIC",
                maxLength: 15,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OKTMO",
                table: "CLINIC",
                maxLength: 15,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "INN",
                table: "CLINIC");

            migrationBuilder.DropColumn(
                name: "KPP",
                table: "CLINIC");

            migrationBuilder.DropColumn(
                name: "OGRN",
                table: "CLINIC");

            migrationBuilder.DropColumn(
                name: "OKTMO",
                table: "CLINIC");
        }
    }
}
