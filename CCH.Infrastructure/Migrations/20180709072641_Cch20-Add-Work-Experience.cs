﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch20AddWorkExperience : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WORK_EXPERIENCE",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FROM = table.Column<DateTime>(type: "Date", nullable: false),
                    ID_DOCTOR = table.Column<long>(nullable: false),
                    PLACE = table.Column<string>(maxLength: 256, nullable: false),
                    POSITION = table.Column<string>(maxLength: 256, nullable: false),
                    TO = table.Column<DateTime>(type: "Date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WORK_EXPERIENCE", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WORK_EXPERIENCE_DOCTOR_ID_DOCTOR",
                        column: x => x.ID_DOCTOR,
                        principalTable: "DOCTOR",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WORK_EXPERIENCE_ID_DOCTOR",
                table: "WORK_EXPERIENCE",
                column: "ID_DOCTOR");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WORK_EXPERIENCE");
        }
    }
}
