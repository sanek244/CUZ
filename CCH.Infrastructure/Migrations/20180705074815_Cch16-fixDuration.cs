﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch16fixDuration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "DURATION",
                table: "ScheduleItem",
                nullable: false,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "DURATION",
                table: "ScheduleItem",
                nullable: false,
                oldClrType: typeof(short));
        }
    }
}
