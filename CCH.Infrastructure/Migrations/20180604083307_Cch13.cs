﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch13 : Migration
    {
        #region Methods

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                "IS_DELETED",
                "CLINIC",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "IS_DELETED",
                "CLINIC");
        }

        #endregion
    }
}