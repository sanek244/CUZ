﻿using CCH.Resources;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch19AddFunctionAppointmentCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(CCHResourceManager.GetContent("SQL.count_appointments_by_day.create.sql"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(CCHResourceManager.GetContent("SQL.count_appointments_by_day.drop.sql"));
        }
    }
}
