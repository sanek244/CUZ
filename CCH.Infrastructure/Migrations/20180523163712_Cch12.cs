﻿using CCH.Resources;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ScheduleItem_ID_CLINIC",
                table: "ScheduleItem",
                column: "ID_CLINIC");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleItem_ID_DOCTOR",
                table: "ScheduleItem",
                column: "ID_DOCTOR");

            migrationBuilder.CreateIndex(
                name: "IX_DOCTOR_ID_DOCTOR_SPECIALITY",
                table: "DOCTOR",
                column: "ID_DOCTOR_SPECIALITY");

            migrationBuilder.AddForeignKey(
                name: "FK_DOCTOR_DOCTOR_SPECIALITY_ID_DOCTOR_SPECIALITY",
                table: "DOCTOR",
                column: "ID_DOCTOR_SPECIALITY",
                principalTable: "DOCTOR_SPECIALITY",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleItem_CLINIC_ID_CLINIC",
                table: "ScheduleItem",
                column: "ID_CLINIC",
                principalTable: "CLINIC",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleItem_DOCTOR_ID_DOCTOR",
                table: "ScheduleItem",
                column: "ID_DOCTOR",
                principalTable: "DOCTOR",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

                migrationBuilder.Sql(CCHResourceManager.GetContent("SQL.doctor_search.create.sql"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DOCTOR_DOCTOR_SPECIALITY_ID_DOCTOR_SPECIALITY",
                table: "DOCTOR");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleItem_CLINIC_ID_CLINIC",
                table: "ScheduleItem");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleItem_DOCTOR_ID_DOCTOR",
                table: "ScheduleItem");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleItem_ID_CLINIC",
                table: "ScheduleItem");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleItem_ID_DOCTOR",
                table: "ScheduleItem");

            migrationBuilder.DropIndex(
                name: "IX_DOCTOR_ID_DOCTOR_SPECIALITY",
                table: "DOCTOR");

            migrationBuilder.Sql(CCHResourceManager.GetContent("SQL.doctor_search.drop.sql"));
        }
    }
}
