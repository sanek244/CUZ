﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CCH.Infrastructure.Migrations
{
    public partial class Cch17updateDoctor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IS_RECORD_AVAILABLE_PRIMARY_RECEPTION",
                table: "DOCTOR",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IS_RECORD_AVAILABLE_PRIMARY_RECEPTION",
                table: "DOCTOR");
        }
    }
}
