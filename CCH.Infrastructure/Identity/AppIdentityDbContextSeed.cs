﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CCH.Infrastructure.Identity
{
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager, AppIdentityDbContext context)
        {
            try
            {
                context.Database.Migrate();

                if (!context.Users.Any())
                {
                    context.Add(new ApplicationUser
                    {
                        Login = "user1",
                        PasswordHash = "KawlZg4weOh+MJfTgi5Q1w==", //12345678
                        UserName = "Иван",
                        Surname = "Дворкович",
                        Patronymic = "Алексеевич"
                    });

                    context.Add(new ApplicationUser
                    {
                        Login = "user2",
                        PasswordHash = "BtSWMsncm8tirq75lhK6aw==", //1
                        UserName = "Дмитрий",
                        Surname = "Зурабанов",
                        Patronymic = "Георгович",
                        PhoneNumber = "+7 923 404 58 99"
                    });

                    context.Add(new ApplicationUser
                    {
                        Login = "admin",
                        PasswordHash = "GaKFQUS2Oo92F6byJQGbEg==", //admin
                        UserName = "Администратор",
                        Surname = "Администраторов",
                        Patronymic = "Администраторович",
                        PhoneNumber = "+7 777 777 77 77"
                    });
                    await context.SaveChangesAsync();
                }
            }
            catch(Exception error)
            {

            }
        }
    }
}