﻿using CCH.Common.Entities;
using Microsoft.AspNetCore.Identity;

namespace CCH.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// Строка для входа в систему
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Напоинания по календарю
        /// </summary>
        public Reminder Reminder;
    }
}