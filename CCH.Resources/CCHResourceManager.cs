﻿using System.IO;
using System.Reflection;
using System.Text;

namespace CCH.Resources
{
    public class CCHResourceManager
    {
        /// <summary>
        /// Получение содержимого файла по полному имени в проекте.
        /// </summary>
        /// <param name="fileName">Полный путь к файлу внутри сборки (разделитель '.').</param>
        /// <returns></returns>
        public static string GetContent(string fileName)
        {
            var assembly = Assembly.GetAssembly(typeof(CCHResourceManager));
            var resourceStream = assembly.GetManifestResourceStream("CCH.Resources." + fileName);
            using(var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
