﻿CREATE OR REPLACE FUNCTION public.doctor_search(
    ids_doctor_speciality bigint[],
    ids_clinic_speciality bigint[],
    id_clinic bigint,
    fio text,
    adress_clinic text,
	is_available_recording boolean,
	date_from DATE default null,
	date_to DATE default null,
	time_from TIME default null,
	time_to TIME default null)
RETURNS SETOF "DOCTOR" AS
$BODY$
DECLARE
	dow int;
	schedule_id_doctor bigint;
	schedule_date date;
	schedule_of_the_day record;
	schedule_time_from TIME;
BEGIN
	DROP TABLE IF EXISTS temp_doctor_search;
	CREATE TEMP TABLE temp_doctor_search AS
	SELECT DISTINCT 
		d.*
	FROM public."DOCTOR" AS d
		JOIN public."DOCTOR_SPECIALITY" AS s ON s."ID" = d."ID_DOCTOR_SPECIALITY"
		JOIN public."ClinicDoctor" AS cd ON cd."ID_DOCTOR" = d."ID"
		JOIN public."CLINIC" AS c ON c."ID" = cd."ID_CLINIC"
	WHERE 
		(cd."ID_CLINIC" = id_clinic OR COALESCE(id_clinic, 0) = 0) AND
		(s."ID" = ANY(ids_doctor_speciality) OR array_length(ids_doctor_speciality, 1) IS NULL) AND
		(c."ID_CLINIC_SPECIALIZATION" = ANY(ids_clinic_speciality) OR array_length(ids_clinic_speciality, 1) IS NULL) AND
		(c."ADDRESS_DESCRIPTION" ~* adress_clinic OR adress_clinic IS NULL) AND
		(d."DOCTOR_F" ~* fio OR d."DOCTOR_I" ~* fio OR d ."DOCTOR_O" ~* fio OR fio IS NULL) AND
		(NOT is_available_recording OR d."IS_RECORD_AVAILABLE_PRIMARY_RECEPTION" = true) AND
		(NOT is_available_recording OR EXISTS (
			SELECT 1
			FROM public."ScheduleItem" AS si 
				JOIN public."CLINIC" AS c2 ON c2."ID" = si."ID_CLINIC"
			WHERE 
				d."ID" = si."ID_DOCTOR" AND 
				(si."ID_CLINIC" = id_clinic OR COALESCE(id_clinic, 0) = 0) AND 
				(c2."ID_CLINIC_SPECIALIZATION" = ANY(ids_clinic_speciality) OR array_length(ids_clinic_speciality, 1) IS NULL) AND
				(c2."ADDRESS_DESCRIPTION" ~* adress_clinic OR adress_clinic IS NULL) AND
				si."Discriminator" != 'Appointment'
		));
	
	--Проверка на случай некорректно введенных данных
	IF NOT (date_from IS NOT NULL AND date_to IS NOT NULL AND time_from IS NOT NULL AND time_to IS NOT NULL) OR date_from > date_to OR time_from > time_to THEN
		date_from = NULL;
		date_to = NULL;
		time_from = NULL;
		time_to = NULL;
	END IF;
	
	IF is_available_recording OR (date_from IS NOT NULL AND date_to IS NOT NULL AND time_from IS NOT NULL AND time_to IS NOT NULL) THEN
		DROP TABLE IF EXISTS temp_doctor_search_schedule;
		CREATE TEMPORARY TABLE temp_doctor_search_schedule (id_doctor BIGINT, date_of_schedule DATE, time_from TIME);
		
		--Проходим по каждому врачу, у которого есть расписание
		FOR schedule_id_doctor IN SELECT "ID" FROM temp_doctor_search LOOP
			dow = EXTRACT(DOW FROM COALESCE(date_from, CURRENT_DATE)) - 1;
			schedule_date = CURRENT_DATE;
			
			--Проходим по каждому дню, ищем свободное время на любой день (в течении 30 дней, если не выбран поиск по дате)
			FOR dom IN 0..(COALESCE(date_to - date_from, 30)) LOOP
				dow = (dow + 1) % 7;
				schedule_date = (COALESCE (date_from, CURRENT_DATE) + dom * INTERVAL '1 day')::DATE;
				
				--Проходим по каждому элементу расписания в порядке приоритета (Расписание на конкретный день > Расписание на день недели > Общее расписания)
				IF EXISTS (SELECT 1 FROM public."ScheduleItem" WHERE "ID_DOCTOR" = schedule_id_doctor AND "DATE"::DATE = schedule_date AND "Discriminator" = 'ScheduleItemDay') THEN
					FOR schedule_of_the_day IN SELECT * FROM public."ScheduleItem" WHERE "ID_DOCTOR" = schedule_id_doctor AND "DATE"::DATE = schedule_date AND "Discriminator" = 'ScheduleItemDay' LOOP
						schedule_time_from = schedule_of_the_day."FROM"::TIME;
						WHILE schedule_time_from < schedule_of_the_day."TO"::TIME AND schedule_time_from < COALESCE(time_to, '23:59:59'::TIME) LOOP
							IF schedule_time_from >= COALESCE(time_from, '00:00:00'::TIME) THEN
								INSERT INTO temp_doctor_search_schedule VALUES (schedule_id_doctor, schedule_date, schedule_time_from);
							END IF;
							schedule_time_from = schedule_time_from + schedule_of_the_day."DURATION" * INTERVAL '1 minute';
						END LOOP;
					END LOOP;
				ELSEIF EXISTS (SELECT 1 FROM public."ScheduleItem" WHERE "ID_DOCTOR" = schedule_id_doctor AND "DAY" = dow AND "Discriminator" = 'ScheduleItemDayWeek') THEN
					FOR schedule_of_the_day IN SELECT * FROM public."ScheduleItem" WHERE "ID_DOCTOR" = schedule_id_doctor AND "DAY" = dow AND "Discriminator" = 'ScheduleItemDayWeek' LOOP
						schedule_time_from = schedule_of_the_day."FROM"::TIME;
						WHILE schedule_time_from < schedule_of_the_day."TO"::TIME AND schedule_time_from < COALESCE(time_to, '23:59:59'::TIME) LOOP
							IF schedule_time_from >= COALESCE(time_from, '00:00:00'::TIME) THEN
								INSERT INTO temp_doctor_search_schedule VALUES (schedule_id_doctor, schedule_date, schedule_time_from);
							END IF;
							schedule_time_from = schedule_time_from + schedule_of_the_day."DURATION" * INTERVAL '1 minute';
						END LOOP;
					END LOOP;
				END IF;
				
				IF dom = 0 AND COALESCE(date_from, CURRENT_DATE) = CURRENT_DATE THEN
					DELETE FROM temp_doctor_search_schedule AS t
					WHERE t.id_doctor = schedule_id_doctor AND t.time_from < CURRENT_TIME + INTERVAL '2 hour';
				END IF;
				
				DELETE FROM temp_doctor_search_schedule AS t
				USING  public."ScheduleItem" AS s 
				WHERE s."ID_DOCTOR" = t.id_doctor AND s."FROM"::DATE = t.date_of_schedule AND s."FROM"::TIME = t.time_from AND s."Discriminator" = 'Appointment';
				
				IF EXISTS (SELECT 1 FROM temp_doctor_search_schedule WHERE id_doctor = schedule_id_doctor) THEN
					EXIT;
				END IF;
				
			END LOOP;
			
		END LOOP;
		
		RETURN QUERY SELECT DISTINCT D.* 
		FROM temp_doctor_search AS D
			JOIN temp_doctor_search_schedule AS T ON T.id_doctor = D."ID";
	
	ELSE
	
		RETURN QUERY SELECT D.* 
		FROM temp_doctor_search AS D;
		
	END IF;
	
END;	
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.doctor_search(bigint[], bigint[], bigint, text, text, boolean, date, date, time, time)
  OWNER TO postgres;
