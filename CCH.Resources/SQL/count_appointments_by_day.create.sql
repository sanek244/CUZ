﻿CREATE OR REPLACE FUNCTION public.count_appointments_by_day(id_clinic bigint, id_doctor bigint)
RETURNS TABLE(Date date, Count bigint) AS
$BODY$
	SELECT s."FROM"::Date, COUNT(s)
	FROM public."ScheduleItem" AS s
	WHERE 
		s."Discriminator" = 'Appointment' AND 
		s."STATUS" < 2 AND 
		s."ID_CLINIC" = id_clinic AND
		s."ID_DOCTOR" = id_doctor AND
		s."FROM" > CURRENT_DATE
	GROUP BY 
		s."FROM"::Date;
$BODY$
  LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.count_appointments_by_day(bigint, bigint)
  OWNER TO postgres;
